var kony = kony || {};
kony.diebold = kony.diebold || {};
kony.diebold.controller = kony.diebold.controller || {};

kony.diebold.controller.confirmationController = function (){
  	/**
	 * @function initialize: Constructor for the Skin Controller 
	 */
	var initialize = function () {
      
      //PreShow
       
      frmConfirmation.preShow = onConfirmationPreShow();
      
      frmConfirmation.postShow = onConfirmationPostShow();
      
      //Header
      	 frmConfirmation.flxBackBtn.onTouchEnd = onBackBtnConfirmationClick;
      
      frmConfirmation.flxTouchIDVerification.onTouchEnd = onTouchIdVerificationConfirmationClick;
      	
      frmConfirmation.btnBackToHomeBtn.onClick = onBtnBackToHomeConfirmationClick;
    },	
        
         /*
  * PreShow function
  */
    onConfirmationPreShow = function(){
  //   return confirmationInit.call(this);
    };
  
        /*
  * PostShow function
  */
    onConfirmationPostShow = function(){
  	return paymentCompleted.call(this);
    };
        
   /*
  * Header Back button function
  */
    onBackBtnConfirmationClick = function(){
   frmCheckout.show();
    };
  
   /*
  * Touch Id verification button function
  */
    onTouchIdVerificationConfirmationClick = function(){
   	return paymentCompleted.call(this);
    };
  
  
   /*
  * onn Back to Home button function
  */
    onBtnBackToHomeConfirmationClick = function(){
   		frmHome.show();
    };
  
     
/*
* Intialize class
*/
  initialize();
};