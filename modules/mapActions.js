//Welcome location map function
function setMapLocation() {
  var getMapData = function() {
    return [
      {
        lat:"30.3436117",
        lon:"-97.8032899",
        name:"The Grocery",
        desc:"4456 Koelpin Keys Austin, TX",
        showCallout:true,
        calloutData : {
          lblMessage: "The Grocery",
          lblAddress: "4456 Koelpin Keys Austin, TX"
        }      
      },
    ];  
  };

  frmHome.mapWelcomeLocation.locationData = getMapData();
  frmHome.mapWelcomeLocation.navigateTo(2, true);
}

//Search location map function
function setSearchLocations() {
  var getMapData = function() {
    return [
      {
        lat:"30.3436117",
        lon:"-97.8032899",
        name:"The Grocery",
        desc:"4456 Koelpin Keys Austin, TX",
        showCallout:false,
        calloutData : {
          lblMessage: "The Grocery",
          lblAddress: "4456 Koelpin Keys Austin, TX"
        }      
      },
      {
        lat:"30.3416072",
        lon:"-97.8053302",
        name:"Electronics Shop",
        desc:"4450 Koelpin Keys Austin, TX",
        showCallout:false,
        calloutData : {
          lblMessage: "Electronics Shop",
          lblAddress: "4456 Koelpin Keys Austin, TX"
        }      
      },
      {
        lat:"30.3416072",
        lon:"-97.8053302",
        name:"Open Market",
        desc:"4444 Koelpin Keys Austin, TX",
        showCallout:false,
        calloutData : {
          lblMessage: "Open Market",
          lblAddress: "4444 Koelpin Keys Austin, TX"
        }      
      },
      {
        lat:"30.336382",
        lon:"-97.8051277",
        name:"Monaco Retail",
        desc:"669 Naomie Curve Austin, TX",
        showCallout:false,
        calloutData : {
          lblMessage: "Monaco Retail",
          lblAddress: "669 Naomie Curve Austin, TX"
        }      
      },
      {
        lat:"30.3347895",
        lon:"-97.8087623",
        name:"Food Mart",
        desc:"669 Naomie Curve Austin, TX",
        showCallout:false,
        calloutData : {
          lblMessage: "Food Mart",
          lblAddress: "669 Naomie Curve Austin, TX"
        }      
      },
      {
        lat:"30.3305244",
        lon:"-97.8105199",
        name:"Bella Bella Boutique",
        desc:"6603 Bergstrom Parkway Austin, TX",
        showCallout:false,
        calloutData : {
          lblMessage: "Bella Bella Boutique",
          lblAddress: "6603 Bergstrom Parkway Austin, TX"
        }      
      },
    ];  
  };

  frmHome.mapSearchLocations.locationData = getMapData();
  frmHome.mapSearchLocations.navigateTo(2, false);
}