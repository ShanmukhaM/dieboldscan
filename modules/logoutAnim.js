//Logout animation
function logoutAnim()
{
  frmHome.flxWelcome.isVisible=true;
  frmHome.flxWelcome.opacity=1;
  frmHome.flxHomeContainer.isVisible=false;
  frmHome.flxWelcomeMessage.top='50%';
  frmHome.flxWelcomeMessage.opacity=0; 
  frmHome.flxWelcomeMessage.isVisible=false;
  frmHome.flxLocationConfirm.top='50%';
  frmHome.flxLocationConfirm.opacity=0;
  frmHome.flxLocationMap.opacity=0;
  frmHome.flxLocationConfirm.isVisible=false;
  menuToggleOff();
  loginAnim();
}