//Welcome Search animation
function searchLocation()
{
  var currFrm = kony.application.getCurrentForm();
  if (currFrm.flxSearchLocation.isVisible===false)
  {
    currFrm.flxSearchLocation.isVisible=true;
    currFrm.flxSearchHeader.animate(
      kony.ui.createAnimation({
        "0": {
          "top":"-8%"
        },
        "100":{
          "top":"0%","stepConfig":{"timingFunction": kony.anim.EASEIN_IN_OUT}}}),
      {"delay":0,"fillMode": kony.anim.FILL_MODE_FORWARDS,"duration": 0.25},
      {"animationEnd": function () {

      }}
    );

    currFrm.flxLocationList.animate(
      kony.ui.createAnimation({
        "0": {
          "top":"100%"
        },
        "100":{
          "top":"8%","stepConfig":{"timingFunction": kony.anim.EASEIN_IN_OUT}}}),
      {"delay":0,"fillMode": kony.anim.FILL_MODE_FORWARDS,"duration": 0.35},
      {"animationEnd": function () {

      }}
    );  
  }else if (currFrm.flxSearchLocation.isVisible===true)
  {
    currFrm.flxSearchHeader.animate(
      kony.ui.createAnimation({
        "0": {
          "top":"0%"
        },
        "100":{
          "top":"-8%","stepConfig":{"timingFunction": kony.anim.EASEIN_IN_OUT}}}),
      {"delay":0,"fillMode": kony.anim.FILL_MODE_FORWARDS,"duration": 0.25},
      {"animationEnd": function () {

      }}
    );

    currFrm.flxLocationList.animate(
      kony.ui.createAnimation({
        "0": {
          "top":"8%"
        },
        "100":{
          "top":"100%","stepConfig":{"timingFunction": kony.anim.EASEIN_IN_OUT}}}),
      {"delay":0,"fillMode": kony.anim.FILL_MODE_FORWARDS,"duration": 0.35},
      {"animationEnd": function () {
        currFrm.flxSearchLocation.isVisible=false;
      }}
    );  
  }
}

//Function when user selects a location
function locationSelected()
{
  var currFrm = kony.application.getCurrentForm();
  currFrm.flxHomeContainer.isVisible=true;
  currFrm.flxWelcome.animate(
    kony.ui.createAnimation({
      "0": {
        "opacity":1        
      },
      "100":{
        "opacity":0,"stepConfig":{"timingFunction": kony.anim.EASEIN_IN_OUT}}}),
    {"delay":0.65,"fillMode": kony.anim.FILL_MODE_FORWARDS,"duration": 0.2},
    {"animationEnd": function () {
      currFrm.flxWelcome.isVisible=false;
    }}
  );
}

//Function to switch list and map view
function mapToggleView()
{
  var currFrm = kony.application.getCurrentForm();
  if (currFrm.segLocationList.isVisible===true)
  {
    currFrm.mapSearchLocations.isVisible=true;
    currFrm.mapSearchLocations.animate(
      kony.ui.createAnimation({
        "0": {
          "opacity":0        
        },
        "100":{
          "opacity":1,"stepConfig":{"timingFunction": kony.anim.EASEIN_IN_OUT}}}),
      {"delay":0,"fillMode": kony.anim.FILL_MODE_FORWARDS,"duration": 0.2},
      {"animationEnd": function () {
        currFrm.lblListMapViewBtn.text='List';
      }}
    );
    currFrm.segLocationList.animate(
      kony.ui.createAnimation({
        "0": {
          "opacity":1        
        },
        "100":{
          "opacity":0,"stepConfig":{"timingFunction": kony.anim.EASEIN_IN_OUT}}}),
      {"delay":0,"fillMode": kony.anim.FILL_MODE_FORWARDS,"duration": 0.2},
      {"animationEnd": function () {
        currFrm.segLocationList.isVisible=false;
      }}
    );
  }else if (currFrm.segLocationList.isVisible===false){
    currFrm.segLocationList.isVisible=true;
    currFrm.flxMapLocationDetails.isVisible=false;
    currFrm.segLocationList.animate(
      kony.ui.createAnimation({
        "0": {
          "opacity":0
        },
        "100":{
          "opacity":1,"stepConfig":{"timingFunction": kony.anim.EASEIN_IN_OUT}}}),
      {"delay":0,"fillMode": kony.anim.FILL_MODE_FORWARDS,"duration": 0.2},
      {"animationEnd": function () {
        currFrm.lblListMapViewBtn.text='Map';
      }}
    );
    currFrm.mapSearchLocations.animate(
      kony.ui.createAnimation({
        "0": {
          "opacity":1        
        },
        "100":{
          "opacity":0,"stepConfig":{"timingFunction": kony.anim.EASEIN_IN_OUT}}}),
      {"delay":0,"fillMode": kony.anim.FILL_MODE_FORWARDS,"duration": 0.2},
      {"animationEnd": function () {
        currFrm.mapSearchLocations.isVisible=false;
      }}
    );
  }
}

//Function map location details popup
function mapLocationPopup()
{
  var currFrm = kony.application.getCurrentForm();
  currFrm.flxMapLocationDetails.isVisible=true;
  currFrm.flxMapLocationDetails.animate(
    kony.ui.createAnimation({
      "0": {
        "opacity":0,
        "bottom":"-160dp"
      },
      "60":{
        "opacity":1,
        "bottom":"20dp"
      },
      "100":{
        "bottom":"10dp","stepConfig":{"timingFunction": kony.anim.EASEIN_IN_OUT}}}),
    {"delay":0,"fillMode": kony.anim.FILL_MODE_FORWARDS,"duration": 0.2},
    {"animationEnd": function () {
      
    }}
  );
}
