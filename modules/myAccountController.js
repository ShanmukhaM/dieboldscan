var kony = kony || {};
kony.diebold = kony.diebold || {};
kony.diebold.controller = kony.diebold.controller || {};

kony.diebold.controller.myAccountController = function (){
  	/**
	 * @function initialize: Constructor for the Skin Controller 
	 */
	var initialize = function () {
      
      //Header
      	 frmAccount.flxBackBtn.onTouchEnd = onBackBtnClick;
    	 frmAccount.flxEditBtn.onTouchEnd= onEditBtnClick;
      	 
	},	
        
   /*
  * Header Back button function
  */
    onBackBtnClick = function(){
   return backBtn.call(this);
    };
  
  
   /*
  * Header Edit button function
  */
    onEditBtnClick = function(){
  //no funtion yet 
    };
  
  
/*
* Intialize class
*/
  initialize();
};