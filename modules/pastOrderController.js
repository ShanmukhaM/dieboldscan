var kony = kony || {};
kony.diebold = kony.diebold || {};
kony.diebold.controller = kony.diebold.controller || {};

kony.diebold.controller.pastOrderController = function (){
  	/**
	 * @function initialize: Constructor for the Skin Controller 
	 */
	var initialize = function () {
      
      //Header
      	 frmPastOrders.flxBackBtn.onTouchEnd = onBackBtnPastOrderClick;
      	 frmPastOrders.segPastOrders.onRowClick = onRowClickSegPastOrderClick;
      	 
	},	
        
   /*
  * Header Back button function
  */
    onBackBtnPastOrderClick = function(){
   return backBtn.call(this);
    };
  
  
   /*
  * OnRowClick Segment function
  */
    onRowClickSegPastOrderClick = function(){
  
      frmPastOrderDetails.show();
      
    };
  
  
/*
* Intialize class
*/
  initialize();
};