var kony = kony || {}, gblInstance;
kony.diebold = kony.diebold || {};


kony.diebold.Globals = function() {
var loginController,myAccountController,pastOrderController,CartController,checkOutController,
        /**
         * @function initialize: Initialize function to define constants
         */
        initialize = function() {
        };
     
    /*
     *	Login Controller
     */
    this.getloginController = function() {
        loginController = new kony.diebold.controller.loginController();
        return loginController;
    };
   
   /*
     *	MyAccount Controller
     */
    this.getmyAccountController = function() {
        myAccountController = new kony.diebold.controller.myAccountController();
        return myAccountController;
    };
   
  
   /*
     *	Past Order Controller
     */
    this.getpastOrderController = function() {
        pastOrderController = new kony.diebold.controller.pastOrderController();
        return pastOrderController;
    };
  
   /*
     *	Card Controller
     */
    this.getCartController = function() {
        CartController = new kony.diebold.controller.CartController();
        return CartController;
    };
  
  /*
     *	CheckOut Controller
     */
    this.getcheckOutController = function() {
        checkOutController = new kony.diebold.controller.checkOutController();
        return checkOutController;
    };
  
  
  /*
     *	confirmation Controller
     */
    this.getconfirmationController = function() {
        confirmationController = new kony.diebold.controller.confirmationController();
        return confirmationController;
    };
  
   /*
     *	Manage payment Controller
     */
    this.getmanagePaymentController = function() {
        managePaymentController = new kony.diebold.controller.managePaymentController();
        return managePaymentController;
    };
  
    
    this.getpastOrderDetailsController = function() {
        pastOrderDetailsController = new kony.diebold.controller.pastOrderDetailsController();
        return pastOrderDetailsController;
    };
  
  
  /*
     *	 Payment Method Controller
     */
 	
  	this.getpaymentMethodController= function() {
      paymentMethodController = new kony.diebold.controller.paymentMethodController();
      return paymentMethodController;
    };
  
  /*
     *	 product details Controller
     */
  
  this.getproductDetailController = function() {
    productDetailController = new kony.diebold.controller.productDetailController();
    return productDetailController;
  };
  
  /*
     *	form Scan OverLay Controller
     */
  
  this.getscanOverLayController = function() {
    scanOverLayController = new kony.diebold.controller.scanOverLayController();
    return scanOverLayController;
  }
  
  /*
  * Initialize
  */
    initialize();
};