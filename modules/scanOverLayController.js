var kony = kony || {};
kony.diebold = kony.diebold || {};
kony.diebold.controller = kony.diebold.controller || {};

kony.diebold.controller.scanOverLayController = function() {
  
   /**
	 * @function initialize: Constructor for the Skin Controller 
	 */
  var initialize = function() {
    frmScanOverlay.init = scanOverlayInitialize;
    frmScanOverlay.flxCloseBtn.onTouchEnd = closeBtnScanForm;
    frmScanOverlay.flxCameraBtn.onTouchEnd = clickCamera;
    
  },
      
 /*
  * init function loaded
  */  
     scanOverlayInitialize = function() {
       return scanInit.call(this);
     };
  
  /*
  * scan form closed
  */
  	 closeBtnScanForm = function() {
       frmCart.show();
     };
  
  /*
  * camera click function
  */
  	clickCamera = function() {
      scanItemService.call(this);
      slideInRecipe.call(this);
      scannedItem.call(this);
    };
  
  
/*
* Intialize class
*/
  initialize();
  
};