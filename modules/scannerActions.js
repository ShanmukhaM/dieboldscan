//Scan screen initial state
function scanInit()
{
  frmScanOverlay.flxSlideInMessage.top='-16%';
  frmScanOverlay.flxRecentlyScannedItemOne.isVisible=false;
  frmScanOverlay.flxRecentlyScannedItemOne.opacity=0; 
}
//Add scanned item to recently scanned list
function scannedItem()
{
  var currFrm = kony.application.getCurrentForm();
  var trans0 = kony.ui.makeAffineTransform();
  trans0.scale(1, 1);
  var trans50 = kony.ui.makeAffineTransform();
  trans50.scale(1.2, 1.2);
  var trans100 = kony.ui.makeAffineTransform();
  trans100.scale(1, 1);
  currFrm.flxRecentlyScannedItemOne.isVisible=true;
  currFrm.flxRecentlyScannedItemOne.animate(
    kony.ui.createAnimation({
      "0": {
        "transform":trans0,
        "opacity":0
      },
      "50": {
        "transform":trans50,
        "opacity":1
      },
      "100":{
        "opacity":1,"stepConfig":{"timingFunction": kony.anim.EASE},"transform": trans100}}),
    {"delay":0.2,"fillMode": kony.anim.FILL_MODE_FORWARDS,"duration": 0.5},
    {"animationEnd": function () {

    }}
  );

}