var kony = kony || {};
kony.diebold = kony.diebold || {};
kony.diebold.controller = kony.diebold.controller || {};

kony.diebold.controller.productDetailController = function() {
  
  /**
	 * @function initialize: Constructor for the Skin Controller 
	 */
  
  var initialize = function() {
    
    frmProductDetail.init = productDetailInitialize;
    frmProductDetail.flxBackBtn.onTouchEnd = backButtonProductDetail;
    frmProductDetail.flxCartBtn.onTouchEnd = clickCartBtn;
 
  },
      
 /*
  * init function loaded
  */
      
      productDetailInitialize = function() {
           return pageSkinTransp.call(this);
      };
  
  /*
  * Header Back button function
  */
  
  	backButtonProductDetail = function() {
      return backBtn.call(this);
    };
  
  /*
  * Header Cart button function
  */
    
    clickCartBtn = function() {
       frmCart.show();
    };

/*
* Intialize class
*/
  initialize();
  
};