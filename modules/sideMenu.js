//Side menu initial state
function menuInit()
{
  frmHome.flxSideMenuContainer.isVisible=false;
  frmHome.flxOverlay.opacity=0;
  frmHome.flxMenuList.left='-80%';
  frmHome.flxMenuList.flxMyAccountBtn.left='-100%';
  frmHome.flxMenuList.flxPastOrdersBtn.left='-100%';
  frmHome.flxMenuList.flxPaymentBtn.left='-100%';
  frmHome.flxMenuList.flxSavedRecipesBtn.left='-100%';
  frmHome.flxMenuList.flxSettingsBtn.left='-100%';
  frmHome.flxMenuList.flxLogoutBtn.left='-100%';
}

//Side menu animation
function menuToggleOn()
{
  var currFrm = kony.application.getCurrentForm();
  currFrm.flxSideMenuContainer.isVisible=true;
  currFrm.flxOverlay.animate(
    kony.ui.createAnimation({
      "0": {
        "opacity":0        
      },
      "100":{
        "opacity":1,"stepConfig":{"timingFunction": kony.anim.EASEIN_IN_OUT}}}),
    {"delay":0.2,"fillMode": kony.anim.FILL_MODE_FORWARDS,"duration": 0.3},
    {"animationEnd": function () {

    }}
  );
  currFrm.flxMenuList.animate(
    kony.ui.createAnimation({
      "0": {
        "left":"-80%"        
      },
      "100":{
        "left":"0%","stepConfig":{"timingFunction": kony.anim.EASEIN_IN_OUT}}}),
    {"delay":0.2,"fillMode": kony.anim.FILL_MODE_FORWARDS,"duration": 0.3},
    {"animationEnd": function () {

    }}
  );
  currFrm.flxMyAccountBtn.animate(
    kony.ui.createAnimation({
      "0": {
        "left":"-100%"        
      },
      "100":{
        "left":"0%","stepConfig":{"timingFunction": kony.anim.EASEIN_IN_OUT}}}),
    {"delay":0.3,"fillMode": kony.anim.FILL_MODE_FORWARDS,"duration": 0.3},
    {"animationEnd": function () {

    }}
  );
  currFrm.flxPastOrdersBtn.animate(
    kony.ui.createAnimation({
      "0": {
        "left":"-100%"        
      },
      "100":{
        "left":"0%","stepConfig":{"timingFunction": kony.anim.EASEIN_IN_OUT}}}),
    {"delay":0.35,"fillMode": kony.anim.FILL_MODE_FORWARDS,"duration": 0.3},
    {"animationEnd": function () {

    }}
  );
  currFrm.flxPaymentBtn.animate(
    kony.ui.createAnimation({
      "0": {
        "left":"-100%"        
      },
      "100":{
        "left":"0%","stepConfig":{"timingFunction": kony.anim.EASEIN_IN_OUT}}}),
    {"delay":0.4,"fillMode": kony.anim.FILL_MODE_FORWARDS,"duration": 0.3},
    {"animationEnd": function () {

    }}
  );
  currFrm.flxSavedRecipesBtn.animate(
    kony.ui.createAnimation({
      "0": {
        "left":"-100%"        
      },
      "100":{
        "left":"0%","stepConfig":{"timingFunction": kony.anim.EASEIN_IN_OUT}}}),
    {"delay":0.45,"fillMode": kony.anim.FILL_MODE_FORWARDS,"duration": 0.3},
    {"animationEnd": function () {

    }}
  );
  currFrm.flxSettingsBtn.animate(
    kony.ui.createAnimation({
      "0": {
        "left":"-100%"        
      },
      "100":{
        "left":"0%","stepConfig":{"timingFunction": kony.anim.EASEIN_IN_OUT}}}),
    {"delay":0.5,"fillMode": kony.anim.FILL_MODE_FORWARDS,"duration": 0.3},
    {"animationEnd": function () {

    }}
  );
  currFrm.flxLogoutBtn.animate(
    kony.ui.createAnimation({
      "0": {
        "left":"-100%"        
      },
      "100":{
        "left":"0%","stepConfig":{"timingFunction": kony.anim.EASEIN_IN_OUT}}}),
    {"delay":0.55,"fillMode": kony.anim.FILL_MODE_FORWARDS,"duration": 0.3},
    {"animationEnd": function () {

    }}
  );
}

function menuToggleOff(){
  var currFrm = kony.application.getCurrentForm();
  currFrm.flxMyAccountBtn.animate(
    kony.ui.createAnimation({
      "0": {
        "left":"0%"        
      },
      "100":{
        "left":"-100%","stepConfig":{"timingFunction": kony.anim.EASEIN_IN_OUT}}}),
    {"delay":0,"fillMode": kony.anim.FILL_MODE_FORWARDS,"duration": 0.3},
    {"animationEnd": function () {

    }}
  );
  currFrm.flxPastOrdersBtn.animate(
    kony.ui.createAnimation({
      "0": {
        "left":"0%"        
      },
      "100":{
        "left":"-100%","stepConfig":{"timingFunction": kony.anim.EASEIN_IN_OUT}}}),
    {"delay":0,"fillMode": kony.anim.FILL_MODE_FORWARDS,"duration": 0.3},
    {"animationEnd": function () {

    }}
  );
  currFrm.flxPaymentBtn.animate(
    kony.ui.createAnimation({
      "0": {
        "left":"0%"        
      },
      "100":{
        "left":"-100%","stepConfig":{"timingFunction": kony.anim.EASEIN_IN_OUT}}}),
    {"delay":0,"fillMode": kony.anim.FILL_MODE_FORWARDS,"duration": 0.3},
    {"animationEnd": function () {

    }}
  );
  currFrm.flxSavedRecipesBtn.animate(
    kony.ui.createAnimation({
      "0": {
        "left":"0%"        
      },
      "100":{
        "left":"-100%","stepConfig":{"timingFunction": kony.anim.EASEIN_IN_OUT}}}),
    {"delay":0,"fillMode": kony.anim.FILL_MODE_FORWARDS,"duration": 0.3},
    {"animationEnd": function () {

    }}
  );
  currFrm.flxSettingsBtn.animate(
    kony.ui.createAnimation({
      "0": {
        "left":"0%"        
      },
      "100":{
        "left":"-100%","stepConfig":{"timingFunction": kony.anim.EASEIN_IN_OUT}}}),
    {"delay":0,"fillMode": kony.anim.FILL_MODE_FORWARDS,"duration": 0.3},
    {"animationEnd": function () {

    }}
  );
  currFrm.flxLogoutBtn.animate(
    kony.ui.createAnimation({
      "0": {
        "left":"0%"        
      },
      "100":{
        "left":"-100%","stepConfig":{"timingFunction": kony.anim.EASEIN_IN_OUT}}}),
    {"delay":0,"fillMode": kony.anim.FILL_MODE_FORWARDS,"duration": 0.3},
    {"animationEnd": function () {

    }}
  );
  currFrm.flxOverlay.animate(
    kony.ui.createAnimation({
      "0": {
        "opacity":1        
      },
      "100":{
        "opacity":0,"stepConfig":{"timingFunction": kony.anim.EASEIN_IN_OUT}}}),
    {"delay":0.3,"fillMode": kony.anim.FILL_MODE_FORWARDS,"duration": 0.3},
    {"animationEnd": function () {

    }}
  );
  currFrm.flxMenuList.animate(
    kony.ui.createAnimation({
      "0": {
        "left":"0%"        
      },
      "100":{
        "left":"-80%","stepConfig":{"timingFunction": kony.anim.EASEIN_IN_OUT}}}),
    {"delay":0.3,"fillMode": kony.anim.FILL_MODE_FORWARDS,"duration": 0.3},
    {"animationEnd": function () {
      currFrm.flxSideMenuContainer.isVisible=false;
    }}
  );
}