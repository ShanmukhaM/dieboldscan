package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import com.zebra.sda.StartScanner;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_nsbarcode extends JSLibrary {

 
	String[] methods = { };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[1];
 libs[0] = new BarCodeScan();
 return libs;
 }



	public N_nsbarcode(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "nsbarcode";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 


class BarCodeScan extends JSLibrary {

 
 
	public static final String launchMiniApp = "launchMiniApp";
 
	String[] methods = { launchMiniApp };

	public Object createInstance(final Object[] params) {
 return new com.zebra.sda.StartScanner(
 );
 }


	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 com.konylabs.vm.Function fun0 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 fun0 = (com.konylabs.vm.Function)params[0+inc];
 }
 ret = this.launchMiniApp(params[0]
 ,fun0
 );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "BarCodeScan";
	}

	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] launchMiniApp( Object self ,com.konylabs.vm.Function inputKey0
 ){
 
		Object[] ret = null;
 ((com.zebra.sda.StartScanner)self).launchMiniApp( (com.konylabs.vm.Function)inputKey0
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
}

};
