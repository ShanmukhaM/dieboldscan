//Slide in message from top animation
function slideInMessage() {
    var currFrm = kony.application.getCurrentForm();
    var trans0 = kony.ui.makeAffineTransform();
    trans0.scale(1, 1);
    var trans50 = kony.ui.makeAffineTransform();
    trans50.scale(1.5, 1.5);
    var trans100 = kony.ui.makeAffineTransform();
    trans100.scale(1, 1);
    currFrm.flxSlideInMessage.animate(kony.ui.createAnimation({
        "0": {
            "top": "-6%"
        },
        "100": {
            "top": "0%",
            "stepConfig": {
                "timingFunction": kony.anim.EASEIN_IN_OUT
            }
        }
    }), {
        "delay": 0,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    }, {
        "animationEnd": function() {
            currFrm.imgSlideInMessageIcon.animate(kony.ui.createAnimation({
                "0": {
                    "transform": trans0,
                },
                "50": {
                    "transform": trans50,
                },
                "100": {
                    "stepConfig": {
                        "timingFunction": kony.anim.EASE
                    },
                    "transform": trans100
                }
            }), {
                "delay": 0,
                "iterationCount": 1,
                "fillMode": kony.anim.FILL_MODE_FORWARDS,
                "duration": 0.25
            }, {
                "animationEnd": function() {}
            });
            //slide up
            currFrm.flxSlideInMessage.animate(kony.ui.createAnimation({
                "0": {
                    "top": "0%"
                },
                "100": {
                    "top": "-6%",
                    "stepConfig": {
                        "timingFunction": kony.anim.EASEIN_IN_OUT
                    }
                }
            }), {
                "delay": 2,
                "fillMode": kony.anim.FILL_MODE_FORWARDS,
                "duration": 0.25
            }, {
                "animationEnd": function() {}
            });
        }
    });
}
//Slide in recipe messag from top animation
function slideInRecipe() {
    var currFrm = kony.application.getCurrentForm();
    currFrm.flxSlideInMessage.animate(kony.ui.createAnimation({
        "0": {
            "top": "-16%"
        },
        "100": {
            "top": "0%",
            "stepConfig": {
                "timingFunction": kony.anim.EASEIN_IN_OUT
            }
        }
    }), {
        "delay": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    }, {
        "animationEnd": function() {
            //slide up
            currFrm.flxSlideInMessage.animate(kony.ui.createAnimation({
                "0": {
                    "top": "0%"
                },
                "100": {
                    "top": "-16%",
                    "stepConfig": {
                        "timingFunction": kony.anim.EASEIN_IN_OUT
                    }
                }
            }), {
                "delay": 4,
                "fillMode": kony.anim.FILL_MODE_FORWARDS,
                "duration": 0.25
            }, {
                "animationEnd": function() {}
            });
        }
    });
}