//Popup Initial State
function popupInit() {
    var currFrm = kony.application.getCurrentForm();
    //   frmHome.flxPopupMessage.isVisible=false;
    //   frmHome.flxPopupOverlay.opacity=0;
    //   frmHome.flxPopupBox.top='50%';
    //   frmHome.flxPopupBox.opacity=0;
    currFrm.flxPopupMessage.isVisible = false;
    currFrm.flxPopupOverlay.opacity = 0;
    currFrm.flxPopupBox.top = '50%';
    currFrm.flxPopupBox.opacity = 0;
}
//Popup animation show/hide
function popupToggle() {
    var currFrm = kony.application.getCurrentForm();
    if (currFrm.flxPopupMessage.isVisible === false) {
        currFrm.flxPopupMessage.isVisible = true;
        currFrm.flxPopupOverlay.animate(kony.ui.createAnimation({
            "0": {
                "opacity": 0
            },
            "100": {
                "opacity": 1,
                "stepConfig": {
                    "timingFunction": kony.anim.EASEIN_IN_OUT
                }
            }
        }), {
            "delay": 0.2,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": function() {}
        });
        currFrm.flxPopupBox.animate(kony.ui.createAnimation({
            "0": {
                "top": "50%",
                "opacity": 0
            },
            "100": {
                "top": "15%",
                "opacity": 1,
                "stepConfig": {
                    "timingFunction": kony.anim.EASEIN_IN_OUT
                }
            }
        }), {
            "delay": 0.3,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": function() {}
        });
    } else {
        currFrm.flxPopupOverlay.animate(kony.ui.createAnimation({
            "0": {
                "opacity": 1
            },
            "100": {
                "opacity": 0,
                "stepConfig": {
                    "timingFunction": kony.anim.EASEIN_IN_OUT
                }
            }
        }), {
            "delay": 0,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": function() {}
        });
        currFrm.flxPopupBox.animate(kony.ui.createAnimation({
            "0": {
                "top": "15%",
                "opacity": 1
            },
            "100": {
                "top": "50%",
                "opacity": 0,
                "stepConfig": {
                    "timingFunction": kony.anim.EASEIN_IN_OUT
                }
            }
        }), {
            "delay": 0,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": function() {
                currFrm.flxPopupMessage.isVisible = false;
            }
        });
    }
}