var kony = kony || {};
kony.diebold = kony.diebold || {};
kony.diebold.controller = kony.diebold.controller || {};
kony.diebold.controller.loginController = function() {
    /**
     * @function initialize: Constructor for the Skin Controller 
     */
    var initialize = function() {
            frmHome.init = homeInitfn;
            frmHome.postShow = homePostShowfn;
            frmHome.flxSignInBtn.onTouchEnd = AccountManagementService; //onLoginBtnClick;
            frmHome.flxForgotPasswordBtn.onTouchEnd = onForgotPasswordClick;
            frmHome.rememberMeSwitch.onSlide = onToggleRememberCustomerId;
            frmHome.btnSignUp.onClick = onRegisterUserClick;
            frmHome.btnConfrimLocationNo.onClick = onConfirmLocationNoClick;
            frmHome.btnConfrimLocationYes.onClick = onConfirmLocationYesClick;
            frmHome.flxCancelBtn.onTouchEnd = onCancelLocationClick;
            frmHome.flxListMapViewBtn.onTouchEnd = onListMapLocationClick;
            //hamberger menu functions start
            frmHome.flxCloseBtn.onTouchEnd = onCloseMenuClick;
            frmHome.flxMyAccountBtn.onTouchEnd = onMyAccountMenuClick;
            frmHome.flxPastOrdersBtn.onTouchEnd = onPastOrdersMenuClick;
            frmHome.flxPaymentBtn.onTouchEnd = onPaymentMenuClick;
            frmHome.flxSavedRecipesBtn.onTouchEnd = onSavedReciepeMenuClick;
            frmHome.flxSettingsBtn.onTouchEnd = onSettingMenuClick;
            frmHome.flxLogoutBtn.onTouchEnd = onLogoutMenuClick;
            kony.print("Inside Controller");
            //Actual Home sccreen Functions start
            //Header
            frmHome.flxMenuBtn.onTouchEnd = onHeaderMenuClick;
            frmHome.flxCartBtn.onTouchEnd = onHeaderMenuCartClick;
            //Scanner
            frmHome.flxScannerBtn.onTouchEnd = onScannerBtnClick;
            //store coupons
            frmHome.segStoreCoupons.onRowClick = onRowStoreCouponClick;
            kony.print("At the end of Controller");
            //Popup
            frmHome.flxClosePopupBtn.onTouchEnd = onClosePopupClick;
            kony.print("1");
            frmHome.btnPopupActionOne.onClick = onPopupActionOneClick;
            kony.print("2");
            frmHome.btnPopupActionTwo.onClick = onPopupActionTwoClick;
            kony.print("3");
            frmHome.flxPopupOverlay.onTouchEnd = onPopupClick;
        },
        /*
         * Home Init Action
         */
        homeInitfn = function() {
            homeInit.call(this);
            setMapLocation.call(this);
            menuInit.call(this);
            setSearchLocations.call(this);
        };
    /* autentication ..Login service */
    AccountManagementService = function() {
        onToggleRememberCustomerId.call(this);
        var serviceName = "AccountManagement";
        var operationName = "customerData";
        var cid = frmHome.txbUserName.text.trim();
        kony.print("##cid:" + cid);
        var inputParams = {
            "customerid": cid
        }; //"2039200001000003"};
        inputParams.httpheaders = {
            'Content-Type': 'application/json'
        };
        mfintegrationsecureinvokerasync(inputParams, serviceName, operationName, accountInfoSuccessCallback);

        function accountInfoSuccessCallback(status, response) {
            if (response.customerID === "") {
                frmHome.flxValidCustomerIdpopup.setVisibility(true);
                frmHome.txbUserName.text = "";
            } else {
                loginAnimOut.call(this); //alert(status+"enterd success##"+JSON.stringify(response));
                welcomeAnim.call(this);
                frmHome.lblBagCount.text = TotalItems.toString();
                frmHome.lblWelcomeUserName.text = response.firstName;
            }
        }
    };
    /*
     * Home PostShow Action
     */
    homePostShowfn = function() {
        loginAnim.call(this);
        popupInit.call(this);
        clientId = kony.store.getItem("DbCustomerId");
        if (clientId !== null && clientId !== undefined && clientId !== "") {
            frmHome.txbUserName.text = clientId;
            frmHome.rememberMeSwitch.selectedIndex = 0;
        }
    };
    /* Toggle remember me */
    onToggleRememberCustomerId = function() {
        var switchindex = frmHome.rememberMeSwitch.selectedIndex;
        kony.print("##appindex" + switchindex);
        var cid = frmHome.txbUserName.text;
        if (cid === null || cid === undefined) cid = "";
        try {
            switch (switchindex) {
                case 0:
                    kony.store.setItem("DbCustomerId", cid);
                    break;
                case 1:
                    kony.store.setItem("DbCustomerId", "");
                    break;
            }
        } catch (e) {}
    };
    /*
     * Login Button Action
     */
    onLoginBtnClick = function() {
        loginAnimOut.call(this);
        welcomeAnim.call(this);
    };
    /*
     * Forgot Password Action
     */
    onForgotPasswordClick = function() {
        kony.print("call forgot password service here");
    };
    /*
     * Register User
     */
    onRegisterUserClick = function() {
        kony.print("Registration service call here");
    };
    /*
     * Confirm Location No noClicks
     */
    onConfirmLocationNoClick = function() {
        return searchLocation.call(this);
    };
    /*
     * Confirm Location Yes noClicks
     */
    onConfirmLocationYesClick = function() {
        return locationYes.call(this);
    };
    /*
     * Cancel Location noClicks
     */
    onCancelLocationClick = function() {
        return searchLocation.call(this);
    };
    /*
     * List Map Location noClicks
     */
    onListMapLocationClick = function() {
        return mapToggleView.call(this);
    };
    /*
     * Menu cancel noClicks
     */
    onCloseMenuClick = function() {
        return menuToggleOff.call(this);
    };
    /*
     * Menu MyAccount noClicks
     */
    onMyAccountMenuClick = function() {
        menuToggleOff.call(this);
        frmAccount.show();
    };
    /*
     * Menu PastOrders noClicks
     */
    onPastOrdersMenuClick = function() {
        menuToggleOff.call(this);
        frmPastOrders.show();
    };
    /*
     * Menu Payment noClicks
     */
    onPaymentMenuClick = function() {
        menuToggleOff.call(this);
        frmManagePayment.show();
    };
    /*
     * Menu Saved receipt noClicks
     */
    onSavedReciepeMenuClick = function() {
        //no function yet
        return mapToggleView.call(this);
    };
    /*
     * Menu Settings noClicks
     */
    onSettingMenuClick = function() {
        //no function yet 
        return mapToggleView.call(this);
    };
    /*
     * Menu Logout noClicks
     */
    onLogoutMenuClick = function() {
        return logoutAnim.call(this);
    };
    /*
     * Menu Header noClicks
     */
    onHeaderMenuClick = function() {
        return menuToggleOn.call(this);
    };
    /*
     * Menu cart header noClicks
     */
    onHeaderMenuCartClick = function() {
        frmCart.show();
    };
    /*
     * scanner button noClicks
     */
    onScannerBtnClick = function() {
        //
        frmScanOverlay.show();
    };
    /*
     * Store Coupon segment Clicks
     */
    onRowStoreCouponClick = function() {
        return popupToggle.call(this);
    };
    /*
     * popup action one Clicks
     */
    onPopupActionOneClick = function() {
        popupToggle.call(this);
        slideInMessage.call(this);
    };
    /*
     * popup action two Clicks
     */
    onPopupActionTwoClick = function() {
        return locationYes.call(this);
    };
    /*
     * popup action two Clicks
     */
    onPopupClick = function() {
        return popupToggle.call(this);
    };
    /*
     * popup close Clicks
     */
    onClosePopupClick = function() {
        return popupToggle.call(this);
    };
    /*
     * Intialize class
     */
    initialize();
};