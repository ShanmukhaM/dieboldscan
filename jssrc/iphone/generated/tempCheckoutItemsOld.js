function initializetempCheckoutItemsOld() {
    flxCheckoutItemOld = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxCheckoutItemOld",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlexWhie"
    }, {}, {});
    flxCheckoutItemOld.setDefaultUnit(kony.flex.DP);
    var flxProductImage = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "44dp",
        "id": "flxProductImage",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "onClick": AS_FlexContainer_dd8961c0622749e595bc8058f3efc748,
        "skin": "slFbox",
        "top": "12dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxProductImage.setDefaultUnit(kony.flex.DP);
    var imgProduct = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "imgProduct",
        "isVisible": true,
        "skin": "slImage",
        "src": "chicken_breasts.png",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxProductImage.add(imgProduct);
    var lblProductName = new kony.ui.Label({
        "id": "lblProductName",
        "isVisible": true,
        "left": "70dp",
        "maxNumberOfLines": 1,
        "skin": "sknLblRegDblue124",
        "text": "Product Name",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": 12,
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var rtxProductDiscounts = new kony.ui.RichText({
        "height": "25dp",
        "id": "rtxProductDiscounts",
        "isVisible": true,
        "right": 10,
        "skin": "sknLblBoldOrange90",
        "text": "10% OFF–$8.<sup style=\"font-size:20px\">81</sup>",
        "top": "38dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var rtxProductPrice = new kony.ui.RichText({
        "id": "rtxProductPrice",
        "isVisible": true,
        "right": 10,
        "skin": "sknLblBoldDblue124",
        "text": "$59.<sup style=\"font-size:20px\">00</sup>",
        "top": "12dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblItemQty = new kony.ui.Label({
        "id": "lblItemQty",
        "isVisible": true,
        "left": 70,
        "skin": "sknLblRegGrey90",
        "text": "QTY 1",
        "top": 40,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxCheckoutItemOld.add(flxProductImage, lblProductName, rtxProductDiscounts, rtxProductPrice, lblItemQty);
}