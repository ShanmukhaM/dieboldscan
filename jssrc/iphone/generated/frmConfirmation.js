function addWidgetsfrmConfirmation() {
    frmConfirmation.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "8%",
        "id": "flxHeader",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlxWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxBackBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxBackBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxBackBtn.setDefaultUnit(kony.flex.DP);
    var imArrowLeftIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "22dp",
        "id": "imArrowLeftIcon",
        "isVisible": true,
        "left": "5dp",
        "skin": "slImage",
        "src": "chevron_left.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxBackBtn.add(imArrowLeftIcon);
    var flxCheckOut = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxCheckOut",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "slFbox",
        "top": "0dp",
        "width": "80dp",
        "zIndex": 1
    }, {}, {});
    flxCheckOut.setDefaultUnit(kony.flex.DP);
    var lblCheckoutBtn = new kony.ui.Label({
        "centerY": "55%",
        "id": "lblCheckoutBtn",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknLblMdBlack111",
        "text": "Checkout",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxCheckOut.add(lblCheckoutBtn);
    var lblHeaderTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblHeaderTitle",
        "isVisible": true,
        "skin": "sknLblRegDblue136",
        "text": "Payment Verification",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxHeader.add(flxBackBtn, flxCheckOut, lblHeaderTitle);
    var flxPaymentVerification = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "84%",
        "horizontalScrollIndicator": true,
        "id": "flxPaymentVerification",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "8%",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPaymentVerification.setDefaultUnit(kony.flex.DP);
    var lblCreditCardTitle = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblCreditCardTitle",
        "isVisible": true,
        "skin": "sknLblRegDblue124",
        "text": "Your credit card",
        "top": "40dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxPayMethodConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "60dp",
        "id": "flxPayMethodConfirm",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "5dp",
        "width": "60%",
        "zIndex": 1
    }, {}, {});
    flxPayMethodConfirm.setDefaultUnit(kony.flex.DP);
    var flxPaymentInfoContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxPaymentInfoContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPaymentInfoContainer.setDefaultUnit(kony.flex.DP);
    var imgPaymentIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "22dp",
        "id": "imgPaymentIcon",
        "isVisible": true,
        "left": "55dp",
        "skin": "slImage",
        "src": "visa_card.png",
        "width": "32dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblSelectedPaymentMethod = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblSelectedPaymentMethod",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegDblue124",
        "text": "**** 5241",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxPaymentInfoContainer.add(imgPaymentIcon, lblSelectedPaymentMethod);
    flxPayMethodConfirm.add(flxPaymentInfoContainer);
    var lblPurchaseAmountTitle = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblPurchaseAmountTitle",
        "isVisible": true,
        "skin": "sknLblRegDblue124",
        "text": "will be chargerd the total amount of",
        "top": "20dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var rtxPurchaseAmount = new kony.ui.RichText({
        "centerX": "50%",
        "id": "rtxPurchaseAmount",
        "isVisible": true,
        "skin": "sknRtxBlackDblue260",
        "text": "$31.<sup style=\"font-size:33px\">50</sup>",
        "top": "10dp",
        "width": "88%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxPINVerification = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxPINVerification",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "skin": "slFbox",
        "top": "30dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPINVerification.setDefaultUnit(kony.flex.DP);
    var lblPINVerification = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblPINVerification",
        "isVisible": true,
        "skin": "sknLblRegGrey110",
        "text": "Enter your 4 digit PIN code to verify this transaction",
        "top": "0dp",
        "width": "88%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxPINInputs = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxPINInputs",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "32dp",
        "skin": "slFbox",
        "top": "30dp",
        "width": "220dp",
        "zIndex": 1
    }, {}, {});
    flxPINInputs.setDefaultUnit(kony.flex.DP);
    var txbPINNumberOne = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "40dp",
        "id": "txbPINNumberOne",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "left": "0dp",
        "secureTextEntry": false,
        "skin": "sknTxtBrWhite1BlackReg124",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "0dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
        "showClearButton": false,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var txbPINNumberTwo = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "40dp",
        "id": "txbPINNumberTwo",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "left": "20dp",
        "secureTextEntry": false,
        "skin": "sknTxtBrWhite1BlackReg124",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "0dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
        "showClearButton": false,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var txbPINNumberThree = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "40dp",
        "id": "txbPINNumberThree",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "left": "20dp",
        "secureTextEntry": false,
        "skin": "sknTxtBrWhite1BlackReg124",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "0dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
        "showClearButton": false,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var txbPINNumberFour = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "40dp",
        "id": "txbPINNumberFour",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "left": "20dp",
        "secureTextEntry": false,
        "skin": "sknTxtBrWhite1BlackReg124",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "0dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "showClearButton": false,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxPINInputs.add(txbPINNumberOne, txbPINNumberTwo, txbPINNumberThree, txbPINNumberFour);
    flxPINVerification.add(lblPINVerification, flxPINInputs);
    var flxTouchIDVerification = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "130dp",
        "id": "flxTouchIDVerification",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "30dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTouchIDVerification.setDefaultUnit(kony.flex.DP);
    var lblTouchIDVerification = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblTouchIDVerification",
        "isVisible": true,
        "skin": "sknLblRegGrey110",
        "text": "Please verify this transaction with Touch ID",
        "top": "0dp",
        "width": "88%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var imgTouchIDIcon = new kony.ui.Image2({
        "centerX": "50%",
        "height": "44dp",
        "id": "imgTouchIDIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": "touch_id.png",
        "top": "50dp",
        "width": "44dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxTouchIDVerification.add(lblTouchIDVerification, imgTouchIDIcon);
    flxPaymentVerification.add(lblCreditCardTitle, flxPayMethodConfirm, lblPurchaseAmountTitle, rtxPurchaseAmount, flxPINVerification, flxTouchIDVerification);
    var flxReceiptScan = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "flxReceiptScan",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0%",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxReceiptScan.setDefaultUnit(kony.flex.DP);
    var flxPaymentComplete = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "45dp",
        "id": "flxPaymentComplete",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "skin": "slFbox",
        "top": "2%",
        "width": "230dp",
        "zIndex": 1
    }, {}, {});
    flxPaymentComplete.setDefaultUnit(kony.flex.DP);
    var imgCheckIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "22dp",
        "id": "imgCheckIcon",
        "isVisible": true,
        "left": "10dp",
        "skin": "slImage",
        "src": "chekmark_fill_icon.png",
        "top": "0dp",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblPaymentComplete = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblPaymentComplete",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblBoldDblue134",
        "text": "Payment Completed!",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxPaymentComplete.add(imgCheckIcon, lblPaymentComplete);
    var lblPaymentCompletedMessage = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblPaymentCompletedMessage",
        "isVisible": true,
        "skin": "sknLblRegGrey110",
        "text": "Please scan the code bleow at the exit station.",
        "top": "10%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxReceipt = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "23%",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "63%",
        "horizontalScrollIndicator": true,
        "id": "flxReceipt",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxReceipt.setDefaultUnit(kony.flex.DP);
    var imgReceipt = new kony.ui.Image2({
        "centerX": "50%",
        "id": "imgReceipt",
        "isVisible": true,
        "skin": "slImage",
        "src": "receipt_img.png",
        "top": "0%",
        "width": "88%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxReceipt.add(imgReceipt);
    var flxReceiptScanCode = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "centerX": "50%",
        "clipBounds": true,
        "height": "23%",
        "id": "flxReceiptScanCode",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxReceiptScanCode.setDefaultUnit(kony.flex.DP);
    var imgShadow = new kony.ui.Image2({
        "centerX": "50%",
        "id": "imgShadow",
        "isVisible": true,
        "skin": "slImage",
        "src": "shadow.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgScanCode = new kony.ui.Image2({
        "centerX": "50%",
        "id": "imgScanCode",
        "isVisible": true,
        "skin": "slImage",
        "src": "scan_code.png",
        "top": 25,
        "width": "75%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnBackToHomeBtn = new kony.ui.Button({
        "bottom": 15,
        "centerX": "50%",
        "focusSkin": "sknTextBtnBlueTxtRegFocus",
        "height": "40dp",
        "id": "btnBackToHomeBtn",
        "isVisible": true,
        "skin": "sknBtnRegBlue107",
        "text": "Back to home screen",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    flxReceiptScanCode.add(imgShadow, imgScanCode, btnBackToHomeBtn);
    flxReceiptScan.add(flxPaymentComplete, lblPaymentCompletedMessage, flxReceipt, flxReceiptScanCode);
    var flxVerifyPaymentBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "centerX": "50%",
        "clipBounds": true,
        "height": "8%",
        "id": "flxVerifyPaymentBtn",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxBlue",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxVerifyPaymentBtn.setDefaultUnit(kony.flex.DP);
    var lblPayBtn = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblPayBtn",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblBlackWhite121",
        "text": "VERIFY PAYMENT",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxVerifyPaymentBtn.add(lblPayBtn);
    frmConfirmation.add(flxHeader, flxPaymentVerification, flxReceiptScan, flxVerifyPaymentBtn);
};

function frmConfirmationGlobals() {
    frmConfirmation = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmConfirmation,
        "enabledForIdleTimeout": false,
        "id": "frmConfirmation",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFrmWhite",
        "statusBarHidden": true
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": false,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
        "needsIndicatorDuringPostShow": false,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};