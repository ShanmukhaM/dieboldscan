function initializetempCheckoutItemsNew() {
    flxCheckoutItem = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "45dp",
        "id": "flxCheckoutItem",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlexWhie"
    }, {}, {});
    flxCheckoutItem.setDefaultUnit(kony.flex.DP);
    var flxProductInfo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "flxProductInfo",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "17dp",
        "width": "58%",
        "zIndex": 1
    }, {}, {});
    flxProductInfo.setDefaultUnit(kony.flex.DP);
    var lblProductQty = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblProductQty",
        "isVisible": true,
        "left": "10dp",
        "maxNumberOfLines": 1,
        "skin": "sknLblRegDblue124",
        "text": "1",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblProductName = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblProductName",
        "isVisible": true,
        "left": "10dp",
        "maxNumberOfLines": 1,
        "skin": "sknLblRegDblue124",
        "text": "Product Name",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxProductInfo.add(lblProductQty, lblProductName);
    var rtxProductDiscounts = new kony.ui.RichText({
        "centerY": "50%",
        "height": "25dp",
        "id": "rtxProductDiscounts",
        "isVisible": true,
        "right": "25%",
        "skin": "sknLblBoldOrange90",
        "text": "10% OFF",
        "top": "38dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var rtxProductPrice = new kony.ui.RichText({
        "centerY": "50%",
        "id": "rtxProductPrice",
        "isVisible": true,
        "right": 10,
        "skin": "sknLblBoldDblue124",
        "text": "$59.00",
        "top": "12dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxCheckoutItem.add(flxProductInfo, rtxProductDiscounts, rtxProductPrice);
}