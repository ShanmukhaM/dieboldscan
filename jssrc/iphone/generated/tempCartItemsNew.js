function initializetempCartItemsNew() {
    flxCartItem = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90dp",
        "id": "flxCartItem",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxWhite"
    }, {}, {});
    flxCartItem.setDefaultUnit(kony.flex.DP);
    var flxProductImage = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "44dp",
        "id": "flxProductImage",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "onClick": AS_FlexContainer_g93fb1e902cb4d89b1648d1be879bb23,
        "skin": "slFbox",
        "top": "15dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxProductImage.setDefaultUnit(kony.flex.DP);
    var imgProduct = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "imgProduct",
        "isVisible": true,
        "skin": "slImage",
        "src": "close_white_shadow_icon.png",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxProductImage.add(imgProduct);
    var lblProductName = new kony.ui.Label({
        "id": "lblProductName",
        "isVisible": true,
        "left": "70dp",
        "maxNumberOfLines": 1,
        "skin": "sknLblRegDblue124",
        "text": "Product Name",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": 15,
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var rtxProductDiscounts = new kony.ui.RichText({
        "height": "25dp",
        "id": "rtxProductDiscounts",
        "isVisible": true,
        "right": 40,
        "skin": "sknLblBoldOrange90",
        "text": "10% OFF",
        "top": "50dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var rtxProductDiscountsValue = new kony.ui.RichText({
        "height": "25dp",
        "id": "rtxProductDiscountsValue",
        "isVisible": true,
        "right": 10,
        "skin": "sknLblBoldOrange90",
        "text": "–$80",
        "top": "50dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var rtxProductPrice = new kony.ui.RichText({
        "id": "rtxProductPrice",
        "isVisible": true,
        "right": 10,
        "skin": "sknLblBoldDblue124",
        "text": "$59.<sup style=\"font-size:20px\">00</sup>",
        "top": "15dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxQuantity = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 5,
        "clipBounds": true,
        "height": "45dp",
        "id": "flxQuantity",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 70,
        "skin": "slFbox",
        "width": "28%",
        "zIndex": 1
    }, {}, {});
    flxQuantity.setDefaultUnit(kony.flex.DP);
    var flxMinusBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxMinusBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_ea1c6428eb1b46e3a1b6b22d28290d1c,
        "skin": "sknFlxBtn",
        "top": "40dp",
        "width": "40dp",
        "zIndex": 1
    }, {}, {});
    flxMinusBtn.setDefaultUnit(kony.flex.DP);
    var imgMinusIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "22dp",
        "id": "imgMinusIcon",
        "isVisible": true,
        "left": 0,
        "skin": "slImage",
        "src": "minus_icon.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxMinusBtn.add(imgMinusIcon);
    var tbxQtyInput = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "sknTxtRegDblue124",
        "height": "40dp",
        "id": "tbxQtyInput",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "secureTextEntry": false,
        "skin": "sknTxtRegDblue124",
        "text": "1",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "5dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "showClearButton": false,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxPlusBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxPlusBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_a37f50c856ad40efa001c50d839864da,
        "right": 0,
        "skin": "sknFlxBtn",
        "top": "40dp",
        "width": "40dp",
        "zIndex": 1
    }, {}, {});
    flxPlusBtn.setDefaultUnit(kony.flex.DP);
    var imgPlusIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "22dp",
        "id": "imgPlusIcon",
        "isVisible": true,
        "right": 0,
        "skin": "slImage",
        "src": "plus_icon.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxPlusBtn.add(imgPlusIcon);
    flxQuantity.add(flxMinusBtn, tbxQtyInput, flxPlusBtn);
    flxCartItem.add(flxProductImage, lblProductName, rtxProductDiscounts, rtxProductDiscountsValue, rtxProductPrice, flxQuantity);
}