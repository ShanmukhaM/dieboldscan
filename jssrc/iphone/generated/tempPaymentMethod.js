function initializetempPaymentMethod() {
    flxPaymentMethod = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxPaymentMethod",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlexWhie",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPaymentMethod.setDefaultUnit(kony.flex.DP);
    var imgCheckmark = new kony.ui.Image2({
        "centerY": "50%",
        "height": "18dp",
        "id": "imgCheckmark",
        "isVisible": true,
        "right": "10dp",
        "skin": "slImage",
        "src": "tick_icon.png",
        "width": "18dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxPaymentInfoContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxPaymentInfoContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "85%",
        "zIndex": 1
    }, {}, {});
    flxPaymentInfoContainer.setDefaultUnit(kony.flex.DP);
    var imgPaymentIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "22dp",
        "id": "imgPaymentIcon",
        "isVisible": true,
        "left": "10dp",
        "skin": "slImage",
        "src": "visa_card.png",
        "width": "32dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblSelectedPaymentMethod = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblSelectedPaymentMethod",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegDblue124",
        "text": "**** 5241",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var imgStarIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "18dp",
        "id": "imgStarIcon",
        "isVisible": true,
        "left": "10dp",
        "skin": "slImage",
        "src": "star_icon.png",
        "width": "18dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxPaymentInfoContainer.add(imgPaymentIcon, lblSelectedPaymentMethod, imgStarIcon);
    flxPaymentMethod.add(imgCheckmark, flxPaymentInfoContainer);
}