//startup.js file
var globalhttpheaders = {};
var appConfig = {
    appId: "dnretail",
    appName: "dnretail",
    appVersion: "1.0.0",
    platformVersion: null,
    serverIp: "10.20.17.46",
    serverPort: "80",
    secureServerPort: "443",
    isDebug: false,
    middlewareContext: "dnretail",
    isturlbase: "https://diebold.konylabs.net/services",
    isMFApp: true,
    appKey: "3f86aa681f18543cdd5f63170edc3b35",
    appSecret: "7fe15ccb5270dd17808f5a8cc3e537a4",
    serviceUrl: "https://diebold.konylabs.net/authService/100000002/appconfig",
    svcDoc: {
        "appId": "38109634-7ca4-453b-a725-e2fcb7bc8c04",
        "baseId": "15128819-062f-4882-a9ec-d2ad40d88553",
        "name": "ScanGoTesting",
        "selflink": "https://diebold.konylabs.net/authService/100000002/appconfig",
        "integsvc": {
            "BasketCalculationTest": "https://diebold.konylabs.net/services/BasketCalculationTest"
        },
        "reportingsvc": {
            "custom": "https://diebold.konylabs.net/services/CMS",
            "session": "https://diebold.konylabs.net/services/IST"
        },
        "services_meta": {
            "BasketCalculationTest": {
                "version": "1.0",
                "url": "https://diebold.konylabs.net/services/BasketCalculationTest",
                "type": "integsvc"
            }
        }
    },
    svcDocRefresh: false,
    svcDocRefreshTimeSecs: -1,
    eventTypes: ["FormEntry", "ServiceRequest", "Error", "Crash"],
    url: "https://diebold.konylabs.net/admin/dnretail/MWServlet",
    secureurl: "https://diebold.konylabs.net/admin/dnretail/MWServlet"
};
sessionID = "";

function appInit(params) {
    skinsInit();
    kony.application.setCheckBoxSelectionImageAlignment(constants.CHECKBOX_SELECTION_IMAGE_ALIGNMENT_RIGHT);
    kony.application.setDefaultTextboxPadding(false);
    kony.application.setRespectImageSizeForImageWidgetAlignment(true);
    initializeUserWidgets();
    initializetempCartItemsNew();
    initializetempCartItemsOld();
    initializetempCheckoutItemsNew();
    initializetempCheckoutItemsOld();
    initializetempCoupons();
    initializetempListTitle();
    initializetempLocation();
    initializetempPastOrders();
    initializetempPayment();
    initializetempPaymentMethod();
    initializetempProductDetails();
    initializetempProductImages();
    frmAccountGlobals();
    frmCartGlobals();
    frmCheckoutGlobals();
    frmConfirmationGlobals();
    frmHomeGlobals();
    frmManagePaymentGlobals();
    frmPastOrderDetailsGlobals();
    frmPastOrdersGlobals();
    frmPaymentMethodGlobals();
    frmProductDetailGlobals();
    frmScanOverlayGlobals();
    setAppBehaviors();
};

function setAppBehaviors() {
    kony.application.setApplicationBehaviors({
        applyMarginPaddingInBCGMode: false,
        adherePercentageStrictly: true,
        retainSpaceOnHide: true,
        marginsIncludedInWidgetContainerWeight: true,
        APILevel: 7300
    })
};

function themeCallBack() {
    initializeGlobalVariables();
    callAppMenu();
    kony.application.setApplicationInitializationEvents({
        init: appInit,
        postappinit: AS_AppEvents_cb52ec8fc0fe43928b5ff008b074ef1c,
        showstartupform: function() {
            frmHome.show();
        }
    });
};

function loadResources() {
    globalhttpheaders = {};
    sdkInitConfig = {
        "appConfig": appConfig,
        "isMFApp": appConfig.isMFApp,
        "appKey": appConfig.appKey,
        "appSecret": appConfig.appSecret,
        "eventTypes": appConfig.eventTypes,
        "serviceUrl": appConfig.serviceUrl
    }
    kony.setupsdks(sdkInitConfig, onSuccessSDKCallBack, onSuccessSDKCallBack);
};

function onSuccessSDKCallBack() {
    kony.theme.setCurrentTheme("default", themeCallBack, themeCallBack);
}
kony.application.setApplicationMode(constants.APPLICATION_MODE_NATIVE);
//If default locale is specified. This is set even before any other app life cycle event is called.
loadResources();
kony.print = function() {
    return;
};