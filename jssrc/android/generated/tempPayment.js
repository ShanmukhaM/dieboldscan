function initializetempPayment() {
    flxPayment = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxPayment",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlexWhie",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPayment.setDefaultUnit(kony.flex.DP);
    var flxPaymentInfoContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxPaymentInfoContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "85%",
        "zIndex": 1
    }, {}, {});
    flxPaymentInfoContainer.setDefaultUnit(kony.flex.DP);
    var CopyimgPaymentIcon0a0be185320fd40 = new kony.ui.Image2({
        "centerY": "50%",
        "height": "22dp",
        "id": "CopyimgPaymentIcon0a0be185320fd40",
        "isVisible": true,
        "left": "10dp",
        "skin": "slImage",
        "src": "visa_card.png",
        "width": "32dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var CopylblSelectedPaymentMethod0fa9bf54f150544 = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblSelectedPaymentMethod0fa9bf54f150544",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknDarkSmTxtMd",
        "text": "**** 5241",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyimgStarIcon0ae41901c564844 = new kony.ui.Image2({
        "centerY": "50%",
        "height": "18dp",
        "id": "CopyimgStarIcon0ae41901c564844",
        "isVisible": true,
        "left": "10dp",
        "skin": "slImage",
        "src": "star_icon.png",
        "width": "18dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxPaymentInfoContainer.add(CopyimgPaymentIcon0a0be185320fd40, CopylblSelectedPaymentMethod0fa9bf54f150544, CopyimgStarIcon0ae41901c564844);
    flxPayment.add(flxPaymentInfoContainer);
}