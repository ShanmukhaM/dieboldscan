//actions.js file 
function AS_AppEvents_cb52ec8fc0fe43928b5ff008b074ef1c(eventobject) {
    return konypostShow.call(this);
}

function AS_Button_a66e509c4b8a4ff2b9d0bf81e86b1120(eventobject) {
    popupToggle.call(this);
    slideInMessage.call(this);
}

function AS_Button_aed4cf7350834224a6a6bbeb834eb475(eventobject) {
    return searchLocation.call(this);
}

function AS_Button_b721e14f09494a869977b60a6dd07409(eventobject) {
    return locationYes.call(this);
}

function AS_Button_bcbe6e3dd6ee418bbfbcd39f47cf11e9(eventobject) {
    return locationYes.call(this);
}

function AS_Button_c2a3e610df804defb66e05b05a8bb3ec(eventobject) {
    return locationYes.call(this);
}

function AS_Button_c2c8cb23cec34335a87df4f6458c2eb2(eventobject, context) {
    return slideInMessage.call(this);
}

function AS_Button_c6fd9338f9734d2a9d644844f195da85(eventobject, context) {
    return slideInMessage.call(this);
}

function AS_Button_c8931e33e68f458e9c823d84869e3937(eventobject) {
    return searchLocation.call(this);
}

function AS_Button_cee29bd68b044115ac3091e18bc515e2(eventobject) {
    return popupToggle.call(this);
}

function AS_Button_cee4aa9b42e241ff83dfdd6a60ef17e2(eventobject) {
    return searchLocation.call(this);
}

function AS_Button_d50da29a7a174cc4a2d191ca1e3d6086(eventobject) {
    return locationYes.call(this);
}

function AS_Button_dcdeef6c8a72425995a8b8ee86fd5d18(eventobject) {
    popupToggle.call(this);
    slideInMessage.call(this);
}

function AS_Button_dfbcf0c18fbe40b7b60c772c2ccf6522(eventobject) {
    return popupToggle.call(this);
}

function AS_Button_e231553e52054297ba46def3f3829b6d(eventobject) {
    return locationYes.call(this);
}

function AS_Button_e2fd2e7d4b674521939c4a752e1e1808(eventobject) {
    return searchLocation.call(this);
}

function AS_Button_e97421e215e14c0db4c15797b4a3a2cd(eventobject, context) {
    return slideInMessage.call(this);
}

function AS_Button_eac745ea5ba54e6bb2e86e7666338131(eventobject) {
    searchLocation.call(this);
    locationSelected.call(this);
}

function AS_Button_ef2d9259a4aa41e2aa35e7dec69d57dc(eventobject, context) {
    frmHome.show();
}

function AS_Button_ef631071071d49e696c4a75e122a81cd(eventobject) {
    return locationYes.call(this);
}

function AS_Button_f23df423702246f0919498bbc01f9e41(eventobject) {
    searchLocation.call(this);
    locationSelected.call(this);
}

function AS_Button_f968b669adfd430ab69b198a75b1042e(eventobject) {
    return searchLocation.call(this);
}

function AS_Button_ic5ae13e37d54beab15229ed9228c85f(eventobject, context) {
    return slideInMessage.call(this);
}

function AS_Button_j9195078822144729eecd3c08acbab17(eventobject, context) {
    return slideInMessage.call(this);
}

function AS_Button_ja9be7e4d3f848cc8b10b42521eac0ee(eventobject) {
    return locationYes.call(this);
}

function AS_Button_jf3820e519ec40db95728441f0c4dd7c(eventobject) {}

function AS_FlexContainer_a016ed172daa4e0fa28988811bb78d8b(eventobject) {
    return paymentCompleted.call(this);
}

function AS_FlexContainer_a057decad2cc4d28acd904ec1535e4d6(eventobject, x, y) {}

function AS_FlexContainer_a37f50c856ad40efa001c50d839864da(eventobject, context) {
    return AddItem.call(this);
}

function AS_FlexContainer_a81bab69314e4f51952ce977bcf198c5(eventobject) {
    return popupToggle.call(this);
}

function AS_FlexContainer_a8e755b5c97342b8af3d4e809cd39cf3(eventobject) {
    frmCart.show();
}

function AS_FlexContainer_a933ade9c4674ca3b26208d0fcf728ae(eventobject) {
    return pastOrderReceipt.call(this);
}

function AS_FlexContainer_ac6dc8f0fcdd4e43add159b75d38a692(eventobject) {
    return backBtn.call(this);
}

function AS_FlexContainer_b210e19d4270496c8f5af95dd201c711(eventobject) {
    scanItemService.call(this, null);
    slideInRecipe.call(this);
    scannedItem.call(this);
}

function AS_FlexContainer_b3393f5260594c2bb40a0046449cd7e2(eventobject) {
    undefined.show();
}

function AS_FlexContainer_b34d2e796fd04379bdf207659fc6c57f(eventobject) {}

function AS_FlexContainer_b493547dabef435a98d7d72a388e6caf(eventobject) {
    loginAnimOut.call(this);
    welcomeAnim.call(this);
}

function AS_FlexContainer_b575a48452994bb0b05bd051be770d0b(eventobject) {
    return backBtn.call(this);
}

function AS_FlexContainer_b5f648a567e5445f9943bdb317f0cc1f(eventobject) {
    popupToggle.call(this);
    frmConfirmation.show();
}

function AS_FlexContainer_b8ab6292f80b439eabd7a36d87c01095(eventobject) {
    popupToggle.call(this);
}

function AS_FlexContainer_be7c32ec57c442bf8f7e2dd3a2fd1f1e(eventobject) {
    return logoutAnim.call(this);
}

function AS_FlexContainer_bf909f460b7f49c6ab56ca48f98b060c(eventobject) {
    return backBtn.call(this);
}

function AS_FlexContainer_c048a6f3f7ef40e6996d8e1a0696fe93(eventobject) {
    frmCheckout.show();
}

function AS_FlexContainer_c16403ff08fd44ffb9f2ec1bce40e29b(eventobject) {
    frmCheckout.show();
}

function AS_FlexContainer_c2b9865169934283a2b4a622b32f1b74(eventobject) {
    loginAnimOut.call(this);
    welcomeAnim.call(this);
}

function AS_FlexContainer_c4eae43f38884cd2a1dd87bea7a4bbc3(eventobject) {
    return popupToggle.call(this);
}

function AS_FlexContainer_c830d5c0d8cd4ccd82bf3f7c9f5d8db7(eventobject) {
    frmCart.show();
}

function AS_FlexContainer_c8d9d8b41c214413bbbb053486634530(eventobject, context) {
    function SHOW_ALERT__c72270bf7f7f4fad8d8e8772e8771e75_True() {}

    function SHOW_ALERT__c72270bf7f7f4fad8d8e8772e8771e75_Callback() {
        SHOW_ALERT__c72270bf7f7f4fad8d8e8772e8771e75_True()
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_INFO,
        "alertTitle": "Plus",
        "yesLabel": "Ok",
        "message": "You added one more to your cart",
        "alertHandler": SHOW_ALERT__c72270bf7f7f4fad8d8e8772e8771e75_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    })
}

function AS_FlexContainer_cc3903703a86460191f59edf4db0b82c(eventobject) {
    return menuToggleOn.call(this);
}

function AS_FlexContainer_cf016980ac1b4073a4c68c5d5a1b870b(eventobject) {
    frmCart.show();
}

function AS_FlexContainer_cfa4cea76ac14c51b3f772117e2dc834(eventobject, context) {
    frmProductDetail.show();
}

function AS_FlexContainer_d014cbc8bb35420492e010de25e3ea93(eventobject) {
    frmPaymentMethod.show();
}

function AS_FlexContainer_d2cf3bfbf0e54ce2ab1e7930d3369b60(eventobject) {
    return searchLocation.call(this);
}

function AS_FlexContainer_d5ab43f48ef2464ab0037ac800462f12(eventobject) {
    return popupToggle.call(this);
}

function AS_FlexContainer_d8697b44891b4b8eaf2527244c6b1ab5(eventobject) {
    return popupToggle.call(this);
}

function AS_FlexContainer_d94124e5db474d13b6bced9ac850e2d8(eventobject) {
    return menuToggleOff.call(this);
}

function AS_FlexContainer_dbc666d7bcaa4545b034b2a2c0de6df7(eventobject) {
    menuToggleOff.call(this);
    frmManagePayment.show();
}

function AS_FlexContainer_dd8961c0622749e595bc8058f3efc748(eventobject, context) {
    frmProductDetail.show();
}

function AS_FlexContainer_de157aee8160488792fdd89c569d221d(eventobject) {
    return popupToggle.call(this);
}

function AS_FlexContainer_e02a57a19dba42e59b12054748bc0a37(eventobject) {
    return popupToggle.call(this);
}

function AS_FlexContainer_e5b44515f17f4bf687fecfc4158fc040(eventobject) {
    frmCart.show();
}

function AS_FlexContainer_e638c56bdaf342f588c6f528c79f4aef(eventobject) {
    return mapToggleView.call(this);
}

function AS_FlexContainer_e6d3a53b055b44a3bba46d2c9ee8f380(eventobject) {
    popupToggle.call(this);
}

function AS_FlexContainer_e6f3011cbbd349fe8a903e8e2b23906f(eventobject) {
    return menuToggleOff.call(this);
}

function AS_FlexContainer_e7fc038efc614f66821d01547e071e36(eventobject) {
    frmCheckout.show();
}

function AS_FlexContainer_e9d0b357fb1740c09762b71d2f41178c(eventobject) {
    frmPaymentMethod.show();
}

function AS_FlexContainer_ea1c6428eb1b46e3a1b6b22d28290d1c(eventobject, context) {
    return RemoveItem.call(this);
}

function AS_FlexContainer_ebb37da6ad474fd7b054968b62830c7c(eventobject) {
    frmCheckout.show();
}

function AS_FlexContainer_ebced3bdfbed4194af19ec5af5bd50ff(eventobject) {
    frmScanOverlay.show();
}

function AS_FlexContainer_ec9c3417123a4bf9b99f9afe19877f90(eventobject) {
    return backBtn.call(this);
}

function AS_FlexContainer_ecc3f241cba843dcb4c34221884b2072(eventobject) {
    menuToggleOff.call(this);
    frmPastOrders.show();
}

function AS_FlexContainer_eee5a11004ff470e8e584da290f71c7f(eventobject) {
    return menuToggleOff.call(this);
}

function AS_FlexContainer_f5003d34780b4032a6a5850a45098251(eventobject) {
    popupToggle.call(this);
    frmConfirmation.show();
}

function AS_FlexContainer_f6ce8bf5f6884383b337008acfb18601(eventobject) {
    return backBtn.call(this);
}

function AS_FlexContainer_fb06543c1d9548bd9784de1f1ecbfb51(eventobject) {
    return popupToggle.call(this);
}

function AS_FlexContainer_fb75c7b258ca43739bdcd7f38e5305d7(eventobject) {
    return mapToggleView.call(this);
}

function AS_FlexContainer_fb922d641e5f4b239946c6c24b48bb8b(eventobject) {
    return popupToggle.call(this);
}

function AS_FlexContainer_fc55ebd82da9492db8337c2f930000b4(eventobject) {
    menuToggleOff.call(this);
    frmPastOrders.show();
}

function AS_FlexContainer_g0268ea2b31f45a4aa253ec051e2d46e(eventobject) {
    frmPaymentMethod.show();
}

function AS_FlexContainer_g93fb1e902cb4d89b1648d1be879bb23(eventobject, context) {
    frmProductDetail.show();
}

function AS_FlexContainer_ga8566f8c4df4417b704d11fd369aae6(eventobject) {
    return menuToggleOff.call(this);
}

function AS_FlexContainer_gbd4e50671cd419289cefe37b4133a96(eventobject) {
    frmCart.show();
}

function AS_FlexContainer_gdff3dc60e7e4606860109bfa803284d(eventobject) {
    frmScanOverlay.show();
}

function AS_FlexContainer_h1a8d61a514546918bfbf4b10fc57224(eventobject) {
    frmCheckout.show();
}

function AS_FlexContainer_h2061fe975e0440dbf1e91557f97e326(eventobject) {
    return pastOrderReceipt.call(this);
}

function AS_FlexContainer_h21e812d7e8c4d0484917209e1e901da(eventobject) {
    frmCart.show();
}

function AS_FlexContainer_h8b3ac18e84b4b0f8591951f5b283d81(eventobject) {
    frmHome.show();
}

function AS_FlexContainer_hb925012b79e4c9dba9bb093588b30f0(eventobject) {
    undefined.show();
}

function AS_FlexContainer_hb9ad12056f84e8f865c57999239d6bf(eventobject) {
    frmCart.show();
}

function AS_FlexContainer_hbc10849f5194346bf46cc953739a271(eventobject) {
    return popupToggle.call(this);
}

function AS_FlexContainer_i4ae4949380b4d83b16d8ae06c2f6829(eventobject) {
    return pastOrderReceipt.call(this);
}

function AS_FlexContainer_i4b47d757aad4c0f90f0db3113c1ee3c(eventobject) {
    return logoutAnim.call(this);
}

function AS_FlexContainer_i5fa6c405ffd47d4ac7fb81d5e0b4910(eventobject) {
    popupToggle.call(this);
}

function AS_FlexContainer_ic54a60dbcbd4a129aa005c3e1daac75(eventobject) {
    frmCart.show();
}

function AS_FlexContainer_ie0061f65f204b9a8947b8be186dea60(eventobject) {
    return popupToggle.call(this);
}

function AS_FlexContainer_ie68713e067946caa2a3e0292d8c30cd(eventobject) {
    return searchLocation.call(this);
}

function AS_FlexContainer_j557577e3b564dfebe139fec976e1943(eventobject) {
    return popupToggle.call(this);
}

function AS_FlexContainer_j5b16d485c2643fa9452f6f747dba926(eventobject) {
    frmPastOrders.show();
}

function AS_FlexContainer_ja3cd1a96f7c462ab91af7be944456d0(eventobject) {
    menuToggleOff.call(this);
    frmManagePayment.show();
}

function AS_FlexContainer_jed5bcfbe59845f6acf9c0f752917276(eventobject) {
    frmHome.show();
}

function AS_FlexContainer_jed6008d751d40c1a933f4a3f2eb0b7a(eventobject) {
    return menuToggleOn.call(this);
}

function AS_FlexContainer_jef45f9d368f437a97b54e987d4bcbca(eventobject, context) {
    return AddItem.call(this);
}

function AS_FlexContainer_jeff017dd7c1433b8fc7cc2ce091a04f(eventobject) {
    frmPaymentMethod.show();
}

function AS_FlexContainer_jf73f76b8f1d4eba81b0f293cb24fdd5(eventobject) {
    menuToggleOff.call(this);
    frmAccount.show();
}

function AS_Form_a116973ab3574653929a52aeb3957b95(eventobject) {
    return pastOrderInit.call(this);
}

function AS_Form_ab7824c5d34e42aa808f78b6242f7221(eventobject) {
    return popupInit.call(this);
}

function AS_Form_b379d7284755461e87d11a8238eae42a(eventobject) {
    return popupInit.call(this);
}

function AS_Form_d260e5d2aab44375b644d850e5b7f23a(eventobject) {
    return popupInit.call(this);
}

function AS_Form_d71c5555973b4a6d8353c329e7819dfe(eventobject) {
    loginAnim.call(this);
    popupInit.call(this);
}

function AS_Form_dd15c020160b4fefb06aa282dee7052e(eventobject) {
    return pageSkinTransp.call(this);
}

function AS_Form_df700be88bcb43d39ac5b3ed7951dfbd(eventobject) {
    return konypostShow.call(this);
}

function AS_Form_e035765316b2407eb3c773dbe5cc0b81(eventobject) {
    homeInit.call(this);
    setMapLocation.call(this);
    menuInit.call(this);
    setSearchLocations.call(this);
}

function AS_Form_e1c189e6f3d74142b9eb4c9fc7f3337b(eventobject) {}

function AS_Form_ea3f5216ec1f4d1b8f22177f1e4ebec6(eventobject) {}

function AS_Form_ec459791567e43aa847750ccd4142593(eventobject) {
    return scanInit.call(this);
}

function AS_Form_ec7a164736644644afae0881a0c7b6d1(eventobject) {
    return popupInit.call(this);
}

function AS_Form_f7761fc163994d69878fc31035a87cbc(eventobject) {
    return paymentCompleted.call(this);
}

function AS_Form_f9da0b8f1ceb46d4837801b04b67f2ae(eventobject) {
    return slideInMessageInit.call(this);
}

function AS_Form_g6b55da8bada4c46bd189c5209124ab3(eventobject) {}

function AS_Form_h6e651da60a14f6c932c86660778931a(eventobject) {}

function AS_Form_j2aa929bee6b4c19b11a892a1262f9a3(eventobject) {
    return confirmationInit.call(this);
}

function AS_Map_a85780c427a049b4af6e639734b770da(eventobject, location) {
    return mapLocationPopup.call(this);
}

function AS_Map_c261f0e7cc7748b38c1126dfe8f0bebc(eventobject, location) {
    return mapLocationPopup.call(this);
}

function AS_Map_d05c8333ad1942edae084ffc57610177(eventobject, location) {}

function AS_Segment_a5c9330060d24816b3bf28ca7ad0cfd4(eventobject, sectionNumber, rowNumber) {
    searchLocation.call(this);
    locationSelected.call(this);
}

function AS_Segment_aba01a74c49b4ea0887fc18632ef9056(eventobject, sectionNumber, rowNumber) {}

function AS_Segment_b1713ef5661d469cb918ef69a565b523(eventobject, sectionNumber, rowNumber) {
    frmPastOrderDetails.show();
}

function AS_Segment_c8e8535d717146d68de08c1d05723373(eventobject, sectionNumber, rowNumber) {
    searchLocation.call(this);
    locationSelected.call(this);
}

function AS_Segment_d37cefc876f54e89935588ce698977a7(eventobject, sectionNumber, rowNumber) {
    return popupToggle.call(this);
}

function AS_Segment_d92bd32522194c4e9bceb1be358df223(eventobject, sectionNumber, rowNumber) {
    return popupToggle.call(this);
}

function AS_Segment_e018f1f7c41b45dbb9d4c0b80e52977f(eventobject, sectionNumber, rowNumber) {
    return AddValuesToCart.call(this);
}

function AS_Segment_f8b4a84b141e43cdb76fa0f82ef24b0b(eventobject, sectionNumber, rowNumber) {
    return AddValuesToCart.call(this);
}

function popupclose_frmHome(eventobject) {
    return AS_Button_b63e9a6ff8274d9b9373d38298bfdae5(eventobject);
}

function AS_Button_b63e9a6ff8274d9b9373d38298bfdae5(eventobject) {
    frmHome.flxValidCustomerIdpopup.setVisibility(false);
}