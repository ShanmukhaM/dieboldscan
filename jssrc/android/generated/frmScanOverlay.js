function addWidgetsfrmScanOverlay() {
    frmScanOverlay.setDefaultUnit(kony.flex.DP);
    var flxSlideInMessage = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100dp",
        "id": "flxSlideInMessage",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknSlideInMessage",
        "top": "-16%",
        "width": "100%",
        "zIndex": 3
    }, {}, {});
    flxSlideInMessage.setDefaultUnit(kony.flex.DP);
    var flxRecipeImg = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "44dp",
        "id": "flxRecipeImg",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "15%",
        "skin": "sknRoundCornerBox",
        "top": "8dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxRecipeImg.setDefaultUnit(kony.flex.DP);
    var imgRecipe = new kony.ui.Image2({
        "height": "100%",
        "id": "imgRecipe",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "chicken_parm.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxRecipeImg.add(imgRecipe);
    var lblSlideInMessage = new kony.ui.Label({
        "id": "lblSlideInMessage",
        "isVisible": true,
        "left": "29%",
        "maxNumberOfLines": 2,
        "skin": "sknWhiteSmTxtReg",
        "text": "Try this chicken parmesan recipe!",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": 20,
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxDiscardBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "40dp",
        "id": "flxDiscardBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "right": 0,
        "skin": "sknSecondaryBtnWhiteBorder",
        "width": "49.80%",
        "zIndex": 1
    }, {}, {});
    flxDiscardBtn.setDefaultUnit(kony.flex.DP);
    var lblCloseBtnSlideMessage = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblCloseBtnSlideMessage",
        "isVisible": true,
        "skin": "sknWhiteSmTxtReg",
        "text": "No Thanks",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDiscardBtn.add(lblCloseBtnSlideMessage);
    var flxSaveRecipetBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "40dp",
        "id": "flxSaveRecipetBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "sknSecondaryBtnWhiteBorder",
        "width": "49.80%",
        "zIndex": 1
    }, {}, {});
    flxSaveRecipetBtn.setDefaultUnit(kony.flex.DP);
    var CopylblCloseBtnSlideMessage0b988302a3f544b = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "CopylblCloseBtnSlideMessage0b988302a3f544b",
        "isVisible": true,
        "skin": "sknWhiteSmTxtReg",
        "text": "Save Recipe",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSaveRecipetBtn.add(CopylblCloseBtnSlideMessage0b988302a3f544b);
    flxSlideInMessage.add(flxRecipeImg, lblSlideInMessage, flxDiscardBtn, flxSaveRecipetBtn);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "8%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxCloseBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxCloseBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "slFbox",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxCloseBtn.setDefaultUnit(kony.flex.DP);
    var imgCloseIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "22dp",
        "id": "imgCloseIcon",
        "isVisible": true,
        "left": "10dp",
        "skin": "slImage",
        "src": "close_white_shadow_icon.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxCloseBtn.add(imgCloseIcon);
    flxHeader.add(flxCloseBtn);
    var flxScannOverlay = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxScannOverlay",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknScannerOverlay",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxScannOverlay.setDefaultUnit(kony.flex.DP);
    var imgScanOverlay = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "35%",
        "height": "230dp",
        "id": "imgScanOverlay",
        "isVisible": true,
        "skin": "slImage",
        "src": "scanner_overlay.png",
        "width": "230dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxScannOverlay.add(imgScanOverlay);
    var flxScannedItemsContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "25%",
        "id": "flxScannedItemsContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlxDblue",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxScannedItemsContainer.setDefaultUnit(kony.flex.DP);
    var lblRecentlyScannedItems = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblRecentlyScannedItems",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBoldWhite107",
        "text": "RECENTLY SCANNED PRODUCTS",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxRecentlyScannedItems = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": true,
        "allowVerticalBounce": false,
        "bottom": 0,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "85%",
        "horizontalScrollIndicator": true,
        "id": "flxRecentlyScannedItems",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_HORIZONTAL,
        "skin": "slFSbox",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxRecentlyScannedItems.setDefaultUnit(kony.flex.DP);
    var flxRecentlyScannedItemTwo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 5,
        "clipBounds": false,
        "height": "88%",
        "id": "flxRecentlyScannedItemTwo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "skin": "sknWhiteBgRndCornerBox",
        "width": "55%",
        "zIndex": 1
    }, {}, {});
    flxRecentlyScannedItemTwo.setDefaultUnit(kony.flex.DP);
    var CopylblRecentProductName0e6e6c79458dd44 = new kony.ui.Label({
        "id": "CopylblRecentProductName0e6e6c79458dd44",
        "isVisible": true,
        "left": "65dp",
        "maxNumberOfLines": 2,
        "skin": "sknLblRegDblue124",
        "text": "Salad Mix, Spring Mix",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "10dp",
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyflxProductImage0e3a28557888e4e = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "44dp",
        "id": "CopyflxProductImage0e3a28557888e4e",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "skin": "slFbox",
        "top": "10dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    CopyflxProductImage0e3a28557888e4e.setDefaultUnit(kony.flex.DP);
    var CopyimgProduct0ia713de566ce40 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "CopyimgProduct0ia713de566ce40",
        "isVisible": true,
        "skin": "slImage",
        "src": "salad_mix_bag.png",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    CopyflxProductImage0e3a28557888e4e.add(CopyimgProduct0ia713de566ce40);
    var CopyflxQuantity0i8d384dc72e846 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "60dp",
        "id": "CopyflxQuantity0i8d384dc72e846",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyflxQuantity0i8d384dc72e846.setDefaultUnit(kony.flex.DP);
    var CopytbxQtyInput0gd483ec0d8ef40 = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "sknRoundInputFocus",
        "height": "40dp",
        "id": "CopytbxQtyInput0gd483ec0d8ef40",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "secureTextEntry": false,
        "skin": "sknTxtRegDblue124",
        "text": "2",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "5dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var CopyflxMinusBtn0i2f3ea0304e94d = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "CopyflxMinusBtn0i2f3ea0304e94d",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "skin": "sknFlxBtn",
        "top": "40dp",
        "width": "40dp",
        "zIndex": 1
    }, {}, {});
    CopyflxMinusBtn0i2f3ea0304e94d.setDefaultUnit(kony.flex.DP);
    var CopyimgMinusIcon0b85497bfcb6848 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "22dp",
        "id": "CopyimgMinusIcon0b85497bfcb6848",
        "isVisible": true,
        "skin": "slImage",
        "src": "minus_icon.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    CopyflxMinusBtn0i2f3ea0304e94d.add(CopyimgMinusIcon0b85497bfcb6848);
    var CopyflxPlusBtn0jfbe960e8b6f4a = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "CopyflxPlusBtn0jfbe960e8b6f4a",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 10,
        "skin": "sknFlxBtn",
        "top": "40dp",
        "width": "40dp",
        "zIndex": 1
    }, {}, {});
    CopyflxPlusBtn0jfbe960e8b6f4a.setDefaultUnit(kony.flex.DP);
    var CopyimgPlusIcon0d3d954c848db4a = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "22dp",
        "id": "CopyimgPlusIcon0d3d954c848db4a",
        "isVisible": true,
        "skin": "slImage",
        "src": "plus_icon.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    CopyflxPlusBtn0jfbe960e8b6f4a.add(CopyimgPlusIcon0d3d954c848db4a);
    CopyflxQuantity0i8d384dc72e846.add(CopytbxQtyInput0gd483ec0d8ef40, CopyflxMinusBtn0i2f3ea0304e94d, CopyflxPlusBtn0jfbe960e8b6f4a);
    flxRecentlyScannedItemTwo.add(CopylblRecentProductName0e6e6c79458dd44, CopyflxProductImage0e3a28557888e4e, CopyflxQuantity0i8d384dc72e846);
    var flxRecentlyScannedItemOne = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 5,
        "clipBounds": false,
        "height": "88%",
        "id": "flxRecentlyScannedItemOne",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "skin": "sknWhiteBgRndCornerBox",
        "width": "55%",
        "zIndex": 1
    }, {}, {});
    flxRecentlyScannedItemOne.setDefaultUnit(kony.flex.DP);
    var lblRecentProductName = new kony.ui.Label({
        "id": "lblRecentProductName",
        "isVisible": true,
        "left": "65dp",
        "maxNumberOfLines": 2,
        "skin": "sknLblRegDblue124",
        "text": "Boneless Chicken Breast",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "10dp",
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxProductImage = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "44dp",
        "id": "flxProductImage",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "skin": "slFbox",
        "top": "10dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxProductImage.setDefaultUnit(kony.flex.DP);
    var imgProduct = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "imgProduct",
        "isVisible": true,
        "skin": "slImage",
        "src": "chicken_breasts.png",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxProductImage.add(imgProduct);
    var flxQuantity = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxQuantity",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxQuantity.setDefaultUnit(kony.flex.DP);
    var tbxQtyInput = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "sknRoundInputFocus",
        "height": "40dp",
        "id": "tbxQtyInput",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "secureTextEntry": false,
        "skin": "sknTxtRegDblue124",
        "text": "1",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "5dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxMinusBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxMinusBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "skin": "sknFlxBtn",
        "top": "40dp",
        "width": "40dp",
        "zIndex": 1
    }, {}, {});
    flxMinusBtn.setDefaultUnit(kony.flex.DP);
    var imgMinusIcon = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "22dp",
        "id": "imgMinusIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": "minus_icon.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxMinusBtn.add(imgMinusIcon);
    var flxPlusBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxPlusBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 10,
        "skin": "sknFlxBtn",
        "top": "40dp",
        "width": "40dp",
        "zIndex": 1
    }, {}, {});
    flxPlusBtn.setDefaultUnit(kony.flex.DP);
    var imgPlusIcon = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "22dp",
        "id": "imgPlusIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": "plus_icon.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxPlusBtn.add(imgPlusIcon);
    flxQuantity.add(tbxQtyInput, flxMinusBtn, flxPlusBtn);
    flxRecentlyScannedItemOne.add(lblRecentProductName, flxProductImage, flxQuantity);
    flxRecentlyScannedItems.add(flxRecentlyScannedItemTwo, flxRecentlyScannedItemOne);
    flxScannedItemsContainer.add(lblRecentlyScannedItems, flxRecentlyScannedItems);
    var flxCameraBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "28%",
        "centerX": "50%",
        "clipBounds": true,
        "height": "80dp",
        "id": "flxCameraBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "17dp",
        "skin": "sknBtnCameraTranspWhite",
        "width": "80dp",
        "zIndex": 3
    }, {}, {});
    flxCameraBtn.setDefaultUnit(kony.flex.DP);
    var flxInnerCameraBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxInnerCameraBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknBtnCameraWhite",
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    flxInnerCameraBtn.setDefaultUnit(kony.flex.DP);
    flxInnerCameraBtn.add();
    flxCameraBtn.add(flxInnerCameraBtn);
    frmScanOverlay.add(flxSlideInMessage, flxHeader, flxScannOverlay, flxScannedItemsContainer, flxCameraBtn);
};

function frmScanOverlayGlobals() {
    frmScanOverlay = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmScanOverlay,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": false,
        "id": "frmScanOverlay",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFrmBlackOp10"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "inTransitionConfig": {
            "formAnimation": 0
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};