function addWidgetsfrmCart() {
    frmCart.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "8%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlxWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxBackBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxBackBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxBackBtn.setDefaultUnit(kony.flex.DP);
    var imArrowLeftIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "22dp",
        "id": "imArrowLeftIcon",
        "isVisible": true,
        "left": "5dp",
        "skin": "slImage",
        "src": "chevron_left.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxBackBtn.add(imArrowLeftIcon);
    var lblHeaderTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblHeaderTitle",
        "isVisible": true,
        "skin": "sknLblRegDblue136",
        "text": "Cart",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxCartItemCount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "62%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "30dp",
        "id": "flxCartItemCount",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxCartItemCount.setDefaultUnit(kony.flex.DP);
    var lblBagCount = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBagCount",
        "isVisible": true,
        "left": 0,
        "skin": "sknLblBggreenTextBoldWhite98",
        "text": "5",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [11, 0, 11, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCartItemCount.add(lblBagCount);
    flxHeader.add(flxBackBtn, lblHeaderTitle, flxCartItemCount);
    var flxCartContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "68%",
        "horizontalScrollIndicator": true,
        "id": "flxCartContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "8.20%",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCartContainer.setDefaultUnit(kony.flex.DP);
    var lblTotalCartItems = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblTotalCartItems",
        "isVisible": true,
        "left": 10,
        "skin": "sknLblRegGrey110",
        "text": "You have 0 items in your cart",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": 15,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var segCartItems = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "imgMinusIcon": "",
            "imgPlusIcon": "",
            "imgProduct": "",
            "lblProductName": "",
            "rtxProductDiscounts": "",
            "rtxProductDiscountsValue": "",
            "rtxProductPrice": "",
            "tbxQtyInput": {
                "placeholder": "",
                "text": ""
            }
        }],
        "groupCells": false,
        "id": "segCartItems",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "sknSegWhite",
        "rowSkin": "sknSegWhite",
        "rowTemplate": flxCartItem,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "dfdfdf00",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "45dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxCartItem": "flxCartItem",
            "flxMinusBtn": "flxMinusBtn",
            "flxPlusBtn": "flxPlusBtn",
            "flxProductImage": "flxProductImage",
            "flxQuantity": "flxQuantity",
            "imgMinusIcon": "imgMinusIcon",
            "imgPlusIcon": "imgPlusIcon",
            "imgProduct": "imgProduct",
            "lblProductName": "lblProductName",
            "rtxProductDiscounts": "rtxProductDiscounts",
            "rtxProductDiscountsValue": "rtxProductDiscountsValue",
            "rtxProductPrice": "rtxProductPrice",
            "tbxQtyInput": "tbxQtyInput"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxCartContainer.add(lblTotalCartItems, segCartItems);
    var flxCheckoutTotals = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "11%",
        "clipBounds": true,
        "height": "13%",
        "id": "flxCheckoutTotals",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "sknFlexWhie",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCheckoutTotals.setDefaultUnit(kony.flex.DP);
    var flxTotalsDividerLine1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 1,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxTotalsDividerLine1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknLightGreyDividerLine",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTotalsDividerLine1.setDefaultUnit(kony.flex.DP);
    flxTotalsDividerLine1.add();
    var flxCheckouttSubtotal = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "49%",
        "id": "flxCheckouttSubtotal",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "0",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCheckouttSubtotal.setDefaultUnit(kony.flex.DP);
    var rtxCheckouTotalDiscount = new kony.ui.RichText({
        "centerY": "50%",
        "height": "100%",
        "id": "rtxCheckouTotalDiscount",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknRtRegBlack107",
        "text": "-$0",
        "width": "40%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 1, 0],
        "paddingInPixel": false
    }, {});
    var lblCheckoutTotalDiscount = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblCheckoutTotalDiscount",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegDblue107",
        "text": "Total discounts",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCheckouttSubtotal.add(rtxCheckouTotalDiscount, lblCheckoutTotalDiscount);
    var flxCartTaxes = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "49%",
        "id": "flxCartTaxes",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "0",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCartTaxes.setDefaultUnit(kony.flex.DP);
    var rtxCheckoutSubTotal = new kony.ui.RichText({
        "centerY": "50%",
        "height": "100%",
        "id": "rtxCheckoutSubTotal",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknRtxBoldDblue134",
        "text": "$0",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 1, 0],
        "paddingInPixel": false
    }, {});
    var lblCheckoutSubTotal = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblCheckoutSubTotal",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblBoldDblue134",
        "text": "Cart subtotal",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCartTaxes.add(rtxCheckoutSubTotal, lblCheckoutSubTotal);
    flxCheckoutTotals.add(flxTotalsDividerLine1, flxCheckouttSubtotal, flxCartTaxes);
    var flxButtonContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "11%",
        "id": "flxButtonContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlexWhie",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxButtonContainer.setDefaultUnit(kony.flex.DP);
    var flxCheckoutBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 10,
        "centerX": "50%",
        "clipBounds": true,
        "focusSkin": "sknPrimaryRndBtnFocus",
        "height": "74%",
        "id": "flxCheckoutBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxRoundBlue",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    flxCheckoutBtn.setDefaultUnit(kony.flex.DP);
    var lblCheckoutBtn = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblCheckoutBtn",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblBlackWhite121",
        "text": "CHECKOUT",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCheckoutBtn.add(lblCheckoutBtn);
    flxButtonContainer.add(flxCheckoutBtn);
    var flxPopupMessage = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxPopupMessage",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 6
    }, {}, {});
    flxPopupMessage.setDefaultUnit(kony.flex.DP);
    var flxPopupBox = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxPopupBox",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "skin": "sknFlxWhiteBr1Rd8",
        "top": "15%",
        "width": "80%",
        "zIndex": 2
    }, {}, {});
    flxPopupBox.setDefaultUnit(kony.flex.DP);
    var flxPopupHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxPopupHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPopupHeader.setDefaultUnit(kony.flex.DP);
    var flxClosePopupBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "flxClosePopupBtn",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "right": "10dp",
        "skin": "slFbox",
        "top": "10dp",
        "width": "40dp",
        "zIndex": 1
    }, {}, {});
    flxClosePopupBtn.setDefaultUnit(kony.flex.DP);
    var imgClosePopupIcon = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "18dp",
        "id": "imgClosePopupIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": "close_icon.png",
        "width": "18dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxClosePopupBtn.add(imgClosePopupIcon);
    var lblPopupTitle = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblPopupTitle",
        "isVisible": true,
        "skin": "sknLblBoldDblue134",
        "text": "Confirmation",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "20dp",
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPopupHeader.add(flxClosePopupBtn, lblPopupTitle);
    var lblpopupmsg = new kony.ui.Label({
        "centerX": "50%",
        "height": "55dp",
        "id": "lblpopupmsg",
        "isVisible": true,
        "left": "76dp",
        "skin": "sknLblRegDblue124",
        "text": "Item will be removed from the Cart.Do you want to continue?",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxPopupActionBtns = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0%",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxPopupActionBtns",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPopupActionBtns.setDefaultUnit(kony.flex.DP);
    var flxPopupBtnDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxPopupBtnDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknLightGreyDividerLine",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPopupBtnDivider.setDefaultUnit(kony.flex.DP);
    flxPopupBtnDivider.add();
    var CopyflxPopupBtnDivider0afaf7a8fd52844 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "CopyflxPopupBtnDivider0afaf7a8fd52844",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "skin": "sknLightGreyDividerLine",
        "top": "0dp",
        "width": "1dp",
        "zIndex": 1
    }, {}, {});
    CopyflxPopupBtnDivider0afaf7a8fd52844.setDefaultUnit(kony.flex.DP);
    CopyflxPopupBtnDivider0afaf7a8fd52844.add();
    var btnPopupActionOne = new kony.ui.Button({
        "focusSkin": "sknTextBtnBlueTxtRegFocus",
        "height": "50dp",
        "id": "btnPopupActionOne",
        "isVisible": true,
        "left": "0%",
        "skin": "sknTxtRegBlueBgWhite124",
        "text": "cancel",
        "top": "0dp",
        "width": "49%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnPopupActionTwo = new kony.ui.Button({
        "focusSkin": "sknTextBtnBlueTxtRegFocus",
        "height": "50dp",
        "id": "btnPopupActionTwo",
        "isVisible": true,
        "left": "50%",
        "right": 0,
        "skin": "sknTxtRegBlueBgWhite124",
        "text": "OK",
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxPopupActionBtns.add(flxPopupBtnDivider, CopyflxPopupBtnDivider0afaf7a8fd52844, btnPopupActionOne, btnPopupActionTwo);
    flxPopupBox.add(flxPopupHeader, lblpopupmsg, flxPopupActionBtns);
    var flxPopupOverlay = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxPopupOverlay",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_hbc10849f5194346bf46cc953739a271,
        "skin": "sknFlxBlackOp40",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPopupOverlay.setDefaultUnit(kony.flex.DP);
    flxPopupOverlay.add();
    flxPopupMessage.add(flxPopupBox, flxPopupOverlay);
    frmCart.add(flxHeader, flxCartContainer, flxCheckoutTotals, flxButtonContainer, flxPopupMessage);
};

function frmCartGlobals() {
    frmCart = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmCart,
        "enabledForIdleTimeout": false,
        "id": "frmCart",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFrmWhite"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};