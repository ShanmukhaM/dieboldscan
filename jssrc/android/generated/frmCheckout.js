function addWidgetsfrmCheckout() {
    frmCheckout.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "8%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlxWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxBackBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxBackBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxBackBtn.setDefaultUnit(kony.flex.DP);
    var imArrowLeftIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "22dp",
        "id": "imArrowLeftIcon",
        "isVisible": true,
        "left": "5dp",
        "skin": "slImage",
        "src": "chevron_left.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxBackBtn.add(imArrowLeftIcon);
    var flxCheckOut = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxCheckOut",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "slFbox",
        "top": "0dp",
        "width": "80dp",
        "zIndex": 1
    }, {}, {});
    flxCheckOut.setDefaultUnit(kony.flex.DP);
    var lblCheckoutBtn = new kony.ui.Label({
        "centerY": "55%",
        "id": "lblCheckoutBtn",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknLblMdBlack111",
        "text": "Checkout",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCheckOut.add(lblCheckoutBtn);
    var lblHeaderTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblHeaderTitle",
        "isVisible": true,
        "skin": "sknLblRegDblue136",
        "text": "Checkout",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxHeader.add(flxBackBtn, flxCheckOut, lblHeaderTitle);
    var flxCheckoutContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "44%",
        "horizontalScrollIndicator": true,
        "id": "flxCheckoutContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "8%",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCheckoutContainer.setDefaultUnit(kony.flex.DP);
    var lblScannedItemsTitle = new kony.ui.Label({
        "id": "lblScannedItemsTitle",
        "isVisible": true,
        "left": 10,
        "skin": "sknLblRegGrey100",
        "text": "PRODUCTS (8)",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": 15,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxScannedItemList = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxScannedItemList",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxScannedItemList.setDefaultUnit(kony.flex.DP);
    var segCheckoutItems = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblProductName": "Boneless Chicken Breast",
            "lblProductQty": "1",
            "rtxProductDiscounts": "10% OFF",
            "rtxProductPrice": "$9.79"
        }, {
            "lblProductName": "Salad Mix, Spring Mix",
            "lblProductQty": "2",
            "rtxProductDiscounts": "BOGO 50%",
            "rtxProductPrice": "$4.33"
        }, {
            "lblProductName": "Body Wash",
            "lblProductQty": "1",
            "rtxProductDiscounts": "",
            "rtxProductPrice": "$9.94"
        }, {
            "lblProductName": "Long Grain Enriched Rice",
            "lblProductQty": "1",
            "rtxProductDiscounts": "",
            "rtxProductPrice": "$2.90"
        }, {
            "lblProductName": "Tide Laundry Detergent",
            "lblProductQty": "1",
            "rtxProductDiscounts": "",
            "rtxProductPrice": "$15.51"
        }, {
            "lblProductName": "Ile de France Soft Ripened Brie Cheese",
            "lblProductQty": "1",
            "rtxProductDiscounts": "",
            "rtxProductPrice": "$13.09"
        }, {
            "lblProductName": "St. Pellegrino (Pack of 24)",
            "lblProductQty": "1",
            "rtxProductDiscounts": "",
            "rtxProductPrice": "$24.50"
        }],
        "groupCells": false,
        "id": "segCheckoutItems",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": flxCheckoutItem,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "dfdfdf00",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxCheckoutItem": "flxCheckoutItem",
            "flxProductInfo": "flxProductInfo",
            "lblProductName": "lblProductName",
            "lblProductQty": "lblProductQty",
            "rtxProductDiscounts": "rtxProductDiscounts",
            "rtxProductPrice": "rtxProductPrice"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxScannedItemList.add(segCheckoutItems);
    flxCheckoutContainer.add(lblScannedItemsTitle, flxScannedItemList);
    var lblPaymentMethodTitle = new kony.ui.Label({
        "bottom": "44%",
        "id": "lblPaymentMethodTitle",
        "isVisible": true,
        "left": 10,
        "skin": "sknLblRegGrey100",
        "text": "PAYMENT METHOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxPaymentMethod = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "34%",
        "clipBounds": true,
        "height": "60dp",
        "id": "flxPaymentMethod",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlexWhie",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPaymentMethod.setDefaultUnit(kony.flex.DP);
    var imgPaymentIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "22dp",
        "id": "imgPaymentIcon",
        "isVisible": true,
        "left": "10dp",
        "skin": "slImage",
        "src": "visa_card.png",
        "width": "32dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblSelectedPaymentMethod = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblSelectedPaymentMethod",
        "isVisible": true,
        "left": "54dp",
        "skin": "sknLblRegDblue124",
        "text": "**** 5241",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgChevronRight = new kony.ui.Image2({
        "centerY": "50%",
        "height": "18dp",
        "id": "imgChevronRight",
        "isVisible": true,
        "right": "10dp",
        "skin": "slImage",
        "src": "chevron_right.png",
        "width": "18dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxPaymentMethod.add(imgPaymentIcon, lblSelectedPaymentMethod, imgChevronRight);
    var lblTotalsTitle = new kony.ui.Label({
        "bottom": "30%",
        "id": "lblTotalsTitle",
        "isVisible": true,
        "left": 10,
        "skin": "sknLblRegGrey100",
        "text": "TOTALS",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxCheckoutTotals = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "11%",
        "clipBounds": true,
        "height": "18%",
        "id": "flxCheckoutTotals",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "sknFlexWhie",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCheckoutTotals.setDefaultUnit(kony.flex.DP);
    var flxTotalsDividerLine1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 1,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxTotalsDividerLine1",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknLightGreyDividerLine",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTotalsDividerLine1.setDefaultUnit(kony.flex.DP);
    flxTotalsDividerLine1.add();
    var flxCheckouttSubtotal = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "32.33%",
        "id": "flxCheckouttSubtotal",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "0",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCheckouttSubtotal.setDefaultUnit(kony.flex.DP);
    var rtxCheckoutSubtotal = new kony.ui.RichText({
        "centerY": "50%",
        "height": "100%",
        "id": "rtxCheckoutSubtotal",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknRtRegBlack107",
        "text": "$28.14",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblCheckoutSubtotal = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblCheckoutSubtotal",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegDblue107",
        "text": "Cart subtotal",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCheckouttSubtotal.add(rtxCheckoutSubtotal, lblCheckoutSubtotal);
    var flxCartTaxes = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "32.33%",
        "id": "flxCartTaxes",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "0",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCartTaxes.setDefaultUnit(kony.flex.DP);
    var rtxCheckoutTaxes = new kony.ui.RichText({
        "centerY": "50%",
        "height": "100%",
        "id": "rtxCheckoutTaxes",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknRtRegBlack107",
        "text": "$3.36",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblCheckoutTaxes = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblCheckoutTaxes",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegDblue107",
        "text": "Sales taxes",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCartTaxes.add(rtxCheckoutTaxes, lblCheckoutTaxes);
    var flxTotalsDividerLine2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 1,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxTotalsDividerLine2",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknLightGreyDividerLine",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTotalsDividerLine2.setDefaultUnit(kony.flex.DP);
    flxTotalsDividerLine2.add();
    var flxCheckoutTotal = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "32.33%",
        "id": "flxCheckoutTotal",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "0",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCheckoutTotal.setDefaultUnit(kony.flex.DP);
    var rtxCheckoutTotal = new kony.ui.RichText({
        "centerY": "50%",
        "height": "100%",
        "id": "rtxCheckoutTotal",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknRtxBoldDblue134",
        "text": "$31.50",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblCheckoutTotal = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblCheckoutTotal",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblBoldDblue134",
        "text": "Total",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCheckoutTotal.add(rtxCheckoutTotal, lblCheckoutTotal);
    flxCheckoutTotals.add(flxTotalsDividerLine1, flxCheckouttSubtotal, flxCartTaxes, flxTotalsDividerLine2, flxCheckoutTotal);
    var flxPaymentConfirmation = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxPaymentConfirmation",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlxWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 5
    }, {}, {});
    flxPaymentConfirmation.setDefaultUnit(kony.flex.DP);
    var lblVerifyText = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblVerifyText",
        "isVisible": true,
        "skin": "sknLblRegGrey110",
        "text": "Please verify this transaction with Touch ID",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "70dp",
        "width": "88%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgThumb = new kony.ui.Image2({
        "centerX": "50%",
        "height": "44dp",
        "id": "imgThumb",
        "isVisible": true,
        "skin": "slImage",
        "src": "touch_id.png",
        "top": "110dp",
        "width": "44dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblCreditCardName = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblCreditCardName",
        "isVisible": true,
        "skin": "sknLblRegDblue124",
        "text": "Your credit card",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "175dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxPayMethodConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "60dp",
        "id": "flxPayMethodConfirm",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "200dp",
        "width": "60%",
        "zIndex": 1
    }, {}, {});
    flxPayMethodConfirm.setDefaultUnit(kony.flex.DP);
    var flxPaymentInfoContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxPaymentInfoContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPaymentInfoContainer.setDefaultUnit(kony.flex.DP);
    var CopyimgPaymentIcon0a7a65850ffd04d = new kony.ui.Image2({
        "centerY": "50%",
        "height": "22dp",
        "id": "CopyimgPaymentIcon0a7a65850ffd04d",
        "isVisible": true,
        "left": "55dp",
        "skin": "slImage",
        "src": "visa_card.png",
        "width": "32dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var CopylblSelectedPaymentMethod0i3c156cd11cb4a = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblSelectedPaymentMethod0i3c156cd11cb4a",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegDblue124",
        "text": "**** 5241",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPaymentInfoContainer.add(CopyimgPaymentIcon0a7a65850ffd04d, CopylblSelectedPaymentMethod0i3c156cd11cb4a);
    flxPayMethodConfirm.add(flxPaymentInfoContainer);
    var CopyLabel0ccf45cd463464e = new kony.ui.Label({
        "centerX": "50%",
        "id": "CopyLabel0ccf45cd463464e",
        "isVisible": true,
        "skin": "sknLblRegDblue124",
        "text": "will be chargerd the amount of",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "275dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var RichText0d8b4888b42f649 = new kony.ui.RichText({
        "centerX": "50%",
        "id": "RichText0d8b4888b42f649",
        "isVisible": true,
        "skin": "sknRtxBlackDblue260",
        "text": "$31.<sup style=\"font-size:33px\">50</sup>",
        "top": "310dp",
        "width": "88%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxPin = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxPin",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "skin": "slFbox",
        "top": "400dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPin.setDefaultUnit(kony.flex.DP);
    var lblPinText = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblPinText",
        "isVisible": true,
        "skin": "sknLblRegGrey110",
        "text": "Enter your 4 digit PIN code to verify this transaction",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "88%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxPINInputs = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxPINInputs",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "32dp",
        "skin": "slFbox",
        "top": "30dp",
        "width": "220dp",
        "zIndex": 1
    }, {}, {});
    flxPINInputs.setDefaultUnit(kony.flex.DP);
    var txbPINNumberOne = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "40dp",
        "id": "txbPINNumberOne",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "left": "0dp",
        "secureTextEntry": false,
        "skin": "sknTxtBrWhite1BlackReg124",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "0dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var txbPINNumberTwo = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "40dp",
        "id": "txbPINNumberTwo",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "left": "20dp",
        "secureTextEntry": false,
        "skin": "sknTxtBrWhite1BlackReg124",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "0dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var txbPINNumberThree = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "40dp",
        "id": "txbPINNumberThree",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "left": "20dp",
        "secureTextEntry": false,
        "skin": "sknTxtBrWhite1BlackReg124",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "0dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var txbPINNumberFour = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "40dp",
        "id": "txbPINNumberFour",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "left": "20dp",
        "secureTextEntry": false,
        "skin": "sknTxtBrWhite1BlackReg124",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "0dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxPINInputs.add(txbPINNumberOne, txbPINNumberTwo, txbPINNumberThree, txbPINNumberFour);
    flxPin.add(lblPinText, flxPINInputs);
    var flxVerifyTransactionBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "centerX": "50%",
        "clipBounds": true,
        "height": "8%",
        "id": "flxVerifyTransactionBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxBlue",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxVerifyTransactionBtn.setDefaultUnit(kony.flex.DP);
    var lblVerifyBtn = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblVerifyBtn",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblBlackWhite121",
        "text": "VERIFY TRANSACTION",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxVerifyTransactionBtn.add(lblVerifyBtn);
    var flxVerificationHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "8%",
        "id": "flxVerificationHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxVerificationHeader.setDefaultUnit(kony.flex.DP);
    var flxCloseBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxCloseBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_cf016980ac1b4073a4c68c5d5a1b870b,
        "skin": "slFbox",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxCloseBtn.setDefaultUnit(kony.flex.DP);
    var CopyimArrowLeftIcon0e875c3b1ecab42 = new kony.ui.Image2({
        "centerY": "50%",
        "height": "22dp",
        "id": "CopyimArrowLeftIcon0e875c3b1ecab42",
        "isVisible": true,
        "left": "10dp",
        "skin": "slImage",
        "src": "close_icon.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxCloseBtn.add(CopyimArrowLeftIcon0e875c3b1ecab42);
    var lblVerificationHeader = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblVerificationHeader",
        "isVisible": false,
        "skin": "sknLblRegDblue136",
        "text": "Payment verification",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxVerificationHeader.add(flxCloseBtn, lblVerificationHeader);
    flxPaymentConfirmation.add(lblVerifyText, imgThumb, lblCreditCardName, flxPayMethodConfirm, CopyLabel0ccf45cd463464e, RichText0d8b4888b42f649, flxPin, flxVerifyTransactionBtn, flxVerificationHeader);
    var flxPopupMessage = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxPopupMessage",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 6
    }, {}, {});
    flxPopupMessage.setDefaultUnit(kony.flex.DP);
    var flxPopupBox = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxPopupBox",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "skin": "sknFlxWhiteBr1Rd8",
        "top": "15%",
        "width": "80%",
        "zIndex": 2
    }, {}, {});
    flxPopupBox.setDefaultUnit(kony.flex.DP);
    var flxPopupHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "55dp",
        "id": "flxPopupHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPopupHeader.setDefaultUnit(kony.flex.DP);
    var flxClosePopupBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "flxClosePopupBtn",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "right": "10dp",
        "skin": "slFbox",
        "top": "10dp",
        "width": "40dp",
        "zIndex": 1
    }, {}, {});
    flxClosePopupBtn.setDefaultUnit(kony.flex.DP);
    var imgClosePopupIcon = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "18dp",
        "id": "imgClosePopupIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": "close_icon.png",
        "width": "18dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxClosePopupBtn.add(imgClosePopupIcon);
    var lblPopupTitle = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblPopupTitle",
        "isVisible": true,
        "skin": "sknLblBoldDblue134",
        "text": "Payment Verification",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "20dp",
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPopupHeader.add(flxClosePopupBtn, lblPopupTitle);
    var flxPopupContent = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxPopupContent",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPopupContent.setDefaultUnit(kony.flex.DP);
    var flxTouchIDImg = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "44dp",
        "id": "flxTouchIDImg",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "10dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxTouchIDImg.setDefaultUnit(kony.flex.DP);
    var Image0j0aa64366d5448 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "44dp",
        "id": "Image0j0aa64366d5448",
        "isVisible": true,
        "skin": "slImage",
        "src": "touch_id.png",
        "width": "44dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxTouchIDImg.add(Image0j0aa64366d5448);
    var rtxPurchaseAmount = new kony.ui.RichText({
        "bottom": 20,
        "centerX": "50%",
        "id": "rtxPurchaseAmount",
        "isVisible": true,
        "skin": "sknRtxRegDblue124",
        "text": "Please use Touch ID to confirm your purchase of <b>$31.50</b> on your<br><b>VISA **** 5241</b> ",
        "top": "20dp",
        "width": "88%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxPopupContent.add(flxTouchIDImg, rtxPurchaseAmount);
    var flxPopupActionBtns = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxPopupActionBtns",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": 0,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPopupActionBtns.setDefaultUnit(kony.flex.DP);
    var flxPopupBtnDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxPopupBtnDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknLightGreyDividerLine",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPopupBtnDivider.setDefaultUnit(kony.flex.DP);
    flxPopupBtnDivider.add();
    var btnPopupActionOne = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknTextBtnBlueTxtRegFocus",
        "height": "50dp",
        "id": "btnPopupActionOne",
        "isVisible": true,
        "left": 0,
        "skin": "sknTxtRegBlueBgWhite124",
        "text": "Cancel",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnPopupActionTwo = new kony.ui.Button({
        "focusSkin": "sknTextBtnBlueTxtRegFocus",
        "height": "50dp",
        "id": "btnPopupActionTwo",
        "isVisible": false,
        "right": 0,
        "skin": "sknTxtRegBlueBgWhite124",
        "text": "Yes",
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxPopupActionBtns.add(flxPopupBtnDivider, btnPopupActionOne, btnPopupActionTwo);
    flxPopupBox.add(flxPopupHeader, flxPopupContent, flxPopupActionBtns);
    var flxPopupOverlay = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxPopupOverlay",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_d5ab43f48ef2464ab0037ac800462f12,
        "skin": "sknFlxBlackOp40",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPopupOverlay.setDefaultUnit(kony.flex.DP);
    flxPopupOverlay.add();
    flxPopupMessage.add(flxPopupBox, flxPopupOverlay);
    var flxButtonContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "11%",
        "id": "flxButtonContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlxWhite",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxButtonContainer.setDefaultUnit(kony.flex.DP);
    var flxPayBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 10,
        "centerX": "50%",
        "clipBounds": true,
        "focusSkin": "sknPrimaryRndBtnFocus",
        "height": "74%",
        "id": "flxPayBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxRoundBlue",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    flxPayBtn.setDefaultUnit(kony.flex.DP);
    var lblPayBtn = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblPayBtn",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblBlackWhite121",
        "text": "PAY",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPayBtn.add(lblPayBtn);
    flxButtonContainer.add(flxPayBtn);
    var flxApplePay = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxApplePay",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlxWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 5
    }, {}, {});
    flxApplePay.setDefaultUnit(kony.flex.DP);
    var flxOverlay = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "100.02%",
        "id": "flxOverlay",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlxBlackOp40",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxOverlay.setDefaultUnit(kony.flex.DP);
    flxOverlay.add();
    var flxHalfWhite = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "centerX": "50%",
        "clipBounds": true,
        "height": "50%",
        "id": "flxHalfWhite",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknWhite",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHalfWhite.setDefaultUnit(kony.flex.DP);
    var lblPayWithTouchID = new kony.ui.Label({
        "bottom": "10dp",
        "centerX": "50.00%",
        "id": "lblPayWithTouchID",
        "isVisible": true,
        "skin": "sknLblRegGrey110",
        "text": "Pay with Touch ID",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "88%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgTouchID = new kony.ui.Image2({
        "bottom": "40dp",
        "centerX": "50.00%",
        "height": "44dp",
        "id": "imgTouchID",
        "isVisible": true,
        "skin": "slImage",
        "src": "touch_id.png",
        "width": "44dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "100dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "flxDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "6%",
        "skin": "sknLightGreyDividerLine",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {}, {});
    flxDivider.setDefaultUnit(kony.flex.DP);
    flxDivider.add();
    var flxPayTotal = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "100dp",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxPayTotal",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPayTotal.setDefaultUnit(kony.flex.DP);
    var lblPay = new kony.ui.Label({
        "height": "100%",
        "id": "lblPay",
        "isVisible": true,
        "left": "20%",
        "right": "10dp",
        "skin": "sknLblRegDblue124",
        "text": "PAY",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTotal = new kony.ui.Label({
        "height": "100%",
        "id": "lblTotal",
        "isVisible": true,
        "right": "20dp",
        "skin": "sknLblBoldDblue134",
        "text": "$1.99",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPayTotal.add(lblPay, lblTotal);
    var flxDivider2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "140dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "flxDivider2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknLightGreyDividerLine",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {}, {});
    flxDivider2.setDefaultUnit(kony.flex.DP);
    flxDivider2.add();
    var flxSubtotal = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "160dp",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxSubtotal",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSubtotal.setDefaultUnit(kony.flex.DP);
    var CopylblPay0ca9f6cab9cec43 = new kony.ui.Label({
        "height": "100%",
        "id": "CopylblPay0ca9f6cab9cec43",
        "isVisible": true,
        "left": "20%",
        "right": "10dp",
        "skin": "sknLblRegGrey100",
        "text": "SALES TAX",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblTotal0b3c5dc443ac046 = new kony.ui.Label({
        "height": "100%",
        "id": "CopylblTotal0b3c5dc443ac046",
        "isVisible": true,
        "right": "20dp",
        "skin": "sknLblRegDblue107",
        "text": "$1.99",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSubtotal.add(CopylblPay0ca9f6cab9cec43, CopylblTotal0b3c5dc443ac046);
    var flxShipping = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "180dp",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxShipping",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxShipping.setDefaultUnit(kony.flex.DP);
    var CopylblPay0c27d043c13c24a = new kony.ui.Label({
        "height": "100%",
        "id": "CopylblPay0c27d043c13c24a",
        "isVisible": true,
        "left": "20%",
        "right": "10dp",
        "skin": "sknLblRegGrey100",
        "text": "SHIPPING",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblTotal0ecfe80f679a24a = new kony.ui.Label({
        "height": "100%",
        "id": "CopylblTotal0ecfe80f679a24a",
        "isVisible": true,
        "right": "20dp",
        "skin": "sknLblRegDblue107",
        "text": "$1.99",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxShipping.add(CopylblPay0c27d043c13c24a, CopylblTotal0ecfe80f679a24a);
    var flxSubtotal2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "200dp",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxSubtotal2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSubtotal2.setDefaultUnit(kony.flex.DP);
    var CopylblPay0ecdbfdcfb69e4f = new kony.ui.Label({
        "height": "100%",
        "id": "CopylblPay0ecdbfdcfb69e4f",
        "isVisible": true,
        "left": "20%",
        "right": "10dp",
        "skin": "sknLblRegGrey100",
        "text": "SUBTOTAL",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblTotal0f04daf20157b48 = new kony.ui.Label({
        "height": "100%",
        "id": "CopylblTotal0f04daf20157b48",
        "isVisible": true,
        "right": "20dp",
        "skin": "sknLblRegDblue107",
        "text": "$1.99",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSubtotal2.add(CopylblPay0ecdbfdcfb69e4f, CopylblTotal0f04daf20157b48);
    var flxDivider3 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "250dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "flxDivider3",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "6%",
        "skin": "sknLightGreyDividerLine",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {}, {});
    flxDivider3.setDefaultUnit(kony.flex.DP);
    flxDivider3.add();
    var FlexContainer0ffb4a8e65b124c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "250dp",
        "clipBounds": true,
        "height": "40dp",
        "id": "FlexContainer0ffb4a8e65b124c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0ffb4a8e65b124c.setDefaultUnit(kony.flex.DP);
    var CopylblSelectedPaymentMethod0fef1778835e740 = new kony.ui.Label({
        "centerY": "48.75%",
        "id": "CopylblSelectedPaymentMethod0fef1778835e740",
        "isVisible": true,
        "left": "120dp",
        "skin": "sknLblRegDblue124",
        "text": "**** 5241",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyimgPaymentIcon0j1aa7eb58b6945 = new kony.ui.Image2({
        "centerY": "48.75%",
        "height": "22dp",
        "id": "CopyimgPaymentIcon0j1aa7eb58b6945",
        "isVisible": true,
        "left": "78dp",
        "skin": "slImage",
        "src": "visa_card.png",
        "width": "32dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var CopylblPay0ed1bdb406d6f4c = new kony.ui.Label({
        "height": "100%",
        "id": "CopylblPay0ed1bdb406d6f4c",
        "isVisible": true,
        "left": "10dp",
        "right": "10dp",
        "skin": "sknLblRegGrey100",
        "text": "CARD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexContainer0ffb4a8e65b124c.add(CopylblSelectedPaymentMethod0fef1778835e740, CopyimgPaymentIcon0j1aa7eb58b6945, CopylblPay0ed1bdb406d6f4c);
    var Divider4 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "290dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "Divider4",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknLightGreyDividerLine",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {}, {});
    Divider4.setDefaultUnit(kony.flex.DP);
    Divider4.add();
    var CopyFlexContainer0fbe053747e7840 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "290dp",
        "clipBounds": true,
        "height": "40dp",
        "id": "CopyFlexContainer0fbe053747e7840",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0fbe053747e7840.setDefaultUnit(kony.flex.DP);
    var CopyimgPaymentIcon0ef046e99006d4d = new kony.ui.Image2({
        "centerY": "48.75%",
        "height": "22dp",
        "id": "CopyimgPaymentIcon0ef046e99006d4d",
        "isVisible": true,
        "left": "10dp",
        "skin": "slImage",
        "src": "apple_pay_logo_f68c9ac252_seeklogo_com.png",
        "width": "32dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var CopylblPay0b30c85e674ca45 = new kony.ui.Label({
        "height": "100%",
        "id": "CopylblPay0b30c85e674ca45",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknLblBoldBlue110",
        "text": "Cancel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyFlexContainer0fbe053747e7840.add(CopyimgPaymentIcon0ef046e99006d4d, CopylblPay0b30c85e674ca45);
    flxHalfWhite.add(lblPayWithTouchID, imgTouchID, flxDivider, flxPayTotal, flxDivider2, flxSubtotal, flxShipping, flxSubtotal2, flxDivider3, FlexContainer0ffb4a8e65b124c, Divider4, CopyFlexContainer0fbe053747e7840);
    flxApplePay.add(flxOverlay, flxHalfWhite);
    frmCheckout.add(flxHeader, flxCheckoutContainer, lblPaymentMethodTitle, flxPaymentMethod, lblTotalsTitle, flxCheckoutTotals, flxPaymentConfirmation, flxPopupMessage, flxButtonContainer, flxApplePay);
};

function frmCheckoutGlobals() {
    frmCheckout = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmCheckout,
        "enabledForIdleTimeout": false,
        "id": "frmCheckout",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFrmWhite"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};