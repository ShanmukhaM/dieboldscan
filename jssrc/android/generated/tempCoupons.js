function initializetempCoupons() {
    flxCoupons = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "65dp",
        "id": "flxCoupons",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxCoupons.setDefaultUnit(kony.flex.DP);
    var lblCouponTitle = new kony.ui.Label({
        "id": "lblCouponTitle",
        "isVisible": true,
        "left": "10dp",
        "maxNumberOfLines": 1,
        "skin": "sknLblRegDblue136",
        "text": "BOGO",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "10dp",
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCouponDetails = new kony.ui.Label({
        "bottom": 10,
        "id": "lblCouponDetails",
        "isVisible": true,
        "left": "10dp",
        "maxNumberOfLines": 1,
        "skin": "sknLblRegGrey110",
        "text": "Expires on Nov 15, 2017",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": 35,
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnRedeemReward = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknTextBtnBlueTxtRegFocus",
        "height": "40dp",
        "id": "btnRedeemReward",
        "isVisible": true,
        "onClick": AS_Button_e97421e215e14c0db4c15797b4a3a2cd,
        "right": 0,
        "skin": "sknBtnRegBlue107",
        "text": "Apply",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [3, 0, 3, 0],
        "paddingInPixel": false
    }, {});
    flxCoupons.add(lblCouponTitle, lblCouponDetails, btnRedeemReward);
}