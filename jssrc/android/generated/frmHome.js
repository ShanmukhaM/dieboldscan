function addWidgetsfrmHome() {
    frmHome.setDefaultUnit(kony.flex.DP);
    var flxWelcome = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxWelcome",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlxDblue",
        "top": "0dp",
        "width": "100%",
        "zIndex": 4
    }, {}, {});
    flxWelcome.setDefaultUnit(kony.flex.DP);
    var imgWelcomeLogo = new kony.ui.Image2({
        "centerX": "50%",
        "height": "100dp",
        "id": "imgWelcomeLogo",
        "isVisible": true,
        "skin": "slImage",
        "src": "dn_logo_white.png",
        "top": "8%",
        "width": "100dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxWelcomeMessage = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "65dp",
        "id": "flxWelcomeMessage",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "skin": "slFbox",
        "top": "25%",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    flxWelcomeMessage.setDefaultUnit(kony.flex.DP);
    var lblWelcomeMessage = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblWelcomeMessage",
        "isVisible": true,
        "skin": "sknLblRegWhite154",
        "text": "Welcome back,",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblWelcomeUserName = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblWelcomeUserName",
        "isVisible": true,
        "skin": "sknLblRegWhite154",
        "text": "Maggie",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxWelcomeMessage.add(lblWelcomeMessage, lblWelcomeUserName);
    var flxLocationConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxLocationConfirm",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "35%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLocationConfirm.setDefaultUnit(kony.flex.DP);
    var lblConfirmLocation = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblConfirmLocation",
        "isVisible": true,
        "skin": "sknLblLightOp80White124",
        "text": "We believe you are at",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "16dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxLocationMap = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "bottom": 20,
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxLocationMap",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "skin": "sknMapWhiteBg",
        "top": "50dp",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    flxLocationMap.setDefaultUnit(kony.flex.DP);
    var flxWelcomeLocationInfo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "bottom": 0,
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxWelcomeLocationInfo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxWelcomeLocationInfo.setDefaultUnit(kony.flex.DP);
    var flxStoreInfo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxStoreInfo",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "100dp",
        "skin": "slFbox",
        "top": "10dp",
        "width": "62%",
        "zIndex": 1
    }, {}, {});
    flxStoreInfo.setDefaultUnit(kony.flex.DP);
    var imgLocationConfirmIcon = new kony.ui.Image2({
        "height": "22dp",
        "id": "imgLocationConfirmIcon",
        "isVisible": false,
        "left": "10dp",
        "right": "10dp",
        "skin": "slImage",
        "src": "location_question_icon.png",
        "top": "12dp",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblLocationNameConfirm = new kony.ui.Label({
        "id": "lblLocationNameConfirm",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegDblue136",
        "text": "The Grocery",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLocationAddressConfirm = new kony.ui.Label({
        "id": "lblLocationAddressConfirm",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegGrey110",
        "text": "4456 Koelpin Keys Austin, TX 73301",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxStoreInfo.add(imgLocationConfirmIcon, lblLocationNameConfirm, lblLocationAddressConfirm);
    var flxStoreImg = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "88dp",
        "id": "flxStoreImg",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "skin": "sknRoundCornerBox",
        "top": "10dp",
        "width": "88dp",
        "zIndex": 1
    }, {}, {});
    flxStoreImg.setDefaultUnit(kony.flex.DP);
    var Image0jde9fe2d383a49 = new kony.ui.Image2({
        "height": "100%",
        "id": "Image0jde9fe2d383a49",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "grocery_store_img.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxStoreImg.add(Image0jde9fe2d383a49);
    flxWelcomeLocationInfo.add(flxStoreInfo, flxStoreImg);
    var flxWelcomeMapLocation = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "100dp",
        "id": "flxWelcomeMapLocation",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxWelcomeMapLocation.setDefaultUnit(kony.flex.DP);
    var mapWelcomeLocation = new kony.ui.Map({
        "calloutWidth": 80,
        "defaultPinImage": "location_pin.png",
        "height": "100%",
        "id": "mapWelcomeLocation",
        "isVisible": true,
        "left": "0dp",
        "provider": constants.MAP_PROVIDER_GOOGLE,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {
        "mode": constants.MAP_VIEW_MODE_NORMAL,
        "showZoomControl": false,
        "zoomLevel": 11
    });
    flxWelcomeMapLocation.add(mapWelcomeLocation);
    var flxConfirmLocationBtns = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxConfirmLocationBtns",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxConfirmLocationBtns.setDefaultUnit(kony.flex.DP);
    var flxBtnDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxBtnDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknLightGreyDividerLine",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBtnDivider.setDefaultUnit(kony.flex.DP);
    flxBtnDivider.add();
    var btnConfrimLocationNo = new kony.ui.Button({
        "focusSkin": "sknTextBtnBlueTxtRegFocus",
        "height": "50dp",
        "id": "btnConfrimLocationNo",
        "isVisible": true,
        "left": 0,
        "skin": "sknTxtRegBlueBgWhite124",
        "text": "No",
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnConfrimLocationYes = new kony.ui.Button({
        "focusSkin": "sknTextBtnBlueTxtRegFocus",
        "height": "50dp",
        "id": "btnConfrimLocationYes",
        "isVisible": true,
        "right": 0,
        "skin": "sknTxtRegBlueBgWhite124",
        "text": "Yes",
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxConfirmLocationBtns.add(flxBtnDivider, btnConfrimLocationNo, btnConfrimLocationYes);
    flxLocationMap.add(flxWelcomeLocationInfo, flxWelcomeMapLocation, flxConfirmLocationBtns);
    flxLocationConfirm.add(lblConfirmLocation, flxLocationMap);
    var flxLoginForm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "300dp",
        "id": "flxLoginForm",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "176dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLoginForm.setDefaultUnit(kony.flex.DP);
    var txbUserName = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50.00%",
        "focusSkin": "sknSearchInputFocus",
        "height": "40dp",
        "id": "txbUserName",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "maxTextLength": null,
        "placeholder": "customer id",
        "secureTextEntry": false,
        "skin": "sknTxtRegBr1Rd35Black124",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "5%",
        "width": "80%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": true,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var txbPassword = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "sknSearchInputFocus",
        "height": "40dp",
        "id": "txbPassword",
        "isVisible": false,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "maxTextLength": null,
        "placeholder": "password",
        "secureTextEntry": true,
        "skin": "sknTxtRegBr1Rd35Black124",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "25%",
        "width": "80%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": true,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxSignInBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "focusSkin": "sknPrimaryRndBtnFocus",
        "height": "45dp",
        "id": "flxSignInBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxRoundBlue",
        "top": "45%",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    flxSignInBtn.setDefaultUnit(kony.flex.DP);
    var lblSignInBtn = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblSignInBtn",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblBlackWhite121",
        "text": "SIGN IN",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSignInBtn.add(lblSignInBtn);
    var flxRememberMe = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxRememberMe",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "75%",
        "width": "68%",
        "zIndex": 1
    }, {}, {});
    flxRememberMe.setDefaultUnit(kony.flex.DP);
    var lblKeepme = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblKeepme",
        "isVisible": true,
        "left": 0,
        "skin": "sknLblLightOp80White124",
        "text": "Keep me signed in",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var rememberMeSwitch = new kony.ui.Switch({
        "centerY": "50%",
        "height": "32dp",
        "id": "rememberMeSwitch",
        "isVisible": true,
        "leftSideText": "ON",
        "right": 0,
        "rightSideText": "OFF",
        "selectedIndex": 1,
        "skin": "slSwitch",
        "top": "2dp",
        "width": "55dp",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxRememberMe.add(lblKeepme, rememberMeSwitch);
    var flxForgotPasswordBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "-5%",
        "centerX": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxForgotPasswordBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "width": "68%",
        "zIndex": 1
    }, {}, {});
    flxForgotPasswordBtn.setDefaultUnit(kony.flex.DP);
    var lblForgotPasswordBtn = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblForgotPasswordBtn",
        "isVisible": true,
        "skin": "sknLblLightOp80White124",
        "text": "Forgot password?",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxForgotPasswordBtn.add(lblForgotPasswordBtn);
    flxLoginForm.add(txbUserName, txbPassword, flxSignInBtn, flxRememberMe, flxForgotPasswordBtn);
    var flxNoAccount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "-10%",
        "clipBounds": true,
        "height": "145dp",
        "id": "flxNoAccount",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxNoAccount.setDefaultUnit(kony.flex.DP);
    var lblNoAccount = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblNoAccount",
        "isVisible": true,
        "skin": "sknLblLightOp80White124",
        "text": "Dont have an account?",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "20dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnSignUp = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknSecondaryBtnWhiteFocus",
        "height": "45dp",
        "id": "btnSignUp",
        "isVisible": true,
        "skin": "sknBtnRegBr1White124",
        "text": "SIGN UP",
        "top": "25dp",
        "width": "45%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxNoAccount.add(lblNoAccount, btnSignUp);
    flxWelcome.add(imgWelcomeLogo, flxWelcomeMessage, flxLocationConfirm, flxLoginForm, flxNoAccount);
    var flxSideMenuContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxSideMenuContainer",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 3
    }, {}, {});
    flxSideMenuContainer.setDefaultUnit(kony.flex.DP);
    var flxMenuList = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxMenuList",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "-80%",
        "skin": "sknFlxWhite",
        "top": "0dp",
        "width": "80%",
        "zIndex": 2
    }, {}, {});
    flxMenuList.setDefaultUnit(kony.flex.DP);
    var flxCloseBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "8%",
        "id": "flxCloseBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_b34d2e796fd04379bdf207659fc6c57f,
        "skin": "slFbox",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxCloseBtn.setDefaultUnit(kony.flex.DP);
    var imgCloseIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "22dp",
        "id": "imgCloseIcon",
        "isVisible": true,
        "left": "10dp",
        "skin": "slImage",
        "src": "close_icon.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxCloseBtn.add(imgCloseIcon);
    var flxMyAccountBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxMyAccountBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "-100%",
        "skin": "slFbox",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMyAccountBtn.setDefaultUnit(kony.flex.DP);
    var lblMyAccountBtn = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblMyAccountBtn",
        "isVisible": true,
        "left": "45dp",
        "skin": "sknLblRegDblue124",
        "text": "my account",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgMyAccountIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "22dp",
        "id": "imgMyAccountIcon",
        "isVisible": true,
        "left": "10dp",
        "skin": "slImage",
        "src": "user_icon.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxMyAccountBtn.add(lblMyAccountBtn, imgMyAccountIcon);
    var flxPastOrdersBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxPastOrdersBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "-100%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPastOrdersBtn.setDefaultUnit(kony.flex.DP);
    var lblPastOrdersBtn = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblPastOrdersBtn",
        "isVisible": true,
        "left": "45dp",
        "skin": "sknLblRegDblue124",
        "text": "past orders",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgPastOrdersIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "22dp",
        "id": "imgPastOrdersIcon",
        "isVisible": true,
        "left": "10dp",
        "skin": "slImage",
        "src": "past_orders_icon.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxPastOrdersBtn.add(lblPastOrdersBtn, imgPastOrdersIcon);
    var flxPaymentBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxPaymentBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "-100%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPaymentBtn.setDefaultUnit(kony.flex.DP);
    var lblPaymentMethodsBtn = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblPaymentMethodsBtn",
        "isVisible": true,
        "left": "45dp",
        "skin": "sknLblRegDblue124",
        "text": "payment",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgPaymentMethod = new kony.ui.Image2({
        "centerY": "50%",
        "height": "22dp",
        "id": "imgPaymentMethod",
        "isVisible": true,
        "left": "10dp",
        "skin": "slImage",
        "src": "payment_method_icon.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxPaymentBtn.add(lblPaymentMethodsBtn, imgPaymentMethod);
    var flxSavedRecipesBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxSavedRecipesBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "-100%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSavedRecipesBtn.setDefaultUnit(kony.flex.DP);
    var lblSavedRecipesBtn = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblSavedRecipesBtn",
        "isVisible": true,
        "left": "45dp",
        "skin": "sknLblRegDblue124",
        "text": "saved recipies",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgSavedRecipesIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "22dp",
        "id": "imgSavedRecipesIcon",
        "isVisible": true,
        "left": "10dp",
        "skin": "slImage",
        "src": "apron_icon.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxSavedRecipesBtn.add(lblSavedRecipesBtn, imgSavedRecipesIcon);
    var flxSettingsBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxSettingsBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "-100%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSettingsBtn.setDefaultUnit(kony.flex.DP);
    var lblSettingsBtn = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblSettingsBtn",
        "isVisible": true,
        "left": "45dp",
        "skin": "sknLblRegDblue124",
        "text": "settings",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgSettingsIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "22dp",
        "id": "imgSettingsIcon",
        "isVisible": true,
        "left": "10dp",
        "skin": "slImage",
        "src": "settings_icon.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxSettingsBtn.add(lblSettingsBtn, imgSettingsIcon);
    var flxLogoutBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxLogoutBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "-100%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLogoutBtn.setDefaultUnit(kony.flex.DP);
    var lblLogoutBtn = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblLogoutBtn",
        "isVisible": true,
        "left": "45dp",
        "skin": "sknLblRegDblue124",
        "text": "logout",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgLogoutIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "22dp",
        "id": "imgLogoutIcon",
        "isVisible": true,
        "left": "10dp",
        "skin": "slImage",
        "src": "logout_icon.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxLogoutBtn.add(lblLogoutBtn, imgLogoutIcon);
    flxMenuList.add(flxCloseBtn, flxMyAccountBtn, flxPastOrdersBtn, flxPaymentBtn, flxSavedRecipesBtn, flxSettingsBtn, flxLogoutBtn);
    var flxOverlay = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxOverlay",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_e6f3011cbbd349fe8a903e8e2b23906f,
        "skin": "sknFlxBlackOp40",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxOverlay.setDefaultUnit(kony.flex.DP);
    flxOverlay.add();
    flxSideMenuContainer.add(flxMenuList, flxOverlay);
    var flxHomeContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxHomeContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHomeContainer.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "8%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlxWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxMenuBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxMenuBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxMenuBtn.setDefaultUnit(kony.flex.DP);
    var imSettingsIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "22dp",
        "id": "imSettingsIcon",
        "isVisible": true,
        "left": 10,
        "skin": "slImage",
        "src": "menu_icon.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxMenuBtn.add(imSettingsIcon);
    var flxCartBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxCartBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "slFbox",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxCartBtn.setDefaultUnit(kony.flex.DP);
    var imgBagIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "22dp",
        "id": "imgBagIcon",
        "isVisible": true,
        "right": 10,
        "skin": "slImage",
        "src": "shopping_bag_icon.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblBagCount = new kony.ui.Label({
        "bottom": "9dp",
        "id": "lblBagCount",
        "isVisible": true,
        "right": "5dp",
        "skin": "sknLblBggreenTextBoldWhite98",
        "text": "5",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [10, 0, 10, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCartBtn.add(imgBagIcon, lblBagCount);
    var lblHeaderTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblHeaderTitle",
        "isVisible": false,
        "skin": "sknLblRegDblue136",
        "text": "Header Title",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxHeader.add(flxMenuBtn, flxCartBtn, lblHeaderTitle);
    var flxUserProfile = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "10%",
        "clipBounds": false,
        "height": "75dp",
        "id": "flxUserProfile",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxWhiteBr1Rd60",
        "width": "75dp",
        "zIndex": 2
    }, {}, {});
    flxUserProfile.setDefaultUnit(kony.flex.DP);
    var flxUserProfileImage = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "flxUserProfileImage",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknUserProfileImageNoShadow",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxUserProfileImage.setDefaultUnit(kony.flex.DP);
    var Image0caba7cffc5664e = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "Image0caba7cffc5664e",
        "isVisible": true,
        "skin": "slImage",
        "src": "profile_image.png",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxUserProfileImage.add(Image0caba7cffc5664e);
    flxUserProfile.add(flxUserProfileImage);
    var flxScannerBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 10,
        "centerX": "50%",
        "clipBounds": true,
        "focusSkin": "sknPrimaryRndBtnFocus",
        "height": "8%",
        "id": "flxScannerBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxRoundBlue",
        "width": "95%",
        "zIndex": 5
    }, {}, {});
    flxScannerBtn.setDefaultUnit(kony.flex.DP);
    var imgScanIcon = new kony.ui.Image2({
        "centerX": "32%",
        "centerY": "50%",
        "height": "22dp",
        "id": "imgScanIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": "scan_icon.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var camScanBtn = new kony.ui.Camera({
        "cameraSource": constants.CAMERA_SOURCE_DEFAULT,
        "compressionLevel": 0,
        "focusSkin": "sknCameraBtnTransp",
        "height": "100%",
        "id": "camScanBtn",
        "isVisible": false,
        "left": "0dp",
        "scaleFactor": 10,
        "skin": "slCamera",
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "accessMode": constants.CAMERA_IMAGE_ACCESS_MODE_PUBLIC,
        "enableOverlay": true,
        "enablePhotoCropFeature": false,
        "overlayConfig": {
            "captureButtonText": "",
            "startVideoButtonText": "",
            "stopVideoButtonText": "",
            "overlayForm": frmScanOverlay,
            "tapAnywhere": false
        }
    });
    var lblScannBtn = new kony.ui.Label({
        "centerX": "56%",
        "centerY": "50%",
        "id": "lblScannBtn",
        "isVisible": true,
        "skin": "sknLblBlackWhite121",
        "text": "SCAN PRODUCTS",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxScannerBtn.add(imgScanIcon, camScanBtn, lblScannBtn);
    var flxHomeContent = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "92%",
        "horizontalScrollIndicator": true,
        "id": "flxHomeContent",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "8%",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHomeContent.setDefaultUnit(kony.flex.DP);
    var lblUserName = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblUserName",
        "isVisible": true,
        "skin": "sknLblRegDblue136",
        "text": "Maggie Moore",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "60dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblMember = new kony.ui.Label({
        "centerX": "50.00%",
        "id": "lblMember",
        "isVisible": true,
        "skin": "sknLblRegGrey110",
        "text": "Member",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxMemberPoints = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxMemberPoints",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMemberPoints.setDefaultUnit(kony.flex.DP);
    var lblPoints = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblPoints",
        "isVisible": true,
        "skin": "sknLblBoldOrange234",
        "text": "2,452pts",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblPointsReward = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblPointsReward",
        "isVisible": true,
        "skin": "sknLblRegDblue107",
        "text": "You are 548 points away from next reward.",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxMemberPoints.add(lblPoints, lblPointsReward);
    var flxCurrentLocation = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxCurrentLocation",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "8dp",
        "width": "88%",
        "zIndex": 1
    }, {}, {});
    flxCurrentLocation.setDefaultUnit(kony.flex.DP);
    var lblMem = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblMem",
        "isVisible": true,
        "maxNumberOfLines": 2,
        "skin": "sknLblRegGrey110",
        "text": "The Grocery @ 4456 Koelpin Keys Austin, TX",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": 0,
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCurrentLocation.add(lblMem);
    var flxMyRewardsContent = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxMyRewardsContent",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlexWhie",
        "top": "20dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMyRewardsContent.setDefaultUnit(kony.flex.DP);
    var lblMyRewardsTitle = new kony.ui.Label({
        "id": "lblMyRewardsTitle",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblBoldBlue110",
        "text": "COUPONS FOR YOU",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "15dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCouponText = new kony.ui.Label({
        "id": "lblCouponText",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegGrey90",
        "text": "These are coupons based on your previous purchases.",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "40dp",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxMyRewardsScroll = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": true,
        "allowVerticalBounce": false,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "180dp",
        "horizontalScrollIndicator": true,
        "id": "flxMyRewardsScroll",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_HORIZONTAL,
        "skin": "slFSbox",
        "top": "55dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMyRewardsScroll.setDefaultUnit(kony.flex.DP);
    var flxReward3 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "150dp",
        "id": "flxReward3",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "skin": "sknFlxWhiteBr1Rd8",
        "top": "10dp",
        "width": "45%",
        "zIndex": 1
    }, {}, {});
    flxReward3.setDefaultUnit(kony.flex.DP);
    var flxRoundCornerBox = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxRoundCornerBox",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknRoundCornerBox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxRoundCornerBox.setDefaultUnit(kony.flex.DP);
    var lblOff = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblOff",
        "isVisible": true,
        "maxNumberOfLines": 1,
        "skin": "sknLblBgblueTextBoldWhite180",
        "text": "10% OFF",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 5, 0, 5],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblYourCouponOne = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblYourCouponOne",
        "isVisible": true,
        "skin": "sknLblRegDblue124",
        "text": "1lb Chicken fillet",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "60dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblRewardExpDate0fc094f209a8a4a = new kony.ui.Label({
        "centerX": "50%",
        "id": "CopylblRewardExpDate0fc094f209a8a4a",
        "isVisible": true,
        "skin": "sknLblRegGrey90",
        "text": "Expires on Dec 31, 2017",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "92dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopybtnRedeemReward0f7695944fb0d4e = new kony.ui.Button({
        "bottom": 0,
        "focusSkin": "sknTextBtnBlueTxtRegFocus",
        "height": "40dp",
        "id": "CopybtnRedeemReward0f7695944fb0d4e",
        "isVisible": true,
        "left": 0,
        "skin": "sknBtnRegBlue107",
        "text": "Redeem",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxRoundCornerBox.add(lblOff, lblYourCouponOne, CopylblRewardExpDate0fc094f209a8a4a, CopybtnRedeemReward0f7695944fb0d4e);
    flxReward3.add(flxRoundCornerBox);
    var flxReward2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "150dp",
        "id": "flxReward2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "skin": "sknFlxWhiteBr1Rd8",
        "top": "10dp",
        "width": "45%",
        "zIndex": 1
    }, {}, {});
    flxReward2.setDefaultUnit(kony.flex.DP);
    var CopyflxRewardsContainer0a5e0e06b49af47 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "CopyflxRewardsContainer0a5e0e06b49af47",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknRoundCornerBox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyflxRewardsContainer0a5e0e06b49af47.setDefaultUnit(kony.flex.DP);
    var CopylblReward0bc76d479281942 = new kony.ui.Label({
        "centerX": "50%",
        "id": "CopylblReward0bc76d479281942",
        "isVisible": true,
        "maxNumberOfLines": 1,
        "skin": "sknGreenBgLgTxtCondReg",
        "text": "$5 OFF",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 5, 0, 5],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblRewardInfo0b1aecfca60ed4c = new kony.ui.Label({
        "centerX": "50%",
        "id": "CopylblRewardInfo0b1aecfca60ed4c",
        "isVisible": true,
        "skin": "sknLblRegDblue124",
        "text": "Crest toothpaste",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "60dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblRewardExpDate0aae022c36c454a = new kony.ui.Label({
        "centerX": "50%",
        "id": "CopylblRewardExpDate0aae022c36c454a",
        "isVisible": true,
        "skin": "sknLblRegGrey90",
        "text": "Expires on Aug 20, 2017",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "92dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopybtnRedeemReward0c3d20f8b289046 = new kony.ui.Button({
        "bottom": 0,
        "focusSkin": "sknTextBtnBlueTxtRegFocus",
        "height": "40dp",
        "id": "CopybtnRedeemReward0c3d20f8b289046",
        "isVisible": true,
        "left": 0,
        "skin": "sknBtnRegBlue107",
        "text": "Redeem",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    CopyflxRewardsContainer0a5e0e06b49af47.add(CopylblReward0bc76d479281942, CopylblRewardInfo0b1aecfca60ed4c, CopylblRewardExpDate0aae022c36c454a, CopybtnRedeemReward0c3d20f8b289046);
    flxReward2.add(CopyflxRewardsContainer0a5e0e06b49af47);
    var flxReward1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "150dp",
        "id": "flxReward1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "skin": "sknFlxWhiteBr1Rd8",
        "top": "10dp",
        "width": "45%",
        "zIndex": 1
    }, {}, {});
    flxReward1.setDefaultUnit(kony.flex.DP);
    var flxRewardsContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxRewardsContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknRoundCornerBox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxRewardsContainer.setDefaultUnit(kony.flex.DP);
    var lblReward = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblReward",
        "isVisible": true,
        "maxNumberOfLines": 1,
        "skin": "sknPinkBgLgTxtCondReg",
        "text": "5% OFF",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [5, 5, 0, 5],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblRewardInfo = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblRewardInfo",
        "isVisible": true,
        "skin": "sknLblRegDblue124",
        "text": "1gal. of milk",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "60dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblRewardExpDate = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblRewardExpDate",
        "isVisible": true,
        "skin": "sknLblRegGrey90",
        "text": "Expires on Sep 30, 2017",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "92dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnRedeemReward = new kony.ui.Button({
        "bottom": 0,
        "focusSkin": "sknTextBtnBlueTxtRegFocus",
        "height": "40dp",
        "id": "btnRedeemReward",
        "isVisible": true,
        "left": 0,
        "skin": "sknBtnRegBlue107",
        "text": "Redeem",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var FlexContainer0j26cf01a0f4941 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "FlexContainer0j26cf01a0f4941",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "slFbox",
        "top": "5dp",
        "width": "40dp",
        "zIndex": 1
    }, {}, {});
    FlexContainer0j26cf01a0f4941.setDefaultUnit(kony.flex.DP);
    FlexContainer0j26cf01a0f4941.add();
    flxRewardsContainer.add(lblReward, lblRewardInfo, lblRewardExpDate, btnRedeemReward, FlexContainer0j26cf01a0f4941);
    flxReward1.add(flxRewardsContainer);
    flxMyRewardsScroll.add(flxReward3, flxReward2, flxReward1);
    flxMyRewardsContent.add(lblMyRewardsTitle, lblCouponText, flxMyRewardsScroll);
    var flxStoreCoupons = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxStoreCoupons",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "sknFlexWhie",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxStoreCoupons.setDefaultUnit(kony.flex.DP);
    var lblCouponsTitle = new kony.ui.Label({
        "id": "lblCouponsTitle",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblBoldBlue110",
        "text": "THE GROCERY COUPONS & DEALS",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "15dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var segStoreCoupons = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "bottom": 80,
        "data": [{
            "btnRedeemReward": "Apply",
            "lblCouponDetails": "Expires on Aug 25, 2017",
            "lblCouponTitle": "BOGO 50% OFF"
        }, {
            "btnRedeemReward": "Apply",
            "lblCouponDetails": "Expires on Sep 30, 2017",
            "lblCouponTitle": "10% All Home Articles"
        }, {
            "btnRedeemReward": "Apply",
            "lblCouponDetails": "Expires on Nov 15, 2017",
            "lblCouponTitle": "$15 OFF your purchase of $20+"
        }, {
            "btnRedeemReward": "Apply",
            "lblCouponDetails": "Expires on Dec 20, 2017",
            "lblCouponTitle": "20% OFF your purchase of $100+"
        }],
        "groupCells": false,
        "id": "segStoreCoupons",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "sknSegWhite",
        "rowSkin": "sknSegWhite",
        "rowTemplate": flxCoupons,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "dfdfdf00",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "10dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "btnRedeemReward": "btnRedeemReward",
            "flxCoupons": "flxCoupons",
            "lblCouponDetails": "lblCouponDetails",
            "lblCouponTitle": "lblCouponTitle"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxStoreCoupons.add(lblCouponsTitle, segStoreCoupons);
    flxHomeContent.add(lblUserName, lblMember, flxMemberPoints, flxCurrentLocation, flxMyRewardsContent, flxStoreCoupons);
    var flxSlideInMessage = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "flxSlideInMessage",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "skin": "sknSlideInMessage",
        "top": "-6%",
        "width": "100%",
        "zIndex": 5
    }, {}, {});
    flxSlideInMessage.setDefaultUnit(kony.flex.DP);
    var imgSlideInMessageIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "18dp",
        "id": "imgSlideInMessageIcon",
        "isVisible": true,
        "left": "15%",
        "skin": "slImage",
        "src": "chekmark_fill_icon.png",
        "width": "18dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblSlideInMessage = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblSlideInMessage",
        "isVisible": true,
        "left": 10,
        "skin": "sknWhiteSmTxtReg",
        "text": "Coupon has been applied to your cart.",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSlideInMessage.add(imgSlideInMessageIcon, lblSlideInMessage);
    var flxPopupMessage = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxPopupMessage",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 6
    }, {}, {});
    flxPopupMessage.setDefaultUnit(kony.flex.DP);
    var flxPopupBox = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxPopupBox",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "skin": "sknFlxWhiteBr1Rd8",
        "top": "15%",
        "width": "80%",
        "zIndex": 2
    }, {}, {});
    flxPopupBox.setDefaultUnit(kony.flex.DP);
    var flxPopupHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "55dp",
        "id": "flxPopupHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPopupHeader.setDefaultUnit(kony.flex.DP);
    var flxClosePopupBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "flxClosePopupBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_fb922d641e5f4b239946c6c24b48bb8b,
        "right": "10dp",
        "skin": "slFbox",
        "top": "10dp",
        "width": "40dp",
        "zIndex": 1
    }, {}, {});
    flxClosePopupBtn.setDefaultUnit(kony.flex.DP);
    var imgClosePopupIcon = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "18dp",
        "id": "imgClosePopupIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": "close_icon.png",
        "width": "18dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxClosePopupBtn.add(imgClosePopupIcon);
    var lblPopupTitle = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblPopupTitle",
        "isVisible": true,
        "skin": "sknLblBoldDblue134",
        "text": "BOGO 50% OFF",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "20dp",
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPopupHeader.add(flxClosePopupBtn, lblPopupTitle);
    var flxPopupContent = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxPopupContent",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPopupContent.setDefaultUnit(kony.flex.DP);
    var lblPopupDescription = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblPopupDescription",
        "isVisible": true,
        "skin": "sknLblRegDblue124",
        "text": "Scan products elegible for this offer and get 50% off the second product.",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var Image0j0aa64366d5448 = new kony.ui.Image2({
        "bottom": 10,
        "centerX": "50%",
        "height": "140dp",
        "id": "Image0j0aa64366d5448",
        "isVisible": true,
        "skin": "slImage",
        "src": "coupon_code.png",
        "top": 10,
        "width": "140dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblPopupDescription2 = new kony.ui.Label({
        "bottom": 20,
        "centerX": "50%",
        "id": "lblPopupDescription2",
        "isVisible": true,
        "left": "122dp",
        "skin": "sknLblRegGrey110",
        "text": "This offer expires on Aug 25, 2017",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPopupContent.add(lblPopupDescription, Image0j0aa64366d5448, lblPopupDescription2);
    var flxPopupActionBtns = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxPopupActionBtns",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": 0,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPopupActionBtns.setDefaultUnit(kony.flex.DP);
    var flxPopupBtnDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxPopupBtnDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknLightGreyDividerLine",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPopupBtnDivider.setDefaultUnit(kony.flex.DP);
    flxPopupBtnDivider.add();
    var btnPopupActionOne = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknTextBtnBlueTxtRegFocus",
        "height": "50dp",
        "id": "btnPopupActionOne",
        "isVisible": true,
        "left": 0,
        "skin": "sknTxtRegBlueBgWhite124",
        "text": "Apply Coupon",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnPopupActionTwo = new kony.ui.Button({
        "focusSkin": "sknTextBtnBlueTxtRegFocus",
        "height": "50dp",
        "id": "btnPopupActionTwo",
        "isVisible": false,
        "right": 0,
        "skin": "sknTxtRegBlueBgWhite124",
        "text": "Yes",
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxPopupActionBtns.add(flxPopupBtnDivider, btnPopupActionOne, btnPopupActionTwo);
    flxPopupBox.add(flxPopupHeader, flxPopupContent, flxPopupActionBtns);
    var flxPopupOverlay = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxPopupOverlay",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlxBlackOp40",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPopupOverlay.setDefaultUnit(kony.flex.DP);
    flxPopupOverlay.add();
    flxPopupMessage.add(flxPopupBox, flxPopupOverlay);
    flxHomeContainer.add(flxHeader, flxUserProfile, flxScannerBtn, flxHomeContent, flxSlideInMessage, flxPopupMessage);
    var flxSearchLocation = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxSearchLocation",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 5
    }, {}, {});
    flxSearchLocation.setDefaultUnit(kony.flex.DP);
    var flxSearchHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "8%",
        "id": "flxSearchHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlxWhite",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSearchHeader.setDefaultUnit(kony.flex.DP);
    var flxCancelBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxCancelBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "slFbox",
        "top": "0dp",
        "width": "70dp",
        "zIndex": 1
    }, {}, {});
    flxCancelBtn.setDefaultUnit(kony.flex.DP);
    var lblCancelBtn = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblCancelBtn",
        "isVisible": true,
        "right": 10,
        "skin": "sknLblMdBlack111",
        "text": "Cancel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCancelBtn.add(lblCancelBtn);
    var txbSearchLocation = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "48%",
        "centerY": "50%",
        "focusSkin": "sknSearchInputFocus",
        "height": "35dp",
        "id": "txbSearchLocation",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "60dp",
        "maxTextLength": null,
        "placeholder": "Search",
        "secureTextEntry": false,
        "skin": "sknTxtRegBr1Rd35Black124",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "68%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [10, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": true,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var imgSearchIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "18dp",
        "id": "imgSearchIcon",
        "isVisible": true,
        "left": "17%",
        "skin": "slImage",
        "src": "search_icon.png",
        "width": "18dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxListMapViewBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxListMapViewBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    flxListMapViewBtn.setDefaultUnit(kony.flex.DP);
    var lblListMapViewBtn = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblListMapViewBtn",
        "isVisible": true,
        "left": 10,
        "skin": "sknLblMdBlack111",
        "text": "Map",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxListMapViewBtn.add(lblListMapViewBtn);
    flxSearchHeader.add(flxCancelBtn, txbSearchLocation, imgSearchIcon, flxListMapViewBtn);
    var flxLocationList = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "92%",
        "id": "flxLocationList",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlxLightGrey",
        "top": "8%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLocationList.setDefaultUnit(kony.flex.DP);
    var segLocationList = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "data": [
            [{
                    "lblListTitle": "NEAR YOU"
                },
                [{
                    "imgLocationIcon": "location_icon.png",
                    "imgMoreIcon": "more_icon.png",
                    "lblLocationAddress": "• 4456 Koelpin Keys Austin, TX",
                    "lblLocationMilage": "0.5 mi",
                    "lblLocationName": "The Grocery"
                }, {
                    "imgLocationIcon": "location_icon.png",
                    "imgMoreIcon": "more_icon.png",
                    "lblLocationAddress": "• 4450 Koelpin Keys Austin, TX",
                    "lblLocationMilage": "1.0 mi",
                    "lblLocationName": "Electronics Shop"
                }, {
                    "imgLocationIcon": "location_icon.png",
                    "imgMoreIcon": "more_icon.png",
                    "lblLocationAddress": "• 4444 Koelpin Keys Austin, TX",
                    "lblLocationMilage": "1.5 mi",
                    "lblLocationName": "Open Market"
                }, {
                    "imgLocationIcon": "location_icon.png",
                    "imgMoreIcon": "more_icon.png",
                    "lblLocationAddress": "• 669 Naomie Curve Austin, TX",
                    "lblLocationMilage": "2.2 mi",
                    "lblLocationName": "Monaco Retail"
                }, {
                    "imgLocationIcon": "location_icon.png",
                    "imgMoreIcon": "more_icon.png",
                    "lblLocationAddress": "• 669 Naomie Curve Austin, TX",
                    "lblLocationMilage": "2.5 mi",
                    "lblLocationName": "Food Mart"
                }, {
                    "imgLocationIcon": "location_icon.png",
                    "imgMoreIcon": "more_icon.png",
                    "lblLocationAddress": "• 6603 Bergstrom Parkway Austin, TX",
                    "lblLocationMilage": "3.2 mi",
                    "lblLocationName": "Bella Bella Boutique"
                }]
            ]
        ],
        "groupCells": false,
        "height": "100%",
        "id": "segLocationList",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_c8e8535d717146d68de08c1d05723373,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": flxLocation,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sknGreySegHeaderBg",
        "sectionHeaderTemplate": flxListTitle,
        "selectionBehavior": constants.SEGUI_SINGLE_SELECT_BEHAVIOR,
        "separatorColor": "dfdfdf00",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "FlexContainer0b8404cd8a8eb40": "FlexContainer0b8404cd8a8eb40",
            "flxListTitle": "flxListTitle",
            "flxLocation": "flxLocation",
            "flxMilesAddress": "flxMilesAddress",
            "imgLocationIcon": "imgLocationIcon",
            "imgMoreIcon": "imgMoreIcon",
            "lblListTitle": "lblListTitle",
            "lblLocationAddress": "lblLocationAddress",
            "lblLocationMilage": "lblLocationMilage",
            "lblLocationName": "lblLocationName"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "dockSectionHeaders": false
    });
    var flxMapLocationDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "-160dp",
        "centerX": "50%",
        "clipBounds": false,
        "height": "160dp",
        "id": "flxMapLocationDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlxWhiteBr1Rd8",
        "width": "95%",
        "zIndex": 2
    }, {}, {});
    flxMapLocationDetails.setDefaultUnit(kony.flex.DP);
    var flxMapLocationDetailsContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "centerX": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "flxMapLocationDetailsContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMapLocationDetailsContainer.setDefaultUnit(kony.flex.DP);
    var flxMapLocationInfo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxMapLocationInfo",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "100dp",
        "skin": "slFbox",
        "top": "10dp",
        "width": "62%",
        "zIndex": 1
    }, {}, {});
    flxMapLocationInfo.setDefaultUnit(kony.flex.DP);
    var lblMapLocationName = new kony.ui.Label({
        "id": "lblMapLocationName",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegDblue136",
        "text": "The Grocery",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblMapLocationAddress = new kony.ui.Label({
        "id": "lblMapLocationAddress",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegGrey110",
        "text": "4456 Koelpin Keys Austin, TX 73301",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblMapLocationMilage = new kony.ui.Label({
        "id": "lblMapLocationMilage",
        "isVisible": true,
        "left": "10dp",
        "maxNumberOfLines": 1,
        "skin": "sknLblBlackGrey110",
        "text": "1.0 mi",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxMapLocationInfo.add(lblMapLocationName, lblMapLocationAddress, lblMapLocationMilage);
    var flxMapLocationPicture = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "88dp",
        "id": "flxMapLocationPicture",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "skin": "sknRoundCornerBox",
        "top": "10dp",
        "width": "88dp",
        "zIndex": 1
    }, {}, {});
    flxMapLocationPicture.setDefaultUnit(kony.flex.DP);
    var CopyImage0i2cef1ad611142 = new kony.ui.Image2({
        "height": "100%",
        "id": "CopyImage0i2cef1ad611142",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "grocery_store_img.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxMapLocationPicture.add(CopyImage0i2cef1ad611142);
    var flxLocationOptionBtns = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 1,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxLocationOptionBtns",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLocationOptionBtns.setDefaultUnit(kony.flex.DP);
    var CopyflxBtnDivider0ged3958bcdfb41 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "60%",
        "id": "CopyflxBtnDivider0ged3958bcdfb41",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknLightGreyDividerLine",
        "width": "1dp",
        "zIndex": 1
    }, {}, {});
    CopyflxBtnDivider0ged3958bcdfb41.setDefaultUnit(kony.flex.DP);
    CopyflxBtnDivider0ged3958bcdfb41.add();
    var btnLocationDetails = new kony.ui.Button({
        "focusSkin": "sknTextBtnBlueTxtRegFocus",
        "height": "50dp",
        "id": "btnLocationDetails",
        "isVisible": true,
        "left": 0,
        "skin": "sknTxtRegBlueBgWhite124",
        "text": "Details",
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnSelectLocation = new kony.ui.Button({
        "focusSkin": "sknTextBtnBlueTxtRegFocus",
        "height": "50dp",
        "id": "btnSelectLocation",
        "isVisible": true,
        "onClick": AS_Button_eac745ea5ba54e6bb2e86e7666338131,
        "right": 0,
        "skin": "sknTxtRegBlueBgWhite124",
        "text": "Select locations",
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxLocationOptionBtns.add(CopyflxBtnDivider0ged3958bcdfb41, btnLocationDetails, btnSelectLocation);
    flxMapLocationDetailsContainer.add(flxMapLocationInfo, flxMapLocationPicture, flxLocationOptionBtns);
    flxMapLocationDetails.add(flxMapLocationDetailsContainer);
    var mapSearchLocations = new kony.ui.Map({
        "calloutWidth": 80,
        "defaultPinImage": "location_pin.png",
        "height": "100%",
        "id": "mapSearchLocations",
        "isVisible": false,
        "left": "0dp",
        "onPinClick": AS_Map_a85780c427a049b4af6e639734b770da,
        "provider": constants.MAP_PROVIDER_GOOGLE,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {
        "mode": constants.MAP_VIEW_MODE_NORMAL,
        "showZoomControl": false,
        "zoomLevel": 11
    });
    flxLocationList.add(segLocationList, flxMapLocationDetails, mapSearchLocations);
    flxSearchLocation.add(flxSearchHeader, flxLocationList);
    var flxValidCustomerIdpopup = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxValidCustomerIdpopup",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 6
    }, {}, {});
    flxValidCustomerIdpopup.setDefaultUnit(kony.flex.DP);
    var popupAlert = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "100dp",
        "id": "popupAlert",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "skin": "sknFlxWhiteBr1Rd8",
        "top": "33%",
        "width": "80%",
        "zIndex": 2
    }, {}, {});
    popupAlert.setDefaultUnit(kony.flex.DP);
    var lblpopupmsg = new kony.ui.Label({
        "centerX": "50%",
        "height": "53dp",
        "id": "lblpopupmsg",
        "isVisible": true,
        "left": "76dp",
        "skin": "sknLblRegDblue124",
        "text": "Please enter a Valid Customer Id",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyflxPopupActionBtns0bdbe51cdbd9b40 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "CopyflxPopupActionBtns0bdbe51cdbd9b40",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "-2dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyflxPopupActionBtns0bdbe51cdbd9b40.setDefaultUnit(kony.flex.DP);
    var CopyflxPopupBtnDivider0e4e2e785fa8549 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyflxPopupBtnDivider0e4e2e785fa8549",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknLightGreyDividerLine",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyflxPopupBtnDivider0e4e2e785fa8549.setDefaultUnit(kony.flex.DP);
    CopyflxPopupBtnDivider0e4e2e785fa8549.add();
    var CopybtnPopupActionTwo0b42b21041ca348 = new kony.ui.Button({
        "focusSkin": "sknTextBtnBlueTxtRegFocus",
        "height": "50dp",
        "id": "CopybtnPopupActionTwo0b42b21041ca348",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_b63e9a6ff8274d9b9373d38298bfdae5,
        "right": 0,
        "skin": "sknTxtRegBlueBgWhite124",
        "text": "OK",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    CopyflxPopupActionBtns0bdbe51cdbd9b40.add(CopyflxPopupBtnDivider0e4e2e785fa8549, CopybtnPopupActionTwo0b42b21041ca348);
    popupAlert.add(lblpopupmsg, CopyflxPopupActionBtns0bdbe51cdbd9b40);
    var CopyflxPopupOverlay0h64529519aa54b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "CopyflxPopupOverlay0h64529519aa54b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_a81bab69314e4f51952ce977bcf198c5,
        "skin": "sknFlxBlackOp40",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyflxPopupOverlay0h64529519aa54b.setDefaultUnit(kony.flex.DP);
    CopyflxPopupOverlay0h64529519aa54b.add();
    flxValidCustomerIdpopup.add(popupAlert, CopyflxPopupOverlay0h64529519aa54b);
    frmHome.add(flxWelcome, flxSideMenuContainer, flxHomeContainer, flxSearchLocation, flxValidCustomerIdpopup);
};

function frmHomeGlobals() {
    frmHome = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmHome,
        "enabledForIdleTimeout": false,
        "id": "frmHome",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFrmWhite"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};