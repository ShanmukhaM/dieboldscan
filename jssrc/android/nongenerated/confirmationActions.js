//Payment confirmation initial state
function confirmationInit() {
    frmConfirmation.flxHeader.isVisible = false;
    frmConfirmation.flxPaymentVerification.isVisible = false;
    frmConfirmation.flxReceiptScan.isVisible = false;
    frmConfirmation.flxReceiptScan.opacity = 0;
    frmConfirmation.lblCreditCardTitle.top = '110dp';
    frmConfirmation.flxTouchIDVerification.opacity = 0;
    frmConfirmation.imgReceipt.top = '100%';
}
//Payment confirmation animation
function confirmationAnim() {
    frmConfirmation.lblCreditCardTitle.animate(kony.ui.createAnimation({
        "0": {
            "top": "110dp"
        },
        "100": {
            "top": "40dp",
            "stepConfig": {
                "timingFunction": kony.anim.EASEIN_IN_OUT
            }
        }
    }), {
        "delay": 0,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.3
    }, {
        "animationEnd": function() {}
    });
    frmConfirmation.flxTouchIDVerification.animate(kony.ui.createAnimation({
        "0": {
            "opacity": 0
        },
        "100": {
            "opacity": 1,
            "stepConfig": {
                "timingFunction": kony.anim.EASEIN_IN_OUT
            }
        }
    }), {
        "delay": 0.2,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.3
    }, {
        "animationEnd": function() {
            var trans0 = kony.ui.makeAffineTransform();
            trans0.scale(1, 1);
            var trans50 = kony.ui.makeAffineTransform();
            trans50.scale(1.5, 1.5);
            var trans100 = kony.ui.makeAffineTransform();
            trans100.scale(1, 1);
            frmConfirmation.imgTouchIDIcon.animate(kony.ui.createAnimation({
                "0": {
                    "transform": trans0
                },
                "50": {
                    "transform": trans50
                },
                "100": {
                    "stepConfig": {
                        "timingFunction": kony.anim.EASEIN_IN_OUT
                    },
                    "transform": trans100
                }
            }), {
                "delay": 0,
                "fillMode": kony.anim.FILL_MODE_FORWARDS,
                "duration": 0.2
            }, {
                "animationEnd": function() {}
            });
        }
    });
}
//Payment payment completed anim
function paymentCompleted() {
    frmConfirmation.flxHeader.isVisible = false;
    frmConfirmation.flxPaymentVerification.isVisible = false;
    frmConfirmation.flxReceiptScan.isVisible = true;
    frmConfirmation.flxReceiptScan.animate(kony.ui.createAnimation({
        "0": {
            "opacity": 0
        },
        "100": {
            "opacity": 1,
            "stepConfig": {
                "timingFunction": kony.anim.EASEIN_IN_OUT
            }
        }
    }), {
        "delay": 0,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    }, {
        "animationEnd": function() {}
    });
    var trans0 = kony.ui.makeAffineTransform();
    trans0.scale(1, 1);
    var trans50 = kony.ui.makeAffineTransform();
    trans50.scale(1.5, 1.5);
    var trans100 = kony.ui.makeAffineTransform();
    trans100.scale(1, 1);
    frmConfirmation.imgCheckIcon.animate(kony.ui.createAnimation({
        "0": {
            "transform": trans0
        },
        "50": {
            "transform": trans50
        },
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "transform": trans100
        }
    }), {
        "delay": 0.3,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.3
    }, {
        "animationEnd": function() {}
    });
    frmConfirmation.imgReceipt.animate(kony.ui.createAnimation({
        "0": {
            "top": "100%"
        },
        "100": {
            "top": "0%",
            "stepConfig": {
                "timingFunction": kony.anim.EASEIN_IN_OUT
            }
        }
    }), {
        "delay": 0.35,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.5
    }, {
        "animationEnd": function() {}
    });
}