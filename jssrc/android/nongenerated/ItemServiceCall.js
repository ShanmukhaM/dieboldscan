var recentScanItem = {};
var ScannedFlag = 0;
//Scan button action
function scanItem(szPosItemId) {
    //var szPosItemId="2010000000373";  //Zibra FFI will provide this
    recentScanItem.Type = "ART_SALE";
    recentScanItem.szPosItemId = szPosItemId;
    recentScanItem.dTaQty = "1";
    createItemServiceRequest(frmCart.segCartItems.data);
}
//Creating request json to be sent to service
function createItemServiceRequest(segmentData) {
    //creating the request by getting records from cart
    kony.print("segmentData: " + JSON.stringify(segmentData));
    var requestStrBasket = [];
    kony.print("Recent ScanItem: " + JSON.stringify(recentScanItem));
    for (var i = 0; i < segmentData.length; i++) {
        if (segmentData[i].szPOSItemID === recentScanItem.szPosItemId) {
            segmentData[i].dTaQty = parseInt(segmentData[i].dTaQty) + 1;
            ScannedFlag = 1;
        }
    }
    if (ScannedFlag === 1) {
        kony.print("Product Already Scanned !");
        ScannedFlag = 0;
    } else if (ScannedFlag === 0) requestStrBasket.push(recentScanItem);
    if (segmentData !== null && segmentData !== undefined) {
        for (var i = 0; i < segmentData.length; i++) {
            var itemJson = {};
            itemJson.Type = "ART_SALE";
            itemJson.szPosItemId = segmentData[i].szPOSItemID;
            itemJson.dTaQty = segmentData[i].dTaQty;
            requestStrBasket.push(itemJson);
        }
    }
    requestStrBasket.push({
        "Type": "TOTAL"
    });
    kony.print("Request Json: " + JSON.stringify(requestStrBasket));
    scanItemService(requestStrBasket);
    recentScanItem = {};
}
//service call on scanning the  product
function scanItemService(requestStrBasket) {
    var serviceName = "BasketCalculation";
    var operationName = "basketRequest";
    //var strBasket=[{"Type":"ART_SALE","szPosItemId":"2010000000373","dTaQty":"1"},{"Type":"ART_SALE","szPosItemId":"2010000000014","dTaQty":"1"},{"Type":"TOTAL"}];
    //var inputParams = {"ClientName": "smart2","szPosItemId": "2010000000373"};
    var inputParams = {
        "ClientName": "smart2",
        "Basket": requestStrBasket
    };
    inputParams.httpheaders = {
        'Content-Type': 'application/json'
    };
    kony.application.showLoadingScreen("");
    AllscannedProduct = [];
    scannedProduct = [];
    mfintegrationsecureinvokerasync(inputParams, serviceName, operationName, scanItemServicecallBack);
    /* 
      var Clientname="smart2";
      var szPosItemId="2010000000373";
      
      integrationServiceObject = kony.sdk.getCurrentInstance().getIntegrationService("BasketCalculation");
      
      integrationServiceObject.invokeOperation("basketRequest", {}, {"ClientName":Clientname,"szPosItemId": szPosItemId}, scanItemServicecallBack,  scanItemServicecallBackFailure);
       */
}

function scanItemServicecallBack(status, scanItemServiceRespponse) {
    if (status === 400) {
        kony.print("response from scanItemService: " + JSON.stringify(scanItemServiceRespponse));
        if (scanItemServiceRespponse.opstatus === 0) {
            //alert(JSON.stringify(scanItemServiceRespponse));
            scannedProduct.push(scanItemServiceRespponse);
            kony.print("product scanned: " + JSON.stringify(scannedProduct));
            multiItemResponseLogic();
            SetItemsToSegment();
        } else {
            alert("Product not added to cart, please scan again.");
        }
    } else {
        alert("Something went wrong, please try after some time.");
    }
}

function SetItemsToSegment() {
    kony.application.dismissLoadingScreen();
    //  frmCart.segCartItems.widgetDataMap={lblProductName:"ItemDescrption",rtxProductDiscounts:"ItemDiscDescription",rtxProductDiscountsValue:"TotalDiscount",rtxProductPrice:"ItemPrice",tbxQtyInput:"ItemQuantity"};
    //	frmCart.segCartItems.removeAll();
    frmCart.segCartItems.widgetDataMap = {
        lblProductName: "szDescription",
        rtxProductDiscounts: "szDiscDesc",
        rtxProductDiscountsValue: "dTotalDiscount",
        rtxProductPrice: "dTaPrice",
        tbxQtyInput: "dTaQty"
    };
    //frmCart.segCartItems.setData(AllscannedProduct);
    kony.print("assigning data to segment: " + JSON.stringify(AllscannedProduct));
    frmCart.segCartItems.data = AllscannedProduct;
    CalcTotalItem();
    //  frmCart.rtxCheckouTotalDiscount.text=totalDiscount;
    frmCart.rtxCheckouTotalDiscount.text = "";
    frmCart.rtxCheckoutSubTotal.text = "$" + subTotal.toString();
    frmCart.show();
};