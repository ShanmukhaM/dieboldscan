var kony = kony || {};
kony.diebold = kony.diebold || {};
kony.diebold.controller = kony.diebold.controller || {};
kony.diebold.controller.CartController = function() {
    /**
     * @function initialize: Constructor for the Skin Controller 
     */
    var initialize = function() {
            //Header
            frmCart.flxBackBtn.onTouchEnd = onBackBtnCartClick;
            //PostShow
            //    frmCart.postShow = onCartPostShow;
            frmCart.segCartItems.onRowClick = onRowClickSegCartItemClick;
            frmCart.flxCheckoutBtn.onTouchEnd = onCheckOutBtnClick;
        },
        /*
         * Header Back button function
         */
        onBackBtnCartClick = function() {
            frmHome.show();
        };
    /*
     * OnRowClick Segment function
     */
    onRowClickSegCartItemClick = function() {
        return AddValuesToCart.call(this);
    };
    /*
     * OnCheckOut button function
     */
    onCheckOutBtnClick = function() {
        frmCheckout.show();
    };
    /*
     * Intialize class
     */
    initialize();
};