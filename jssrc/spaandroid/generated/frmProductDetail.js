function addWidgetsfrmProductDetail() {
    frmProductDetail.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "8%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlxWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxBackBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxBackBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxBackBtn.setDefaultUnit(kony.flex.DP);
    var imArrowLeftIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "22dp",
        "id": "imArrowLeftIcon",
        "isVisible": true,
        "left": "5dp",
        "skin": "slImage",
        "src": "chevron_left.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxBackBtn.add(imArrowLeftIcon);
    var lblHeaderTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblHeaderTitle",
        "isVisible": true,
        "maxNumberOfLines": 1,
        "skin": "sknLblRegDblue136",
        "text": "Salad Mix, Spring Mix",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var flxCartBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxCartBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "slFbox",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxCartBtn.setDefaultUnit(kony.flex.DP);
    var imgBagIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "22dp",
        "id": "imgBagIcon",
        "isVisible": true,
        "right": 10,
        "skin": "slImage",
        "src": "shopping_bag_icon.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblBagCount = new kony.ui.Label({
        "bottom": "9dp",
        "id": "lblBagCount",
        "isVisible": true,
        "right": "5dp",
        "skin": "sknLblBggreenTextBoldWhite98",
        "text": "4",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [10, 0, 10, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxCartBtn.add(imgBagIcon, lblBagCount);
    flxHeader.add(flxBackBtn, lblHeaderTitle, flxCartBtn);
    var flxProductDetail = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "92%",
        "horizontalScrollIndicator": true,
        "id": "flxProductDetail",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "8%",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxProductDetail.setDefaultUnit(kony.flex.DP);
    var flxProductImages = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": false,
        "height": "260dp",
        "id": "flxProductImages",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlxWhiteBr1Rd8",
        "top": "10dp",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    flxProductImages.setDefaultUnit(kony.flex.DP);
    var flxProdImgContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxProdImgContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknProductImageBg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxProdImgContainer.setDefaultUnit(kony.flex.DP);
    var segProductImages = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "data": [{
            "imgProductImage": "product_detail_img.png"
        }],
        "groupCells": false,
        "height": "100%",
        "id": "segProductImages",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "skinTransp",
        "rowTemplate": flxProductImageContainer,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_PAGEVIEW,
        "widgetDataMap": {
            "flxProductImageContainer": "flxProductImageContainer",
            "imgProductImage": "imgProductImage"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxProdImgContainer.add(segProductImages);
    flxProductImages.add(flxProdImgContainer);
    var lblPriceTitle = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblPriceTitle",
        "isVisible": true,
        "skin": "sknLblBlackGrey110",
        "text": "PRICE",
        "top": 20,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var rtxProductPrice = new kony.ui.RichText({
        "centerX": "50%",
        "id": "rtxProductPrice",
        "isVisible": true,
        "skin": "sknRtxBlackDblue260",
        "text": "$4.33",
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblProdSizeTitle = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblProdSizeTitle",
        "isVisible": true,
        "skin": "sknLblBlackGrey110",
        "text": "5 Oz",
        "top": 5,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var flxColorOptions = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxColorOptions",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxColorOptions.setDefaultUnit(kony.flex.DP);
    var flxSelectColorOne = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 10,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxSelectColorOne",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "20%",
        "skin": "sknProductColorSelected",
        "top": "5dp",
        "width": "60dp",
        "zIndex": 1
    }, {}, {});
    flxSelectColorOne.setDefaultUnit(kony.flex.DP);
    var flxColorOptionOne = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "flxColorOptionOne",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknProductColorOne",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxColorOptionOne.setDefaultUnit(kony.flex.DP);
    flxColorOptionOne.add();
    flxSelectColorOne.add(flxColorOptionOne);
    var flxSelectColorTwo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 10,
        "centerX": "50%",
        "clipBounds": true,
        "height": "60dp",
        "id": "flxSelectColorTwo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknProductColorUnselected",
        "top": "5dp",
        "width": "60dp",
        "zIndex": 1
    }, {}, {});
    flxSelectColorTwo.setDefaultUnit(kony.flex.DP);
    var flxColorOptionTwo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "flxColorOptionTwo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknProductColorTwo",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxColorOptionTwo.setDefaultUnit(kony.flex.DP);
    flxColorOptionTwo.add();
    flxSelectColorTwo.add(flxColorOptionTwo);
    var flxSelectColorThree = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 10,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxSelectColorThree",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "20%",
        "skin": "sknProductColorUnselected",
        "top": "5dp",
        "width": "60dp",
        "zIndex": 1
    }, {}, {});
    flxSelectColorThree.setDefaultUnit(kony.flex.DP);
    var flxColorOptionThree = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "flxColorOptionThree",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknProductColorThree",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxColorOptionThree.setDefaultUnit(kony.flex.DP);
    flxColorOptionThree.add();
    flxSelectColorThree.add(flxColorOptionThree);
    flxColorOptions.add(flxSelectColorOne, flxSelectColorTwo, flxSelectColorThree);
    var lblQuantityTitle = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblQuantityTitle",
        "isVisible": true,
        "skin": "sknLblBlackGrey110",
        "text": "QUANTITY",
        "top": 20,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var flxQuantity = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "70dp",
        "id": "flxQuantity",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "0dp",
        "width": "45%",
        "zIndex": 1
    }, {}, {});
    flxQuantity.setDefaultUnit(kony.flex.DP);
    var tbxQtyInput = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "sknRoundInputFocus",
        "height": "50dp",
        "id": "tbxQtyInput",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "secureTextEntry": false,
        "skin": "sknTxtBrWhite1BlackReg124",
        "text": "2",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "5dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoComplete": false,
        "autoCorrect": false
    });
    var flxMinusBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxMinusBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "skin": "sknFlxBtn",
        "top": "50dp",
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    flxMinusBtn.setDefaultUnit(kony.flex.DP);
    var imgMinusIcon = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "22dp",
        "id": "imgMinusIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": "minus_icon.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxMinusBtn.add(imgMinusIcon);
    var flxPlusBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxPlusBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 10,
        "skin": "sknFlxBtn",
        "top": "40dp",
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    flxPlusBtn.setDefaultUnit(kony.flex.DP);
    var imgPlusIcon = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "22dp",
        "id": "imgPlusIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": "plus_icon.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxPlusBtn.add(imgPlusIcon);
    flxQuantity.add(tbxQtyInput, flxMinusBtn, flxPlusBtn);
    var lblDescriptionTitle = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblDescriptionTitle",
        "isVisible": true,
        "skin": "sknLblBlackGrey110",
        "text": "DESCRIPTION",
        "top": 20,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var lblDescription = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblDescription",
        "isVisible": true,
        "skin": "sknLblRegDblue124",
        "text": "If you like your salad tender and silky as satin, heres the salad for you. The taste is sweet and delightful, and the soft, tender textures have made our Sweet Butter an instant favorite with thousands of salad lovers.",
        "top": "10dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var lblSpecificationsTitle = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblSpecificationsTitle",
        "isVisible": true,
        "skin": "sknLblBlackGrey110",
        "text": "NUTRITION FACTS",
        "top": 20,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var segSpecifications = new kony.ui.SegmentedUI2({
        "alternateRowSkin": "sknSegWhite",
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "data": [{
            "CopylblDetailsLeftClumn0f1f863b7fe554b": "Calories 20",
            "lblDetailsLeftClumn": "Calories from Fat 0"
        }, {
            "CopylblDetailsLeftClumn0f1f863b7fe554b": "0%",
            "lblDetailsLeftClumn": "Total Fat 0g"
        }, {
            "CopylblDetailsLeftClumn0f1f863b7fe554b": "0%",
            "lblDetailsLeftClumn": "Cholesterol 0mg"
        }, {
            "CopylblDetailsLeftClumn0f1f863b7fe554b": "0%",
            "lblDetailsLeftClumn": "Sodium 5mg"
        }, {
            "CopylblDetailsLeftClumn0f1f863b7fe554b": "1%",
            "lblDetailsLeftClumn": "Total Carbohydrate 3g"
        }, {
            "CopylblDetailsLeftClumn0f1f863b7fe554b": "8%",
            "lblDetailsLeftClumn": "     Dietary Fiber 2g"
        }, {
            "CopylblDetailsLeftClumn0f1f863b7fe554b": "",
            "lblDetailsLeftClumn": "     Sugars 1g"
        }, {
            "CopylblDetailsLeftClumn0f1f863b7fe554b": "",
            "lblDetailsLeftClumn": "Protein 1g"
        }, {
            "CopylblDetailsLeftClumn0f1f863b7fe554b": "140%",
            "lblDetailsLeftClumn": "Vitamin A"
        }, {
            "CopylblDetailsLeftClumn0f1f863b7fe554b": "2%",
            "lblDetailsLeftClumn": "Calcium"
        }, {
            "CopylblDetailsLeftClumn0f1f863b7fe554b": "120%",
            "lblDetailsLeftClumn": "Vitamin K"
        }, {
            "CopylblDetailsLeftClumn0f1f863b7fe554b": "6%",
            "lblDetailsLeftClumn": "Vitamin C"
        }, {
            "CopylblDetailsLeftClumn0f1f863b7fe554b": "4%",
            "lblDetailsLeftClumn": "Iron"
        }, {
            "CopylblDetailsLeftClumn0f1f863b7fe554b": "30%",
            "lblDetailsLeftClumn": "Folate"
        }],
        "groupCells": false,
        "id": "segSpecifications",
        "isVisible": true,
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "skinTransp",
        "rowTemplate": flxProductDetailsRow,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "separatorThickness": 0,
        "showScrollbars": false,
        "top": "10dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "CopylblDetailsLeftClumn0f1f863b7fe554b": "CopylblDetailsLeftClumn0f1f863b7fe554b",
            "flxDetailLeftColumn": "flxDetailLeftColumn",
            "flxDetailRightColumn": "flxDetailRightColumn",
            "flxProductDetailsRow": "flxProductDetailsRow",
            "lblDetailsLeftClumn": "lblDetailsLeftClumn"
        },
        "width": "90%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxBottomScrollSpacer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "14%",
        "id": "flxBottomScrollSpacer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBottomScrollSpacer.setDefaultUnit(kony.flex.DP);
    flxBottomScrollSpacer.add();
    flxProductDetail.add(flxProductImages, lblPriceTitle, rtxProductPrice, lblProdSizeTitle, flxColorOptions, lblQuantityTitle, flxQuantity, lblDescriptionTitle, lblDescription, lblSpecificationsTitle, segSpecifications, flxBottomScrollSpacer);
    var flxAddToCartBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 10,
        "centerX": "50%",
        "clipBounds": true,
        "focusSkin": "sknPrimaryRndBtnFocus",
        "height": "8%",
        "id": "flxAddToCartBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlxRoundBlue",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    flxAddToCartBtn.setDefaultUnit(kony.flex.DP);
    var lblAddToCartBtn = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "40dp",
        "id": "lblAddToCartBtn",
        "isVisible": true,
        "left": "132dp",
        "skin": "sknLblBlackWhite121",
        "text": "ADD TO CART",
        "top": "18dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxAddToCartBtn.add(lblAddToCartBtn);
    frmProductDetail.add(flxHeader, flxProductDetail, flxAddToCartBtn);
};

function frmProductDetailGlobals() {
    frmProductDetail = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmProductDetail,
        "enabledForIdleTimeout": false,
        "id": "frmProductDetail",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFrmWhite"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "retainScrollPosition": false
    });
};