function initializetempListTitle() {
    flxListTitle = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "flxListTitle",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxListTitle.setDefaultUnit(kony.flex.DP);
    var lblListTitle = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblListTitle",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegGrey100",
        "text": "NEAR YOU",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxListTitle.add(lblListTitle);
}