function addWidgetsfrmCart() {
    frmCart.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "8%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlxWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxBackBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxBackBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxBackBtn.setDefaultUnit(kony.flex.DP);
    var imArrowLeftIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "22dp",
        "id": "imArrowLeftIcon",
        "isVisible": true,
        "left": "5dp",
        "skin": "slImage",
        "src": "chevron_left.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxBackBtn.add(imArrowLeftIcon);
    var lblHeaderTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblHeaderTitle",
        "isVisible": true,
        "skin": "sknLblRegDblue136",
        "text": "Cart",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var flxCartItemCount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "62%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "30dp",
        "id": "flxCartItemCount",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxCartItemCount.setDefaultUnit(kony.flex.DP);
    var lblBagCount = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBagCount",
        "isVisible": true,
        "left": 0,
        "skin": "sknLblBggreenTextBoldWhite98",
        "text": "5",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [11, 0, 11, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxCartItemCount.add(lblBagCount);
    flxHeader.add(flxBackBtn, lblHeaderTitle, flxCartItemCount);
    var flxCartContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "68%",
        "horizontalScrollIndicator": true,
        "id": "flxCartContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "8.20%",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCartContainer.setDefaultUnit(kony.flex.DP);
    var lblTotalCartItems = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblTotalCartItems",
        "isVisible": true,
        "left": 10,
        "skin": "sknLblRegGrey110",
        "text": "You have 8 items in your cart",
        "top": 15,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var segCartItems = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "imgMinusIcon": "minus_icon.png",
            "imgPlusIcon": "plus_icon.png",
            "imgProduct": "chicken_breasts.png",
            "lblProductName": "Boneless Chicken Breast",
            "rtxProductDiscounts": "10% OFF–$8.81",
            "rtxProductPrice": "$9.79",
            "tbxQtyInput": {
                "placeholder": "0",
                "text": "1"
            }
        }, {
            "imgMinusIcon": "minus_icon.png",
            "imgPlusIcon": "plus_icon.png",
            "imgProduct": "salad_mix_bag.png",
            "lblProductName": "Salad Mix, Spring Mix",
            "rtxProductDiscounts": "BOGO 50%–2.16",
            "rtxProductPrice": "$4.33",
            "tbxQtyInput": {
                "placeholder": "0",
                "text": "2"
            }
        }, {
            "imgMinusIcon": "minus_icon.png",
            "imgPlusIcon": "plus_icon.png",
            "imgProduct": "body_wash.png",
            "lblProductName": "Body Wash",
            "rtxProductDiscounts": "",
            "rtxProductPrice": "$9.94",
            "tbxQtyInput": {
                "placeholder": "0",
                "text": "1"
            }
        }, {
            "imgMinusIcon": "minus_icon.png",
            "imgPlusIcon": "plus_icon.png",
            "imgProduct": "rice_bag.png",
            "lblProductName": "Long Grain Enriched Rice",
            "rtxProductDiscounts": "",
            "rtxProductPrice": "$2.90",
            "tbxQtyInput": {
                "placeholder": "0",
                "text": "1"
            }
        }, {
            "imgMinusIcon": "minus_icon.png",
            "imgPlusIcon": "plus_icon.png",
            "imgProduct": "tidesoap.png",
            "lblProductName": "Tide Laundry Detergent",
            "rtxProductDiscounts": "",
            "rtxProductPrice": "$15.51",
            "tbxQtyInput": {
                "placeholder": "0",
                "text": "1"
            }
        }, {
            "imgMinusIcon": "minus_icon.png",
            "imgPlusIcon": "plus_icon.png",
            "imgProduct": "brie.png",
            "lblProductName": "Ile de France Soft Ripened Brie Cheese",
            "rtxProductDiscounts": "",
            "rtxProductPrice": "$13.09",
            "tbxQtyInput": {
                "placeholder": "0",
                "text": "1"
            }
        }, {
            "imgMinusIcon": "minus_icon.png",
            "imgPlusIcon": "plus_icon.png",
            "imgProduct": "sparkling_water.png",
            "lblProductName": "St. Pellegrino (Pack of 24)",
            "rtxProductDiscounts": "",
            "rtxProductPrice": "$24.50",
            "tbxQtyInput": {
                "placeholder": "0",
                "text": "1"
            }
        }],
        "groupCells": false,
        "id": "segCartItems",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "sknSegWhite",
        "rowSkin": "sknSegWhite",
        "rowTemplate": flxCartItem,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "dfdfdf00",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "45dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxCartItem": "flxCartItem",
            "flxMinusBtn": "flxMinusBtn",
            "flxPlusBtn": "flxPlusBtn",
            "flxProductImage": "flxProductImage",
            "flxQuantity": "flxQuantity",
            "imgMinusIcon": "imgMinusIcon",
            "imgPlusIcon": "imgPlusIcon",
            "imgProduct": "imgProduct",
            "lblProductName": "lblProductName",
            "rtxProductDiscounts": "rtxProductDiscounts",
            "rtxProductPrice": "rtxProductPrice",
            "tbxQtyInput": "tbxQtyInput"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxCartContainer.add(lblTotalCartItems, segCartItems);
    var flxCheckoutTotals = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "11%",
        "clipBounds": true,
        "height": "13%",
        "id": "flxCheckoutTotals",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "sknFlexWhie",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCheckoutTotals.setDefaultUnit(kony.flex.DP);
    var flxTotalsDividerLine1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 1,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxTotalsDividerLine1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknLightGreyDividerLine",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTotalsDividerLine1.setDefaultUnit(kony.flex.DP);
    flxTotalsDividerLine1.add();
    var flxCheckouttSubtotal = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "49%",
        "id": "flxCheckouttSubtotal",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "0",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCheckouttSubtotal.setDefaultUnit(kony.flex.DP);
    var rtxCheckoutSubtotal = new kony.ui.RichText({
        "centerY": "50%",
        "height": "100%",
        "id": "rtxCheckoutSubtotal",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknRtRegBlack107",
        "text": "$3.15",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblCheckoutSubtotal = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblCheckoutSubtotal",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegDblue107",
        "text": "Total discounts",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxCheckouttSubtotal.add(rtxCheckoutSubtotal, lblCheckoutSubtotal);
    var flxCartTaxes = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "49%",
        "id": "flxCartTaxes",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "0",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCartTaxes.setDefaultUnit(kony.flex.DP);
    var rtxCheckoutTaxes = new kony.ui.RichText({
        "centerY": "50%",
        "height": "100%",
        "id": "rtxCheckoutTaxes",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknRtxBoldDblue134",
        "text": "$28.14",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblCheckoutTaxes = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblCheckoutTaxes",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblBoldDblue134",
        "text": "Cart subtotal",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxCartTaxes.add(rtxCheckoutTaxes, lblCheckoutTaxes);
    flxCheckoutTotals.add(flxTotalsDividerLine1, flxCheckouttSubtotal, flxCartTaxes);
    var flxButtonContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "11%",
        "id": "flxButtonContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlexWhie",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxButtonContainer.setDefaultUnit(kony.flex.DP);
    var flxCheckoutBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 10,
        "centerX": "50%",
        "clipBounds": true,
        "focusSkin": "sknPrimaryRndBtnFocus",
        "height": "74%",
        "id": "flxCheckoutBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxRoundBlue",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    flxCheckoutBtn.setDefaultUnit(kony.flex.DP);
    var lblCheckoutBtn = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblCheckoutBtn",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblBlackWhite121",
        "text": "CHECKOUT",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxCheckoutBtn.add(lblCheckoutBtn);
    flxButtonContainer.add(flxCheckoutBtn);
    frmCart.add(flxHeader, flxCartContainer, flxCheckoutTotals, flxButtonContainer);
};

function frmCartGlobals() {
    frmCart = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmCart,
        "enabledForIdleTimeout": false,
        "id": "frmCart",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFrmWhite"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "retainScrollPosition": false
    });
};