function initializetempProductDetails() {
    flxProductDetailsRow = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "flxProductDetailsRow",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxProductDetailsRow.setDefaultUnit(kony.flex.DP);
    var flxDetailLeftColumn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxDetailLeftColumn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {}, {});
    flxDetailLeftColumn.setDefaultUnit(kony.flex.DP);
    var lblDetailsLeftClumn = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblDetailsLeftClumn",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblBoldDblue107",
        "text": "Calories 20",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxDetailLeftColumn.add(lblDetailsLeftClumn);
    var flxDetailRightColumn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxDetailRightColumn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "slFbox",
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {}, {});
    flxDetailRightColumn.setDefaultUnit(kony.flex.DP);
    var CopylblDetailsLeftClumn0f1f863b7fe554b = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblDetailsLeftClumn0f1f863b7fe554b",
        "isVisible": true,
        "right": 10,
        "skin": "sknLblRegDblue107",
        "text": "Calories from Fat 0",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxDetailRightColumn.add(CopylblDetailsLeftClumn0f1f863b7fe554b);
    flxProductDetailsRow.add(flxDetailLeftColumn, flxDetailRightColumn);
}