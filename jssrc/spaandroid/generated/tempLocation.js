function initializetempLocation() {
    flxLocation = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "62dp",
        "id": "flxLocation",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxLocation.setDefaultUnit(kony.flex.DP);
    var lblLocationName = new kony.ui.Label({
        "id": "lblLocationName",
        "isVisible": true,
        "left": "40dp",
        "maxNumberOfLines": 1,
        "skin": "sknLblRegDblue124",
        "text": "Electronics Shop",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "10dp",
        "width": "72%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var imgLocationIcon = new kony.ui.Image2({
        "height": "22dp",
        "id": "imgLocationIcon",
        "isVisible": true,
        "left": "8dp",
        "skin": "slImage",
        "src": "location_icon.png",
        "top": "14dp",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxMilesAddress = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "bottom": 10,
        "clipBounds": true,
        "id": "flxMilesAddress",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "40dp",
        "skin": "slFbox",
        "top": "35dp",
        "width": "75%",
        "zIndex": 1
    }, {}, {});
    flxMilesAddress.setDefaultUnit(kony.flex.DP);
    var lblLocationMilage = new kony.ui.Label({
        "id": "lblLocationMilage",
        "isVisible": true,
        "left": "0dp",
        "maxNumberOfLines": 1,
        "right": "5dp",
        "skin": "sknLblBlackGrey110",
        "text": "1.0 mi",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var lblLocationAddress = new kony.ui.Label({
        "id": "lblLocationAddress",
        "isVisible": true,
        "left": "0dp",
        "maxNumberOfLines": 1,
        "skin": "sknLblRegGrey110",
        "text": "• 4450 Koelpin Keys Austin, TX",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "0dp",
        "width": "85%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxMilesAddress.add(lblLocationMilage, lblLocationAddress);
    var FlexContainer0b8404cd8a8eb40 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "44dp",
        "id": "FlexContainer0b8404cd8a8eb40",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "slFbox",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    FlexContainer0b8404cd8a8eb40.setDefaultUnit(kony.flex.DP);
    var imgMoreIcon = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "22dp",
        "id": "imgMoreIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": "more_icon.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    FlexContainer0b8404cd8a8eb40.add(imgMoreIcon);
    flxLocation.add(lblLocationName, imgLocationIcon, flxMilesAddress, FlexContainer0b8404cd8a8eb40);
}