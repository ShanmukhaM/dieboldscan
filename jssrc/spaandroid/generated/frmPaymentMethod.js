function addWidgetsfrmPaymentMethod() {
    frmPaymentMethod.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "8%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlxWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxBackBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxBackBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxBackBtn.setDefaultUnit(kony.flex.DP);
    var imArrowLeftIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "22dp",
        "id": "imArrowLeftIcon",
        "isVisible": true,
        "left": "5dp",
        "skin": "slImage",
        "src": "chevron_left.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxBackBtn.add(imArrowLeftIcon);
    var flxCheckOut = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxCheckOut",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "slFbox",
        "top": "0dp",
        "width": "80dp",
        "zIndex": 1
    }, {}, {});
    flxCheckOut.setDefaultUnit(kony.flex.DP);
    var lblCheckoutBtn = new kony.ui.Label({
        "centerY": "55%",
        "id": "lblCheckoutBtn",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknLblMdBlack111",
        "text": "Checkout",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxCheckOut.add(lblCheckoutBtn);
    var lblHeaderTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblHeaderTitle",
        "isVisible": true,
        "skin": "sknLblRegDblue136",
        "text": "Payment Method",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxHeader.add(flxBackBtn, flxCheckOut, lblHeaderTitle);
    var flxCheckoutContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "66%",
        "horizontalScrollIndicator": true,
        "id": "flxCheckoutContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "8%",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCheckoutContainer.setDefaultUnit(kony.flex.DP);
    var lblPaymentMethodMessage = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblPaymentMethodMessage",
        "isVisible": true,
        "left": 10,
        "skin": "sknLblRegGrey110",
        "text": "Select payment method for checkout.",
        "top": 15,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var segPaymentMethod = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "imgCheckmark": "tick_icon.png",
            "imgPaymentIcon": "visa_card.png",
            "imgStarIcon": "star_icon.png",
            "lblSelectedPaymentMethod": "**** 4248"
        }, {
            "imgCheckmark": "",
            "imgPaymentIcon": "mastercard.png",
            "imgStarIcon": "",
            "lblSelectedPaymentMethod": "**** 5796"
        }, {
            "imgCheckmark": "",
            "imgPaymentIcon": "apple_pay.png",
            "imgStarIcon": "",
            "lblSelectedPaymentMethod": "Apple Pay"
        }, {
            "imgCheckmark": "",
            "imgPaymentIcon": "direct_pay.png",
            "imgStarIcon": "",
            "lblSelectedPaymentMethod": "Direct Pay"
        }],
        "groupCells": false,
        "id": "segPaymentMethod",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": flxPaymentMethod,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "dfdfdf00",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "20dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxPaymentInfoContainer": "flxPaymentInfoContainer",
            "flxPaymentMethod": "flxPaymentMethod",
            "imgCheckmark": "imgCheckmark",
            "imgPaymentIcon": "imgPaymentIcon",
            "imgStarIcon": "imgStarIcon",
            "lblSelectedPaymentMethod": "lblSelectedPaymentMethod"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxCheckoutContainer.add(lblPaymentMethodMessage, segPaymentMethod);
    var flxPayBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 10,
        "centerX": "50%",
        "clipBounds": true,
        "focusSkin": "sknPrimaryRndBtnFocus",
        "height": "8%",
        "id": "flxPayBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxRoundBlue",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    flxPayBtn.setDefaultUnit(kony.flex.DP);
    var lblPayBtn = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblPayBtn",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblBlackWhite121",
        "text": "ADD PAYMENT",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxPayBtn.add(lblPayBtn);
    frmPaymentMethod.add(flxHeader, flxCheckoutContainer, flxPayBtn);
};

function frmPaymentMethodGlobals() {
    frmPaymentMethod = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmPaymentMethod,
        "enabledForIdleTimeout": false,
        "id": "frmPaymentMethod",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFrmWhite"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "retainScrollPosition": false
    });
};