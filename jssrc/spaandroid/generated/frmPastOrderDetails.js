function addWidgetsfrmPastOrderDetails() {
    frmPastOrderDetails.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "8%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlxWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxBackBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxBackBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_j5b16d485c2643fa9452f6f747dba926,
        "skin": "slFbox",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxBackBtn.setDefaultUnit(kony.flex.DP);
    var imArrowLeftIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "22dp",
        "id": "imArrowLeftIcon",
        "isVisible": true,
        "left": "5dp",
        "skin": "slImage",
        "src": "chevron_left.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxBackBtn.add(imArrowLeftIcon);
    var lblHeaderTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblHeaderTitle",
        "isVisible": true,
        "skin": "sknLblRegDblue136",
        "text": "#000212348351",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxHeader.add(flxBackBtn, lblHeaderTitle);
    var flxCheckoutContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "58%",
        "horizontalScrollIndicator": true,
        "id": "flxCheckoutContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "8%",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCheckoutContainer.setDefaultUnit(kony.flex.DP);
    var lblPaymentMethodTitle = new kony.ui.Label({
        "id": "lblPaymentMethodTitle",
        "isVisible": true,
        "left": 10,
        "skin": "sknLblRegGrey100",
        "text": "SUMMARY",
        "top": 20,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var flxOrderSummary = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "85dp",
        "id": "flxOrderSummary",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "sknFlexWhie",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxOrderSummary.setDefaultUnit(kony.flex.DP);
    var lblOrderNumber = new kony.ui.Label({
        "id": "lblOrderNumber",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegDblue107",
        "text": "Transaction #000212348351",
        "top": 10,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var lblOrderDate = new kony.ui.Label({
        "id": "lblOrderDate",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegDblue107",
        "text": "September 7, 2017 - 05:35 PM",
        "top": 5,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var lblPaymentMethod = new kony.ui.Label({
        "id": "lblPaymentMethod",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegDblue107",
        "text": "Payment Method - VISA **** 5241",
        "top": 5,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxOrderSummary.add(lblOrderNumber, lblOrderDate, lblPaymentMethod);
    var lblScannedItemsTitle = new kony.ui.Label({
        "id": "lblScannedItemsTitle",
        "isVisible": true,
        "left": 10,
        "skin": "sknLblRegGrey100",
        "text": "PRODUCTS (8)",
        "top": 20,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var flxScannedItemList = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxScannedItemList",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxScannedItemList.setDefaultUnit(kony.flex.DP);
    var segCheckoutItems = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblProductName": "Boneless Chicken Breast",
            "lblProductQty": "1",
            "rtxProductDiscounts": "10% OFF",
            "rtxProductPrice": "$9.79"
        }, {
            "lblProductName": "Salad Mix, Spring Mix",
            "lblProductQty": "2",
            "rtxProductDiscounts": "BOGO 50%",
            "rtxProductPrice": "$4.33"
        }, {
            "lblProductName": "Body Wash",
            "lblProductQty": "1",
            "rtxProductDiscounts": "",
            "rtxProductPrice": "$9.94"
        }, {
            "lblProductName": "Long Grain Enriched Rice",
            "lblProductQty": "1",
            "rtxProductDiscounts": "",
            "rtxProductPrice": "$2.90"
        }, {
            "lblProductName": "Tide Laundry Detergent",
            "lblProductQty": "1",
            "rtxProductDiscounts": "",
            "rtxProductPrice": "$15.51"
        }, {
            "lblProductName": "Ile de France Soft Ripened Brie Cheese",
            "lblProductQty": "1",
            "rtxProductDiscounts": "",
            "rtxProductPrice": "$13.09"
        }, {
            "lblProductName": "St. Pellegrino (Pack of 24)",
            "lblProductQty": "1",
            "rtxProductDiscounts": "",
            "rtxProductPrice": "$24.50"
        }],
        "groupCells": false,
        "id": "segCheckoutItems",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": flxCheckoutItem,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "dfdfdf00",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxCheckoutItem": "flxCheckoutItem",
            "flxProductInfo": "flxProductInfo",
            "lblProductName": "lblProductName",
            "lblProductQty": "lblProductQty",
            "rtxProductDiscounts": "rtxProductDiscounts",
            "rtxProductPrice": "rtxProductPrice"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxScannedItemList.add(segCheckoutItems);
    flxCheckoutContainer.add(lblPaymentMethodTitle, flxOrderSummary, lblScannedItemsTitle, flxScannedItemList);
    var lblTotalsTitle = new kony.ui.Label({
        "bottom": "30%",
        "id": "lblTotalsTitle",
        "isVisible": true,
        "left": 10,
        "skin": "sknLblRegGrey100",
        "text": "TOTALS",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var flxCheckoutTotals = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "11%",
        "clipBounds": true,
        "height": "18%",
        "id": "flxCheckoutTotals",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "sknFlxWhite",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCheckoutTotals.setDefaultUnit(kony.flex.DP);
    var flxCheckouttSubtotal = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "32.33%",
        "id": "flxCheckouttSubtotal",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "0",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCheckouttSubtotal.setDefaultUnit(kony.flex.DP);
    var rtxCheckoutSubtotal = new kony.ui.RichText({
        "centerY": "50%",
        "height": "100%",
        "id": "rtxCheckoutSubtotal",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknRtRegBlack107",
        "text": "$28.14",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblCheckoutSubtotal = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblCheckoutSubtotal",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegDblue107",
        "text": "Cart subtotal",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxCheckouttSubtotal.add(rtxCheckoutSubtotal, lblCheckoutSubtotal);
    var flxTotalsDividerLine1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 1,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxTotalsDividerLine1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknLightGreyDividerLine",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTotalsDividerLine1.setDefaultUnit(kony.flex.DP);
    flxTotalsDividerLine1.add();
    var flxCartTaxes = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "32.33%",
        "id": "flxCartTaxes",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "0",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCartTaxes.setDefaultUnit(kony.flex.DP);
    var rtxCheckoutTaxes = new kony.ui.RichText({
        "centerY": "50%",
        "height": "100%",
        "id": "rtxCheckoutTaxes",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknRtRegBlack107",
        "text": "$3.36",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblCheckoutTaxes = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblCheckoutTaxes",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegDblue107",
        "text": "Sales taxes",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxCartTaxes.add(rtxCheckoutTaxes, lblCheckoutTaxes);
    var flxTotalsDividerLine2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 1,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxTotalsDividerLine2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknLightGreyDividerLine",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTotalsDividerLine2.setDefaultUnit(kony.flex.DP);
    flxTotalsDividerLine2.add();
    var flxCheckoutTotal = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "32.33%",
        "id": "flxCheckoutTotal",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "0",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCheckoutTotal.setDefaultUnit(kony.flex.DP);
    var rtxCheckoutTotal = new kony.ui.RichText({
        "centerY": "50%",
        "height": "100%",
        "id": "rtxCheckoutTotal",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknRtxBoldDblue134",
        "text": "$31.50",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblCheckoutTotal = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblCheckoutTotal",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblBoldDblue134",
        "text": "Total",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxCheckoutTotal.add(rtxCheckoutTotal, lblCheckoutTotal);
    flxCheckoutTotals.add(flxCheckouttSubtotal, flxTotalsDividerLine1, flxCartTaxes, flxTotalsDividerLine2, flxCheckoutTotal);
    var flxButtonContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "11%",
        "id": "flxButtonContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlxWhite",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxButtonContainer.setDefaultUnit(kony.flex.DP);
    var flxSeeReceiptBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 10,
        "centerX": "50%",
        "clipBounds": true,
        "focusSkin": "sknPrimaryRndBtnFocus",
        "height": "74%",
        "id": "flxSeeReceiptBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxRoundBlue",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    flxSeeReceiptBtn.setDefaultUnit(kony.flex.DP);
    var lblPayBtn = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblPayBtn",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblBlackWhite121",
        "text": "RECEIPT",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxSeeReceiptBtn.add(lblPayBtn);
    flxButtonContainer.add(flxSeeReceiptBtn);
    var flxReceiptScan = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxReceiptScan",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlxLightGrey",
        "top": "100%",
        "width": "100%",
        "zIndex": 3
    }, {}, {});
    flxReceiptScan.setDefaultUnit(kony.flex.DP);
    var flxReceiptHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxReceiptHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxReceiptHeader.setDefaultUnit(kony.flex.DP);
    var flxCloseBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxCloseBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxCloseBtn.setDefaultUnit(kony.flex.DP);
    var imgCloseIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "20dp",
        "id": "imgCloseIcon",
        "isVisible": true,
        "left": "10dp",
        "skin": "slImage",
        "src": "close_icon.png",
        "width": "20dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxCloseBtn.add(imgCloseIcon);
    var lblReceiptTransactionID = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblReceiptTransactionID",
        "isVisible": true,
        "left": "10dp",
        "maxNumberOfLines": 1,
        "skin": "sknLblBoldDblue134",
        "text": "#000212348351",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "10dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var lblReceiptTransactionDate = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblReceiptTransactionDate",
        "isVisible": true,
        "skin": "sknLblRegGrey110",
        "text": "September 7, 2017 - 05:35 PM",
        "top": "35dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxReceiptHeader.add(flxCloseBtn, lblReceiptTransactionID, lblReceiptTransactionDate);
    var flxReceipt = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "15%",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "75%",
        "horizontalScrollIndicator": true,
        "id": "flxReceipt",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxReceipt.setDefaultUnit(kony.flex.DP);
    var imgReceipt = new kony.ui.Image2({
        "centerX": "50%",
        "id": "imgReceipt",
        "isVisible": true,
        "skin": "slImage",
        "src": "receipt_img.png",
        "top": "0%",
        "width": "88%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxReceipt.add(imgReceipt);
    var flxReceiptScanCode = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "centerX": "50%",
        "clipBounds": true,
        "height": "15%",
        "id": "flxReceiptScanCode",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxReceiptScanCode.setDefaultUnit(kony.flex.DP);
    var imgShadow = new kony.ui.Image2({
        "centerX": "50%",
        "id": "imgShadow",
        "isVisible": true,
        "skin": "slImage",
        "src": "shadow.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgScanCode = new kony.ui.Image2({
        "centerX": "50%",
        "id": "imgScanCode",
        "isVisible": true,
        "skin": "slImage",
        "src": "scan_code.png",
        "top": 20,
        "width": "75%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxReceiptScanCode.add(imgShadow, imgScanCode);
    flxReceiptScan.add(flxReceiptHeader, flxReceipt, flxReceiptScanCode);
    frmPastOrderDetails.add(flxHeader, flxCheckoutContainer, lblTotalsTitle, flxCheckoutTotals, flxButtonContainer, flxReceiptScan);
};

function frmPastOrderDetailsGlobals() {
    frmPastOrderDetails = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmPastOrderDetails,
        "enabledForIdleTimeout": false,
        "id": "frmPastOrderDetails",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFrmWhite"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "retainScrollPosition": false
    });
};