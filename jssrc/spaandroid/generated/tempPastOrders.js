function initializetempPastOrders() {
    flxPastOrder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "65dp",
        "id": "flxPastOrder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlexWhie",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPastOrder.setDefaultUnit(kony.flex.DP);
    var lblTransactionNumber = new kony.ui.Label({
        "id": "lblTransactionNumber",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegDblue124",
        "text": "#000212348351",
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var lblTotalAmount = new kony.ui.Label({
        "id": "lblTotalAmount",
        "isVisible": true,
        "right": 44,
        "skin": "sknLblRegDblue124",
        "text": "$31.50",
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var lblTransactionDate = new kony.ui.Label({
        "bottom": 10,
        "id": "lblTransactionDate",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegGrey100",
        "text": "November 29, 2016 - 17:35",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxPastOrder.add(lblTransactionNumber, lblTotalAmount, lblTransactionDate);
}