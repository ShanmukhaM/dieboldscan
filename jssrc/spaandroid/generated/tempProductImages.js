function initializetempProductImages() {
    flxProductImageContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "240dp",
        "id": "flxProductImageContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxProductImageContainer.setDefaultUnit(kony.flex.DP);
    var imgProductImage = new kony.ui.Image2({
        "centerX": "50%",
        "height": "100%",
        "id": "imgProductImage",
        "isVisible": true,
        "skin": "slImage",
        "src": "product_image_big.png",
        "top": 0,
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxProductImageContainer.add(imgProductImage);
}