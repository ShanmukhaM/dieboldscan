function addWidgetsfrmAccount() {
    frmAccount.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "8%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlxWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxBackBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxBackBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxBackBtn.setDefaultUnit(kony.flex.DP);
    var imArrowLeftIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "22dp",
        "id": "imArrowLeftIcon",
        "isVisible": true,
        "left": "5dp",
        "skin": "slImage",
        "src": "chevron_left.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxBackBtn.add(imArrowLeftIcon);
    var flxEditBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxEditBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "slFbox",
        "top": "0dp",
        "width": "80dp",
        "zIndex": 1
    }, {}, {});
    flxEditBtn.setDefaultUnit(kony.flex.DP);
    var lblCheckoutBtn = new kony.ui.Label({
        "centerY": "55%",
        "id": "lblCheckoutBtn",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknLblMdBlack111",
        "text": "Edit",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxEditBtn.add(lblCheckoutBtn);
    var lblHeaderTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblHeaderTitle",
        "isVisible": true,
        "skin": "sknLblRegDblue136",
        "text": "My Account",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxHeader.add(flxBackBtn, flxEditBtn, lblHeaderTitle);
    var flxAccountContent = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "92%",
        "horizontalScrollIndicator": true,
        "id": "flxAccountContent",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "8%",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAccountContent.setDefaultUnit(kony.flex.DP);
    var flxUserProfile = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": false,
        "height": "75dp",
        "id": "flxUserProfile",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxWhiteBr1Rd60",
        "top": 10,
        "width": "75dp",
        "zIndex": 2
    }, {}, {});
    flxUserProfile.setDefaultUnit(kony.flex.DP);
    var flxUserProfileImage = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "flxUserProfileImage",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknUserProfileImageNoShadow",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxUserProfileImage.setDefaultUnit(kony.flex.DP);
    var Image0caba7cffc5664e = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "Image0caba7cffc5664e",
        "isVisible": true,
        "skin": "slImage",
        "src": "profile_image.png",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxUserProfileImage.add(Image0caba7cffc5664e);
    flxUserProfile.add(flxUserProfileImage);
    var flxChangePictureBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "34dp",
        "id": "flxChangePictureBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "88dp",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    flxChangePictureBtn.setDefaultUnit(kony.flex.DP);
    flxChangePictureBtn.add();
    var flxMemberPoints = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxMemberPoints",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "22%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMemberPoints.setDefaultUnit(kony.flex.DP);
    var lblPoints = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblPoints",
        "isVisible": true,
        "skin": "sknLblBoldOrange234",
        "text": "2,452pts",
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var CopylblMember0c451a5e99ace41 = new kony.ui.Label({
        "centerX": "50%",
        "id": "CopylblMember0c451a5e99ace41",
        "isVisible": true,
        "skin": "sknLblRegDblue107",
        "text": "You are 548 points away from next reward.",
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxMemberPoints.add(lblPoints, CopylblMember0c451a5e99ace41);
    var flxAccountInfoContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "80%",
        "id": "flxAccountInfoContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "minWidth": "75%",
        "skin": "sknFlxWhite",
        "top": "21%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxAccountInfoContainer.setDefaultUnit(kony.flex.DP);
    var flxAccountName = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxAccountName",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAccountName.setDefaultUnit(kony.flex.DP);
    var lblAccountNameTitle = new kony.ui.Label({
        "id": "lblAccountNameTitle",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegGrey90",
        "text": "Name",
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var lblAccountName = new kony.ui.Label({
        "bottom": 10,
        "id": "lblAccountName",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegDblue136",
        "text": "Maggie Moore",
        "top": "30dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxAccountName.add(lblAccountNameTitle, lblAccountName);
    var flxEmailAddress = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxEmailAddress",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxEmailAddress.setDefaultUnit(kony.flex.DP);
    var lblEmailAddressTitle = new kony.ui.Label({
        "id": "lblEmailAddressTitle",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegGrey90",
        "text": "Email Address",
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var lblEmailAddress = new kony.ui.Label({
        "bottom": 10,
        "id": "lblEmailAddress",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegDblue136",
        "text": "maggie_moore@gmail.com",
        "top": "30dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxEmailAddress.add(lblEmailAddressTitle, lblEmailAddress);
    var flxAddress = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxAddress",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAddress.setDefaultUnit(kony.flex.DP);
    var lblAddressTitle = new kony.ui.Label({
        "id": "lblAddressTitle",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegGrey90",
        "text": "Addresses",
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var lblAddress1 = new kony.ui.Label({
        "bottom": 10,
        "id": "lblAddress1",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegDblue136",
        "text": "26 Benedict Well Suite 206 Austin, TX 73301",
        "top": "8dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var lblAddress2 = new kony.ui.Label({
        "bottom": 10,
        "id": "lblAddress2",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegDblue136",
        "text": "9263 Runte Estates Austin, TX 73301",
        "top": "8dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxAddress.add(lblAddressTitle, lblAddress1, lblAddress2);
    var flxPhoneNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxPhoneNumber",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPhoneNumber.setDefaultUnit(kony.flex.DP);
    var lblPhoneNumberTitle = new kony.ui.Label({
        "id": "lblPhoneNumberTitle",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegGrey90",
        "text": "Phone Number",
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var lblPhoneNumber = new kony.ui.Label({
        "bottom": 10,
        "id": "lblPhoneNumber",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegDblue136",
        "text": "(819) 952-6354",
        "top": "30dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxPhoneNumber.add(lblPhoneNumberTitle, lblPhoneNumber);
    var flxUsername = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxUsername",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxUsername.setDefaultUnit(kony.flex.DP);
    var lblUsernameTitle = new kony.ui.Label({
        "id": "lblUsernameTitle",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegGrey90",
        "text": "Username",
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var lblUsername = new kony.ui.Label({
        "bottom": 10,
        "id": "lblUsername",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegDblue136",
        "text": "maggie10",
        "top": "30dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxUsername.add(lblUsernameTitle, lblUsername);
    var flxPassword = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxPassword",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPassword.setDefaultUnit(kony.flex.DP);
    var lblPasswordTitle = new kony.ui.Label({
        "id": "lblPasswordTitle",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegGrey90",
        "text": "Password",
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var lblPassword = new kony.ui.Label({
        "bottom": 10,
        "id": "lblPassword",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblRegDblue136",
        "text": "•••••••••••",
        "top": "30dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxPassword.add(lblPasswordTitle, lblPassword);
    flxAccountInfoContainer.add(flxAccountName, flxEmailAddress, flxAddress, flxPhoneNumber, flxUsername, flxPassword);
    flxAccountContent.add(flxUserProfile, flxChangePictureBtn, flxMemberPoints, flxAccountInfoContainer);
    frmAccount.add(flxHeader, flxAccountContent);
};

function frmAccountGlobals() {
    frmAccount = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmAccount,
        "enabledForIdleTimeout": false,
        "id": "frmAccount",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFrmWhite"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "retainScrollPosition": false
    });
};