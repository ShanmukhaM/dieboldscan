function initializetempCartItemsOld() {
    flxCartItemOld = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "110dp",
        "id": "flxCartItemOld",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlexWhie"
    }, {}, {});
    flxCartItemOld.setDefaultUnit(kony.flex.DP);
    var flxProductImage = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "44dp",
        "id": "flxProductImage",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "skin": "slFbox",
        "top": "15dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxProductImage.setDefaultUnit(kony.flex.DP);
    var imgProduct = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "imgProduct",
        "isVisible": true,
        "skin": "slImage",
        "src": "chicken_breasts.png",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxProductImage.add(imgProduct);
    var flxProductDescription = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90dp",
        "id": "flxProductDescription",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "70dp",
        "skin": "slFbox",
        "top": "13dp",
        "width": "40%",
        "zIndex": 1
    }, {}, {});
    flxProductDescription.setDefaultUnit(kony.flex.DP);
    var lblProductName = new kony.ui.Label({
        "id": "lblProductName",
        "isVisible": true,
        "left": "0dp",
        "maxNumberOfLines": 2,
        "skin": "sknDarkSmTxtMd",
        "text": "Product Name",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": 0,
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var lblProductDetail = new kony.ui.Label({
        "id": "lblProductDetail",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblRegGrey110",
        "text": "Size: M",
        "top": 0,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var lblProductPrice = new kony.ui.Label({
        "id": "lblProductPrice",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknDarkSmTxtMd",
        "text": "$59.00",
        "top": 10,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxProductDescription.add(lblProductName, lblProductDetail, lblProductPrice);
    var flxQuantity = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxQuantity",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "slFbox",
        "top": "0dp",
        "width": "40%",
        "zIndex": 1
    }, {}, {});
    flxQuantity.setDefaultUnit(kony.flex.DP);
    var tbxQtyInput = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "sknRoundInputFocus",
        "height": "40dp",
        "id": "tbxQtyInput",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "secureTextEntry": false,
        "skin": "sknTxtBrWhite1BlackReg124",
        "text": "1",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "5dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoComplete": false,
        "autoCorrect": false
    });
    var flxMinusBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxMinusBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "skin": "sknFlxBtn",
        "top": "40dp",
        "width": "40dp",
        "zIndex": 1
    }, {}, {});
    flxMinusBtn.setDefaultUnit(kony.flex.DP);
    var imgMinusIcon = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "22dp",
        "id": "imgMinusIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": "minus_inactive_icon.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxMinusBtn.add(imgMinusIcon);
    var flxPlusBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxPlusBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 10,
        "skin": "sknFlxBtn",
        "top": "40dp",
        "width": "40dp",
        "zIndex": 1
    }, {}, {});
    flxPlusBtn.setDefaultUnit(kony.flex.DP);
    var imgPlusIcon = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "22dp",
        "id": "imgPlusIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": "plus_icon.png",
        "width": "22dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxPlusBtn.add(imgPlusIcon);
    flxQuantity.add(tbxQtyInput, flxMinusBtn, flxPlusBtn);
    var btnRemoveItem = new kony.ui.Button({
        "bottom": 5,
        "focusSkin": "sknTextBtnRedSmTxtThinFocus",
        "height": "35dp",
        "id": "btnRemoveItem",
        "isVisible": true,
        "right": 0,
        "skin": "sknTextBtnRedSmTxtThin",
        "text": "Remove",
        "width": "40%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxCartItemOld.add(flxProductImage, flxProductDescription, flxQuantity, btnRemoveItem);
}