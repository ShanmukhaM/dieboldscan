//startup.js file
var appConfig = {
    appId: "dnretail",
    appName: "dnretail",
    appVersion: "1.0.0",
    platformVersion: null,
    serverIp: "10.20.17.46",
    serverPort: "80",
    secureServerPort: "443",
    isturlbase: "https://diebold.konylabs.net/services",
    isMFApp: true,
    appKey: "77586ce65e7cfc74ea3d7ce9ff7df06a",
    appSecret: "fcb401a499d2a66170a7baa1cc133d23",
    serviceUrl: "https://diebold.konylabs.net/authService/100000002/appconfig",
    svcDoc: {
        "appId": "13c120b3-a521-43c0-bc6d-42485300b437",
        "baseId": "54d419f3-1919-4172-b2f6-d7fb00314379",
        "name": "ScanGo",
        "selflink": "https://diebold.konylabs.net/authService/100000002/appconfig",
        "integsvc": {
            "BasketCalculation": "https://diebold.konylabs.net/services/BasketCalculation"
        },
        "reportingsvc": {
            "custom": "https://diebold.konylabs.net/services/CMS",
            "session": "https://diebold.konylabs.net/services/IST"
        },
        "Webapp": {
            "url": "https://diebold.konylabs.net/Sample"
        },
        "services_meta": {
            "BasketCalculation": {
                "version": "1.0",
                "url": "https://diebold.konylabs.net/services/BasketCalculation",
                "type": "integsvc"
            }
        }
    },
    eventTypes: [],
    url: "https://diebold.konylabs.net/admin/dnretail/MWServlet",
    secureurl: "https://diebold.konylabs.net/admin/dnretail/MWServlet",
    middlewareContext: "dnretail"
};
sessionID = "";

function appInit(params) {
    skinsInit();
    initializeUserWidgets();
    initializetempCartItemsNew();
    initializetempCartItemsOld();
    initializetempCheckoutItemsNew();
    initializetempCheckoutItemsOld();
    initializetempCoupons();
    initializetempListTitle();
    initializetempLocation();
    initializetempPastOrders();
    initializetempPayment();
    initializetempPaymentMethod();
    initializetempProductDetails();
    initializetempProductImages();
    frmAccountGlobals();
    frmCartGlobals();
    frmCheckoutGlobals();
    frmConfirmationGlobals();
    frmHomeGlobals();
    frmManagePaymentGlobals();
    frmPastOrderDetailsGlobals();
    frmPastOrdersGlobals();
    frmPaymentMethodGlobals();
    frmProductDetailGlobals();
    frmScanOverlayGlobals();
    setAppBehaviors();
};

function setAppBehaviors() {
    kony.application.setApplicationBehaviors({
        applyMarginPaddingInBCGMode: false,
        adherePercentageStrictly: true,
        retainSpaceOnHide: true,
        APILevel: 7300
    })
};

function themeCallBack() {
    initializeGlobalVariables();
    requirejs.config({
        baseUrl: "spaandroid/appjs"
    });
    var requireModulesList = getSPARequireModulesList();
    require(requireModulesList, function() {
        kony.application.setApplicationInitializationEvents({
            init: appInit,
            postappinit: AS_AppEvents_cb52ec8fc0fe43928b5ff008b074ef1c,
            showstartupform: function() {
                frmHome.show();
            }
        });
    });
};

function loadResources() {
    kony.theme.packagedthemes(
        ["default"]);
    globalhttpheaders = {};
    callAppMenu();
    sdkInitConfig = {
        "appConfig": appConfig,
        "isMFApp": appConfig.isMFApp,
        "appKey": appConfig.appKey,
        "appSecret": appConfig.appSecret,
        "serviceUrl": appConfig.serviceUrl,
        eventTypes: []
    }
    kony.setupsdks(sdkInitConfig, onSuccessSDKCallBack, onSuccessSDKCallBack);
};

function onSuccessSDKCallBack() {
    kony.theme.setCurrentTheme("default", themeCallBack, themeCallBack);
}

function initializeApp() {
    kony.application.setApplicationMode(constants.APPLICATION_MODE_NATIVE);
    //If default locale is specified. This is set even before any other app life cycle event is called.
    loadResources();
};
									function getSPARequireModulesList(){ return ['kvmodules']; }
								