var ItemCount = null;
var ItemNumber = null;
var ItemDetails = null;
var TotalItems = scannedProduct.length;

function AddValuesToCart() {
    kony.print("selected row: " + JSON.stringify(frmCart.segCartItems.selectedRowItems));
    ItemDetails = frmCart.segCartItems.selectedRowItems;
    ItemNumber = frmCart.segCartItems.selectedRowIndices[0][1][0];
    ItemCount = ItemDetails[0].ItemQuantity;
}

function AddItem() {
    kony.print("Clicked on Plus button");
    frmCart.segCartItems.onRowClick();
    ItemCount = parseInt(ItemCount) + 1;
    TotalItems = TotalItems + 1;
    if (ItemCount === 1) ItemDetails[0].imgMinusIcon = "minus_icon";
    ItemDetails[0].ItemQuantity = ItemCount.toString();
    kony.print("Modified  Details: " + JSON.stringify(ItemDetails));
    frmCart.segCartItems.setDataAt(ItemDetails[0], ItemNumber);
    // alert("Clicked on Plus "+ ItemCount + "item Number "+ ItemNumber);
    CalcTotalItem();
}

function RemoveItem() {
    kony.print("Clicked on Minus button");
    frmCart.segCartItems.onRowClick();
    if (ItemCount > 0) {
        ItemCount = parseInt(ItemCount) - 1;
        TotalItems = TotalItems - 1;
        if (ItemCount === 0) {
            frmCart.segCartItems.removeAt(ItemNumber);
        }
        //ItemDetails[0].imgMinusIcon="minus_inactive_icon";
        else {
            ItemDetails[0].ItemQuantity = ItemCount.toString();
            kony.print("Modified  Details: " + JSON.stringify(ItemDetails));
            frmCart.segCartItems.setDataAt(ItemDetails[0], ItemNumber);
        }
        //alert("Clicked on minus "+ ItemCount + "item Number "+ ItemNumber);
        CalcTotalItem();
    }
}

function CalcTotalItem() {
    frmCart.lblPaymentMethodMessage.text = "You have " + TotalItems + " items in your cart";
}