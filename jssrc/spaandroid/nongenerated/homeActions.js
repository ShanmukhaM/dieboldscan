//Home initial state
function homeInit() {
    //Welcome screen
    frmHome.flxWelcome.isVisible = true;
    frmHome.flxHomeContainer.isVisible = false;
    frmHome.flxWelcome.opacity = 1;
    frmHome.imgWelcomeLogo.top = '50%';
    //Login form
    frmHome.txbUserName.opacity = 0;
    frmHome.txbPassword.opacity = 0;
    frmHome.flxSignInBtn.opacity = 0;
    frmHome.flxRememberMe.opacity = 0;
    frmHome.flxRememberMe.top = '75%';
    frmHome.flxForgotPasswordBtn.opacity = 0;
    frmHome.flxForgotPasswordBtn.bottom = '-5%';
    frmHome.flxNoAccount.opacity = 0;
    frmHome.flxNoAccount.bottom = '-10%';
    //Welcome message after signin
    frmHome.flxWelcomeMessage.top = '50%';
    frmHome.flxWelcomeMessage.opacity = 0;
    frmHome.flxWelcomeMessage.isVisible = false;
    //Confirm location
    frmHome.flxLocationConfirm.top = '50%';
    frmHome.flxLocationConfirm.opacity = 0;
    frmHome.flxLocationMap.opacity = 0;
    frmHome.flxLocationConfirm.isVisible = false;
    //search location map
    frmHome.flxSearchLocation.isVisible = false;
    frmHome.flxSearchHeader.top = '-8%';
    frmHome.flxLocationList.top = '100%';
    frmHome.segLocationList.isVisible = true;
    frmHome.segLocationList.opacity = 1;
    frmHome.flxMapLocationDetails.isVisible = false;
    frmHome.flxMapLocationDetails.bottom = '-160dp';
    frmHome.mapSearchLocations.isVisible = false;
    frmHome.mapSearchLocations.opacity = 0;
}
//Login animation In
function loginAnim() {
    var currFrm = kony.application.getCurrentForm();
    var trans0 = kony.ui.makeAffineTransform();
    trans0.scale(1, 1);
    var trans50 = kony.ui.makeAffineTransform();
    trans50.scale(1.2, 1.2);
    var trans100 = kony.ui.makeAffineTransform();
    trans100.scale(1, 1);
    currFrm.imgWelcomeLogo.animate(kony.ui.createAnimation({
        "0": {
            "top": "50%"
        },
        "100": {
            "top": "8%",
            "stepConfig": {
                "timingFunction": kony.anim.EASEIN_IN_OUT
            }
        }
    }), {
        "delay": 0.25,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.5
    }, {
        "animationEnd": function() {}
    });
    currFrm.txbUserName.animate(kony.ui.createAnimation({
        "0": {
            "transform": trans0,
            "opacity": 0
        },
        "50": {
            "transform": trans50,
            "opacity": 1
        },
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASEIN_IN_OUT
            },
            "transform": trans100
        }
    }), {
        "delay": 0.6,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    }, {
        "animationEnd": function() {}
    });
    currFrm.txbPassword.animate(kony.ui.createAnimation({
        "0": {
            "transform": trans0,
            "opacity": 0
        },
        "50": {
            "transform": trans50,
            "opacity": 1
        },
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASEIN_IN_OUT
            },
            "transform": trans100
        }
    }), {
        "delay": 0.65,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    }, {
        "animationEnd": function() {}
    });
    currFrm.flxSignInBtn.animate(kony.ui.createAnimation({
        "0": {
            "transform": trans0,
            "opacity": 0
        },
        "50": {
            "transform": trans50,
            "opacity": 1
        },
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASEIN_IN_OUT
            },
            "transform": trans100
        }
    }), {
        "delay": 0.7,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    }, {
        "animationEnd": function() {}
    });
    currFrm.flxRememberMe.animate(kony.ui.createAnimation({
        "0": {
            "top": "75%",
            "opacity": 0
        },
        "100": {
            "top": "70%",
            "opacity": 1,
            "stepConfig": {
                "timingFunction": kony.anim.EASEIN_IN_OUT
            }
        }
    }), {
        "delay": 0.75,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    }, {
        "animationEnd": function() {}
    });
    currFrm.flxForgotPasswordBtn.animate(kony.ui.createAnimation({
        "0": {
            "bottom": "-5%",
            "opacity": 0
        },
        "100": {
            "bottom": "0%",
            "opacity": 1,
            "stepConfig": {
                "timingFunction": kony.anim.EASEIN_IN_OUT
            }
        }
    }), {
        "delay": 0.75,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    }, {
        "animationEnd": function() {}
    });
    currFrm.flxNoAccount.animate(kony.ui.createAnimation({
        "0": {
            "bottom": "-10%",
            "opacity": 0
        },
        "100": {
            "bottom": "0%",
            "opacity": 1,
            "stepConfig": {
                "timingFunction": kony.anim.EASEIN_IN_OUT
            }
        }
    }), {
        "delay": 0.75,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    }, {
        "animationEnd": function() {}
    });
}
//Login animation Out
function loginAnimOut() {
    var currFrm = kony.application.getCurrentForm();
    currFrm.txbUserName.animate(kony.ui.createAnimation({
        "0": {
            "opacity": 1
        },
        "100": {
            "opacity": 0,
            "stepConfig": {
                "timingFunction": kony.anim.EASEIN_IN_OUT
            },
        }
    }), {
        "delay": 0,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    }, {
        "animationEnd": function() {}
    });
    currFrm.txbPassword.animate(kony.ui.createAnimation({
        "0": {
            "opacity": 1
        },
        "100": {
            "opacity": 0,
            "stepConfig": {
                "timingFunction": kony.anim.EASEIN_IN_OUT
            }
        }
    }), {
        "delay": 0.05,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    }, {
        "animationEnd": function() {}
    });
    currFrm.flxSignInBtn.animate(kony.ui.createAnimation({
        "0": {
            "opacity": 1
        },
        "100": {
            "opacity": 0,
            "stepConfig": {
                "timingFunction": kony.anim.EASEIN_IN_OUT
            }
        }
    }), {
        "delay": 0.1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    }, {
        "animationEnd": function() {}
    });
    currFrm.flxRememberMe.animate(kony.ui.createAnimation({
        "0": {
            "opacity": 1
        },
        "100": {
            "opacity": 0,
            "stepConfig": {
                "timingFunction": kony.anim.EASEIN_IN_OUT
            }
        }
    }), {
        "delay": 0.1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    }, {
        "animationEnd": function() {}
    });
    currFrm.flxForgotPasswordBtn.animate(kony.ui.createAnimation({
        "0": {
            "opacity": 1
        },
        "100": {
            "opacity": 0,
            "stepConfig": {
                "timingFunction": kony.anim.EASEIN_IN_OUT
            }
        }
    }), {
        "delay": 0.1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    }, {
        "animationEnd": function() {}
    });
    currFrm.flxNoAccount.animate(kony.ui.createAnimation({
        "0": {
            "opacity": 1
        },
        "100": {
            "opacity": 0,
            "stepConfig": {
                "timingFunction": kony.anim.EASEIN_IN_OUT
            }
        }
    }), {
        "delay": 0.1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    }, {
        "animationEnd": function() {}
    });
}
//Welcome post show animation
function welcomeAnim() {
    var currFrm = kony.application.getCurrentForm();
    currFrm.flxLocationConfirm.isVisible = true;
    currFrm.flxWelcomeMessage.isVisible = true;
    currFrm.flxWelcomeMessage.animate(kony.ui.createAnimation({
        "0": {
            "top": "50%",
            "opacity": 0
        },
        "100": {
            "top": "25%",
            "opacity": 1,
            "stepConfig": {
                "timingFunction": kony.anim.EASEIN_IN_OUT
            }
        }
    }), {
        "delay": 0.5,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.5
    }, {
        "animationEnd": function() {}
    });
    currFrm.flxLocationConfirm.animate(kony.ui.createAnimation({
        "0": {
            "top": "50%",
            "opacity": 0
        },
        "100": {
            "top": "33%",
            "opacity": 1,
            "stepConfig": {
                "timingFunction": kony.anim.EASEIN_IN_OUT
            }
        }
    }), {
        "delay": 0.6,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.5
    }, {
        "animationEnd": function() {
            var trans0 = kony.ui.makeAffineTransform();
            trans0.scale(1, 1);
            var trans50 = kony.ui.makeAffineTransform();
            trans50.scale(1.2, 1.2);
            var trans100 = kony.ui.makeAffineTransform();
            trans100.scale(1, 1);
            currFrm.flxLocationMap.animate(kony.ui.createAnimation({
                "0": {
                    "transform": trans0,
                    "opacity": 0
                },
                "50": {
                    "transform": trans50,
                    "opacity": 1
                },
                "100": {
                    "stepConfig": {
                        "timingFunction": kony.anim.EASE
                    },
                    "transform": trans100
                }
            }), {
                "delay": 0.1,
                "iterationCount": 1,
                "fillMode": kony.anim.FILL_MODE_FORWARDS,
                "duration": 0.25
            }, {
                "animationEnd": function() {}
            });
        }
    });
}
//Welcome confirm location animation
function locationYes() {
    var currFrm = kony.application.getCurrentForm();
    frmHome.flxHomeContainer.isVisible = true;
    currFrm.flxWelcome.animate(kony.ui.createAnimation({
        "0": {
            "opacity": 1
        },
        "100": {
            "opacity": 0,
            "stepConfig": {
                "timingFunction": kony.anim.EASEIN_IN_OUT
            }
        }
    }), {
        "delay": 0.65,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.2
    }, {
        "animationEnd": function() {
            currFrm.flxWelcome.isVisible = false;
        }
    });
}