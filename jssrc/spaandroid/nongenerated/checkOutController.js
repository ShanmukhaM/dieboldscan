var kony = kony || {};
kony.diebold = kony.diebold || {};
kony.diebold.controller = kony.diebold.controller || {};
kony.diebold.controller.checkOutController = function() {
    /**
     * @function initialize: Constructor for the Skin Controller 
     */
    var initialize = function() {
            //PostShow
            frmCheckout.postShow = onpostShowCheckOut;
            //Header
            frmCheckout.flxBackBtn.onTouchEnd = onBackBtnChechOutClick;
            //Segment
            frmCheckout.flxPaymentMethod.onTouchEnd = onPaymentMethodCheckOutClick;
            //Popup
            frmCheckout.flxCloseBtn.onTouchEnd = onCloseBtnCheckOutClick;
            frmCheckout.flxPopupOverlay.onTouchEnd = onOverlayCheckOutClick;
            //Checkout
            frmCheckout.flxPayBtn.onTouchEnd = onPayBtnCheckOutClick;
        },
        /*
         * CheckOut PostShow function
         */
        onpostShowCheckOut = function() {
            return popupInit.call(this);
        };
    /*
     * Header Back button function
     */
    onBackBtnChechOutClick = function() {
        frmCart.show();
    };
    /*
     * on Payment method click
     */
    onPaymentMethodCheckOutClick = function() {
        frmPaymentMethod.show();
    };
    /*
     * OnCheckOut close button function
     */
    onCloseBtnCheckOutClick = function() {
        frmCart.show();
    };
    /*
     * OnCheckOut popup close button function
     */
    onOverlayCheckOutClick = function() {
        return popupToggle.call(this);
    };
    /*
     * OnCheckOut Pay button function
     */
    onPayBtnCheckOutClick = function() {
        popupToggle.call(this);
    };
    /*
     * Intialize class
     */
    initialize();
};