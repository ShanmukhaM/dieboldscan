var kony = kony || {};
kony.diebold = kony.diebold || {};
kony.diebold.controller = kony.diebold.controller || {};
kony.diebold.controller.managePaymentController = function() {
    /**
     * @function initialize: Constructor for the Skin Controller 
     */
    var initialize = function() {
            //Header
            frmManagePayment.flxBackBtn.onTouchEnd = onBackBtnManagePaymentClick;
        },
        /*
         * Header Back button function
         */
        onBackBtnManagePaymentClick = function() {
            return backBtn.call(this);
        };
    /*
     * Intialize class
     */
    initialize();
};