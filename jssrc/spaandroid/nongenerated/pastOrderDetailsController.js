var kony = kony || {};
kony.diebold = kony.diebold || {};
kony.diebold.controller = kony.diebold.controller || {};
kony.diebold.controller.pastOrderDetailsController = function() {
    /**
     * @function initialize: Constructor for the Skin Controller 
     */
    var initialize = function() {
            //PostShow
            frmPastOrderDetails.postShow = onPastOrderDetailsPostShow;
            //Header
            frmPastOrderDetails.flxBackBtn.onTouchEnd = onBackBtnPastOrderDetailsClick;
            //Receipt 
            frmPastOrderDetails.flxSeeReceiptBtn.onTouchEnd = onSeeReceiptBtnPastOrderdetailsClick;
            //close Reciept Scan
            frmPastOrderDetails.flxCloseBtn.onTouchEnd = oncloseBtnPastOrderdetailsClick;
        },
        /*
         * PostShow function
         */
        onPastOrderDetailsPostShow = function() {
            return pastOrderInit.call(this);
        };
    /*
     * Header Back button function
     */
    onBackBtnPastOrderDetailsClick = function() {
        frmPastOrders.show();
    };
    /*
     * See reciept button function
     */
    onSeeReceiptBtnPastOrderdetailsClick = function() {
        return pastOrderReceipt.call(this);
    };
    /*
     * See reciept button function
     */
    oncloseBtnPastOrderdetailsClick = function() {
        return pastOrderReceipt.call(this);
    };
    /*
     * Intialize class
     */
    initialize();
};