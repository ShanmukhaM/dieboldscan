var kony = kony || {};
kony.diebold = kony.diebold || {};
kony.diebold.controller = kony.diebold.controller || {};
kony.diebold.controller.paymentMethodController = function() {
    /**
     * @function initialize: Constructor for the Skin Controller 
     */
    var initialize = function() {
            frmPaymentMethod.flxBackBtn.onTouchEnd = backButtonPayment;
        },
        /*
         * Header Back button function
         */
        backButtonPayment = function() {
            return backBtn.call(this);
        };
    /*
     * Intialize class
     */
    initialize();
};