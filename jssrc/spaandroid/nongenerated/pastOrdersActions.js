//Past Orders initial state
function pastOrderInit() {
    frmPastOrderDetails.flxReceiptScan.isVisible = false;
    frmPastOrderDetails.flxReceiptScan.top = '100%';
    frmPastOrderDetails.flxReceiptScan.opacity = 0;
    frmPastOrderDetails.imgReceipt.top = '100%';
}

function pastOrderReceipt() {
    var currFrm = kony.application.getCurrentForm();
    if (currFrm.flxReceiptScan.isVisible === false) {
        currFrm.flxReceiptScan.isVisible = true;
        currFrm.flxReceiptScan.animate(kony.ui.createAnimation({
            "0": {
                "top": "100%",
                "opacity": 0
            },
            "100": {
                "top": "0%",
                "opacity": 1,
                "stepConfig": {
                    "timingFunction": kony.anim.EASEIN_IN_OUT
                }
            }
        }), {
            "delay": 0,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": function() {}
        });
        currFrm.imgReceipt.animate(kony.ui.createAnimation({
            "0": {
                "top": "100%"
            },
            "100": {
                "top": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASEIN_IN_OUT
                }
            }
        }), {
            "delay": 0.2,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.5
        }, {
            "animationEnd": function() {}
        });
    } else {
        currFrm.imgReceipt.animate(kony.ui.createAnimation({
            "0": {
                "top": "0%"
            },
            "100": {
                "top": "100%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASEIN_IN_OUT
                }
            }
        }), {
            "delay": 0,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": function() {}
        });
        currFrm.flxReceiptScan.animate(kony.ui.createAnimation({
            "0": {
                "top": "0%",
                "opacity": 1
            },
            "100": {
                "top": "100%",
                "opacity": 0,
                "stepConfig": {
                    "timingFunction": kony.anim.EASEIN_IN_OUT
                }
            }
        }), {
            "delay": 0,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": function() {
                currFrm.flxReceiptScan.isVisible = false;
            }
        });
    }
}