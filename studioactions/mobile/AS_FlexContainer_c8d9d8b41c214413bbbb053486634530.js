function AS_FlexContainer_c8d9d8b41c214413bbbb053486634530(eventobject, context) {
    function SHOW_ALERT__c72270bf7f7f4fad8d8e8772e8771e75_True() {}

    function SHOW_ALERT__c72270bf7f7f4fad8d8e8772e8771e75_Callback() {
        SHOW_ALERT__c72270bf7f7f4fad8d8e8772e8771e75_True()
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_INFO,
        "alertTitle": "Plus",
        "yesLabel": "Ok",
        "message": "You added one more to your cart",
        "alertHandler": SHOW_ALERT__c72270bf7f7f4fad8d8e8772e8771e75_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    })
}